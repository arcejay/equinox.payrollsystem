﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Services;
using Task = System.Threading.Tasks.Task;

namespace Equinox.PayrollSystem.Module
{
    public class HelperClass
    {
        public static void SetDateRange(DateTime date, out DateTime startDate, out DateTime endDate)
        {
            //set start date
            var todaysDate = date.Day;


            //get current year
            var year = date.Year;

            //date range should be the previous month
            if (todaysDate < 15)
            {
                //get previous month
                var month = date.Month - 1;
                if (month == 0)
                {
                    month = 12;
                    year -= 1;
                }

                //set date range to 15 - last day of the previous month
                var maxday = DateTime.DaysInMonth(date.Year, month);

                startDate = new DateTime(year, month, 16);
                endDate = new DateTime(year, month, maxday);
            }
            else
            {
                var month = date.Month;

                startDate = new DateTime(year, month, 1);
                endDate = new DateTime(year, month, 15);

            }
        }
        public static List<DateTime> GenerateDates(DateTime date1, DateTime date2)
        {
            var start = date1.Date;
            var end = date2.Date;

            return Enumerable.Range(0, 1 + end.Subtract(start).Days)
                .Select(offset => start.AddDays(offset)).ToList();
        }

        public static IDtrService GetDtrService(Employee employee)
        {
            if (employee?.WorkSchedule == null) throw new ArgumentNullException(nameof(employee.WorkSchedule));

            switch (employee.WorkSchedule.RequiredTimeCheck)
            {
                case WorkScheduleType.FourTimeCheck:
                    return new WorkSheduleWith4TimeCheckDtrService();
                case WorkScheduleType.SixTimeCheck:
                    return new WorkScheduleWith6TimeCheckDtrService();
                case WorkScheduleType.TwoTimeCheck:
                    return new WorkScheduleWith2TimeCheckDtrService();
            }

            return null;
        }

        public static DeviceStatus CheckStatus(string ip)
        {
            using (var ping = new Ping())
            {
                var ipadd = ip;
                var address = IPAddress.Parse(ipadd);
                var res = Task.Run(async ()=> await ping.SendPingAsync(address));
                var result = res.Result.Status == IPStatus.Success ? DeviceStatus.Online : DeviceStatus.Offline;

                return result;
            }
        }

        public static async Task<DeviceStatus> CheckStatusAsync(string ip)
        {
            using (var ping = new Ping())
            {
                var ipadd = ip;
                var address = IPAddress.Parse(ipadd);
                var res = await ping.SendPingAsync(address);
                var result = (res.Status == IPStatus.Success) ? DeviceStatus.Online : DeviceStatus.Offline;

                return result;
            }

        }
    }



}
