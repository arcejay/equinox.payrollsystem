﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.StateMachine.NonPersistent;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.StateMachines
{
    public class PayrollItemStateMachine : StateMachine<PayrollItem>, IStateMachineUISettings
    {
        private IState _startState = null;

        public PayrollItemStateMachine(IObjectSpace objectSpace) : base(objectSpace)
        {
            //possible states
            _startState = new State(this, PayrollItemStatus.Pending);
            var releasedState = new State(this, PayrollItemStatus.Released);

            //Pending -> Released
            _startState.Transitions.Add(new Transition(releasedState));

            //initialize
            States.Add(_startState);
            States.Add(releasedState);

        }

        public override string Name => "Chage Status to";
        public override string StatePropertyName => "Status";
        public override IState StartState => _startState;
        public bool ExpandActionsInDetailView => true;
    }
}
