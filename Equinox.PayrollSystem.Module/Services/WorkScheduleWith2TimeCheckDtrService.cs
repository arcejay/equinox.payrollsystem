﻿using System;
using System.Collections.Generic;
using System.Linq;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Extensions;

namespace Equinox.PayrollSystem.Module.Services
{
    public class WorkScheduleWith2TimeCheckDtrService : IDtrService
    {
        public void GetWorkHour(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            if (dtr.Employee.WorkSchedule?.TimeCheckItems.Count != 2) throw new ArgumentOutOfRangeException(nameof(dtr));
            var def = default(TimeSpan);
            var workSchedule = dtr.Employee.WorkSchedule;

            double workHours = 0;

            TimeSpan t1;
            TimeSpan t2;

            var ts1 = workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = workSchedule.TimeCheckItems[1].TimeCheck;

            if (dtr.Time1.TimeOfDay != def && dtr.Time4.TimeOfDay != def)
            {
                //check time1 if greater than ts1 (employee is late)
                if (dtr.Time1.TimeOfDay > ts1)
                {
                    t1 = dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod ? dtr.Time1.TimeOfDay : ts1;
                }
                else
                {
                    //ts1 > t1 (employee is early)
                    t1 = ts1;
                }

                //employee logout later
                if (dtr.Time4.TimeOfDay > ts2)
                {
                    t2 = ts2;
                }
                else
                {
                    //employee logout early
                    t2 = dtr.Time4.TimeOfDay;
                }

                workHours += t2.Subtract(t1).TotalHours;

                workHours = workHours > 8 ? workHours - 1 : workHours;
            }

            dtr.WorkHours = workHours;
        }

        public void GetUnderTime(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            if (dtr.Employee.WorkSchedule?.TimeCheckItems.Count != 2) throw new ArgumentOutOfRangeException(nameof(dtr));

            var def = default(DateTime);

            var workSchedule = dtr.Employee.WorkSchedule;

            double result = 0;

            var ts1 = workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = workSchedule.TimeCheckItems[1].TimeCheck;

            //AM Time In
            if (dtr.Time1 != def )
            {
                //PM Time Out
                if (dtr.Time4 != def )
                {
                    //undertime
                    if (dtr.Time4.TimeOfDay < ts2 )
                    {
                        result += ts2.Subtract(dtr.Time4.TimeOfDay).TotalHours - 1;
                    }
                }
                else
                {
                    var temp = ts2.Subtract(ts1).TotalHours;
                    result += temp >= 8 ? 8 : temp - 1;
                }
            }

            dtr.UnderTime = result;
        }

        public void GetTardiness(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            if (dtr.Employee.WorkSchedule?.TimeCheckItems.Count != 2) throw new ArgumentOutOfRangeException(nameof(dtr));

            var def = default(DateTime);

            var workSchedule = dtr.Employee.WorkSchedule;

            double result = 0;

            var ts1 = workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = workSchedule.TimeCheckItems[1].TimeCheck;

            //PM Time Out
            if (dtr.Time4 != def )
            {
                //AM Time In
                if (dtr.Time1 != def)
                {
                    //Late
                    if (dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod)
                    {
                        result += dtr.Time1.TimeOfDay.Subtract(ts1).TotalHours;
                    }
                }
                else
                {
                    result += ts2.Subtract(ts1).TotalHours;
                }
               
            }

            dtr.Tardiness = result;
        }

        public void GetOfficialBusiness(DailyTimeRecord dtr, OfficialBusiness obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));

            var def = default(TimeSpan);

            var totalHours = obj.EndTime.Subtract(obj.StartTime).TotalHours;
            var employee = dtr.Employee;

            //double ob = 0;

            //OB is full day
            if (totalHours >= 8)
            {
                dtr.OfficialBusiness = totalHours - 1;
                dtr.UnderTime = 0;
                dtr.Tardiness = 0;
                dtr.Lwop = 0;
            }
            else
            {
                //OB is not full day, so exact OB hours must be calculated.
                var timeCheckCount = employee.WorkSchedule.TimeCheckItems.Count;
                
                if (timeCheckCount == 2)
                {
                    var t1 = dtr.Time1.TimeOfDay == def ? obj.StartTime : dtr.Time1.TimeOfDay;
                    var t2 = dtr.Time2.TimeOfDay == def ? obj.EndTime : dtr.Time2.TimeOfDay;

                    dtr.OfficialBusiness = t2.Subtract(t1).TotalHours;
                }
            }
        }

        public void GetHoliday(DailyTimeRecord dtr, Holiday holiday)
        {
            throw new NotImplementedException();
        }

        public void GenerateDtr(DailyTimeRecord dtr, List<DateTime> timelog)
        {
            throw new NotImplementedException();
        }
        
    }
}