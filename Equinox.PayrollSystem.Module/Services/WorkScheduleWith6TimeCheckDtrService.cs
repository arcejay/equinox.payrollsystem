﻿using System;
using System.Collections.Generic;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;

namespace Equinox.PayrollSystem.Module.Services
{
    public class WorkScheduleWith6TimeCheckDtrService : IDtrService
    {
        public void GetWorkHour(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            throw new NotImplementedException();
        }

        public void GetUnderTime(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            throw new NotImplementedException();
        }

        public void GetTardiness(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            throw new NotImplementedException();
        }

        public void GetOfficialBusiness(DailyTimeRecord dtr, OfficialBusiness ob)
        {
            throw new NotImplementedException();
        }

        public void GetHoliday(DailyTimeRecord dtr, Holiday holiday)
        {
            throw new NotImplementedException();
        }

        public void GetHoliday(DailyTimeRecord dtr)
        {
            throw new NotImplementedException();
        }

        public void GenerateDtr(DailyTimeRecord dtr, List<DateTime> timelog)
        {
            throw new NotImplementedException();
        }

        public void GenerateDtr(DailyTimeRecord dtr)
        {
            throw new NotImplementedException();
        }
    }
}