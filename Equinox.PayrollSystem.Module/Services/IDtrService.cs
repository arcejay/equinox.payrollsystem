﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;

namespace Equinox.PayrollSystem.Module.Services
{
    public interface IDtrService
    {
        void GetWorkHour(DailyTimeRecord dtr, int gracePeriod = 15);
        void GetUnderTime(DailyTimeRecord dtr, int gracePeriod = 15);
        void GetTardiness(DailyTimeRecord dtr, int gracePeriod = 15);
        void GetOfficialBusiness(DailyTimeRecord dtr, OfficialBusiness ob);
        void GetHoliday(DailyTimeRecord dtr, Holiday holiday);
        void GenerateDtr(DailyTimeRecord dtr, List<DateTime> timelog);
    }
}
