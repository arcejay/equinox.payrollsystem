﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Extensions;

namespace Equinox.PayrollSystem.Module.Services
{
    public class WorkSheduleWith4TimeCheckDtrService : IDtrService
    {
        public void GetWorkHour(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            if (dtr.Employee.WorkSchedule?.TimeCheckItems.Count != 4) throw new ArgumentOutOfRangeException(nameof(dtr));

            var workSchedule = dtr.Employee.WorkSchedule;
            var def = default(DateTime);

            double workHours = 0;

            //TimeSpan t1;
            //TimeSpan t2;
            //TimeSpan t3;
            //TimeSpan t4;
            var tcItems = workSchedule.TimeCheckItems.OrderBy(x => x.SequenceId).ToArray();

            //var ts1 = tcItems[0].TimeCheck; // workSchedule.TimeCheckItems[0].TimeCheck;
            //var ts2 = tcItems[1].TimeCheck; // workSchedule.TimeCheckItems[1].TimeCheck;

            //var ts3 = tcItems[2].TimeCheck; //workSchedule.TimeCheckItems[2].TimeCheck;
            //var ts4 = tcItems[3].TimeCheck; // workSchedule.TimeCheckItems[3].TimeCheck;

            if (dtr.Time1 != def && dtr.Time2 != def && dtr.Time3 != def && dtr.Time4 != def)
            {
                
            }

            //AM login and logout
            if (dtr.Time1 != def && dtr.Time2 != def)
            {
                ////check time1 if greater than ts1 (employee is late)
                //if (dtr.Time1.TimeOfDay > ts1)
                //{
                //    t1 = dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod ? dtr.Time1.TimeOfDay : ts1;
                //}
                //else
                //{
                //    //ts1 > t1 (employee is early)
                //    t1 = ts1;
                //}

                ////employee logout later
                //if (dtr.Time2.TimeOfDay > ts2)
                //{
                //    t2 = ts2;
                //}
                //else
                //{
                //    //employee logout early
                //    t2 = dtr.Time2.TimeOfDay;
                //}
                //workHours += t2.Subtract(t1).TotalHours;

                workHours = GetAmWorkHours(dtr, gracePeriod);

            }

            //PM Login and Logout
            if (dtr.Time3 != def && dtr.Time4 != def)
            {
                //double offset = 0;

                ////employee is late, no grace period in the afternoon
                ////check first if logout in AM exist and use it as reference for late 
                ////computation
                //TimeSpan pmTimeInThreshold;

                //if (dtr.Time2 == def || dtr.Time2.TimeOfDay < ts2)
                //{
                //    pmTimeInThreshold = ts3;
                //}
                //else
                //{
                //    pmTimeInThreshold = dtr.Time2.TimeOfDay.Add(new TimeSpan(1, 0, 0));
                //}

                ////if (dtr.Time3.TimeOfDay > ts3)
                //if (dtr.Time3.TimeOfDay > pmTimeInThreshold)
                //{
                //    t3 = dtr.Time3.TimeOfDay;
                //    offset = pmTimeInThreshold.Subtract(ts2).TotalHours -1;
                //}
                //else
                //{
                //    t3 = ts3;
                //}

                
                //if (dtr.Time4.TimeOfDay > ts4)
                //{
                //    t4 = ts4;
                //}
                //else
                //{
                //    //employee logs out early
                //    t4 = dtr.Time4.TimeOfDay;
                //}

                ////offset - late in PMLogin
                //// (PMLogout - PMLogin) - offset
                ////var duration = ts4.Subtract(ts3).TotalHours;
                //var duration = t4.Subtract(t3).TotalHours;

                //workHours += duration + offset;
                workHours += GetPmWorkHours(dtr);
            }

            //double lwop = 0;
            ////lwop 
            //if (dtr.Time1 == def && dtr.Time2 == def && dtr.Date.DayOfWeek != DayOfWeek.Sunday)
            //{
            //    lwop += ts2.Subtract(ts1).TotalHours;
            //}

            //if (dtr.Time3 == def && dtr.Time4 == def && dtr.Date.DayOfWeek != DayOfWeek.Sunday)
            //{
            //    lwop += ts4.Subtract(ts3).TotalHours;
            //}

            dtr.Lwop = GetLwop(dtr);
            dtr.WorkHours = workHours;
        }

        private double GetLwop(DailyTimeRecord dtr)
        {
            var workSchedule = dtr.Employee.WorkSchedule;

            var def = default(DateTime);
            
            var tcItems = workSchedule.TimeCheckItems.OrderBy(x => x.SequenceId).ToArray();

            var ts1 = tcItems[0].TimeCheck; // workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = tcItems[1].TimeCheck; // workSchedule.TimeCheckItems[1].TimeCheck;

            var ts3 = tcItems[2].TimeCheck; //workSchedule.TimeCheckItems[2].TimeCheck;
            var ts4 = tcItems[3].TimeCheck; // workSchedule.TimeCheckItems[3].TimeCheck;

            double lwop = 0;
            //lwop 
            if (dtr.Time1 == def && dtr.Time2 == def && dtr.Date.DayOfWeek != DayOfWeek.Sunday)
            {
                lwop += ts2.Subtract(ts1).TotalHours;
            }

            if (dtr.Time3 == def && dtr.Time4 == def && dtr.Date.DayOfWeek != DayOfWeek.Sunday)
            {
                lwop += ts4.Subtract(ts3).TotalHours;
            }

            return lwop;
        }

        private double GetAmWorkHours(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            var workSchedule = dtr.Employee.WorkSchedule;

            //var def = default(DateTime);

            //double workHours = 0;

            TimeSpan t1;
            TimeSpan t2;

            var tcItems = workSchedule.TimeCheckItems.OrderBy(x => x.SequenceId).ToArray();

            var ts1 = tcItems[0].TimeCheck; // workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = tcItems[1].TimeCheck; // workSchedule.TimeCheckItems[1].TimeCheck;

            if (dtr.Time1.TimeOfDay > ts1)
            {
                t1 = dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod ? dtr.Time1.TimeOfDay : ts1;
            }
            else
            {
                //ts1 > t1 (employee is early)
                t1 = ts1;
            }

            //employee logout later
            if (dtr.Time2.TimeOfDay > ts2)
            {
                t2 = ts2;
            }
            else
            {
                //employee logout early
                t2 = dtr.Time2.TimeOfDay;
            }

            return t2.Subtract(t1).TotalHours;
        }

        private double GetPmWorkHours(DailyTimeRecord dtr)
        {
            var workSchedule = dtr.Employee.WorkSchedule;

            var def = default(DateTime);

            //double workHours = 0;

            TimeSpan t3;
            TimeSpan t4;
            var tcItems = workSchedule.TimeCheckItems.OrderBy(x => x.SequenceId).ToArray();

            var ts2 = tcItems[1].TimeCheck; // workSchedule.TimeCheckItems[1].TimeCheck;

            var ts3 = tcItems[2].TimeCheck; //workSchedule.TimeCheckItems[2].TimeCheck;
            var ts4 = tcItems[3].TimeCheck; // workSchedule.TimeCheckItems[3].TimeCheck;


            double offset = 0;

            //employee is late, no grace period in the afternoon
            //check first if logout in AM exist and use it as reference for late 
            //computation
            TimeSpan pmTimeInThreshold;

            if (dtr.Time2 == def || dtr.Time2.TimeOfDay < ts2)
            {
                pmTimeInThreshold = ts3;
            }
            else
            {
                pmTimeInThreshold = dtr.Time2.TimeOfDay.Add(new TimeSpan(1, 0, 0));
            }

            //if (dtr.Time3.TimeOfDay > ts3)
            if ( dtr.Time3.TimeOfDay > pmTimeInThreshold)
            {
                t3 = dtr.Time3.TimeOfDay;
                offset = pmTimeInThreshold.Subtract(ts2).TotalHours - 1;
            }
            else
            {
                t3 = ts3;
            }


            if (dtr.Time4.TimeOfDay > ts4)
            {
                t4 = ts4;
            }
            else
            {
                //employee logs out early
                t4 = dtr.Time4.TimeOfDay;
            }

            //offset - late in PMLogin
            // (PMLogout - PMLogin) - offset
            //var duration = ts4.Subtract(ts3).TotalHours;
            var duration = t4.Subtract(t3).TotalHours;

            return duration + offset;

        }

        public void GetUnderTime(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            if (dtr.Employee.WorkSchedule?.TimeCheckItems.Count != 4) throw new ArgumentOutOfRangeException(nameof(dtr));

            var def = default(DateTime);

            var workSchedule = dtr.Employee.WorkSchedule;

            double result = 0;

            var tcItems = workSchedule.TimeCheckItems.OrderBy(x => x.SequenceId).ToArray();

            var ts1 = tcItems[0].TimeCheck; // workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = tcItems[1].TimeCheck; // workSchedule.TimeCheckItems[1].TimeCheck;

            var ts3 = tcItems[2].TimeCheck; //workSchedule.TimeCheckItems[2].TimeCheck;
            var ts4 = tcItems[3].TimeCheck; // workSchedule.TimeCheckItems[3].TimeCheck;

            if (dtr.Time1 != def && dtr.Time3 != def)
            {
                //AM Logout early
                if (dtr.Time2 != def && dtr.Time2.TimeOfDay < ts2)
                {
                    result += ts2.Subtract(dtr.Time2.TimeOfDay).TotalHours;
                }

                //PM Logout early
                if (dtr.Time4 != def && dtr.Time4.TimeOfDay < ts4)
                {
                    result += ts4.Subtract(dtr.Time4.TimeOfDay).TotalHours;
                }
            }

            //PM
            //no AM TimeOut
            if (dtr.Time1.TimeOfDay != default(TimeSpan) && dtr.Time2.TimeOfDay == default(TimeSpan))
            {
                result += ts2.Subtract(ts1).TotalHours;
            }

            //no PM TimeOut
            if (dtr.Time3.TimeOfDay != default(TimeSpan) && dtr.Time4.TimeOfDay == default(TimeSpan))
            {
                result += ts4.Subtract(ts3).TotalHours;
            }
            dtr.UnderTime = result;
        }

        public void GetTardiness(DailyTimeRecord dtr, int gracePeriod = 15)
        {
            if (dtr.Employee.WorkSchedule?.TimeCheckItems.Count != 4) throw new ArgumentOutOfRangeException(nameof(dtr));

            var def = default(DateTime);

            var workSchedule = dtr.Employee.WorkSchedule;

            double result = 0;

            var tcItems = workSchedule.TimeCheckItems.OrderBy(x => x.SequenceId).ToArray();

            var ts1 = tcItems[0].TimeCheck; // workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = tcItems[1].TimeCheck; // workSchedule.TimeCheckItems[1].TimeCheck;

            var ts3 = tcItems[2].TimeCheck; //workSchedule.TimeCheckItems[2].TimeCheck;
            var ts4 = tcItems[3].TimeCheck; // workSchedule.TimeCheckItems[3].TimeCheck;


            //var ts1 = workSchedule.TimeCheckItems[0].TimeCheck;
            //var ts2 = workSchedule.TimeCheckItems[1].TimeCheck;
            //var ts3 = workSchedule.TimeCheckItems[2].TimeCheck;
            //var ts4 = workSchedule.TimeCheckItems[3].TimeCheck;

            //AM Tardiness
            if (dtr.Time1 != def && dtr.Time2 != def)
            {
                var gp = ts1.Add(new TimeSpan(0, 0, gracePeriod, 0));

                if (dtr.Time1.TimeOfDay > gp)
                {
                    result += dtr.Time1.TimeOfDay.Subtract(ts1).TotalHours;
                }
            }

            //PM Tardiness
            if (dtr.Time3 != def && dtr.Time4 != def)
            {
                //if am checkout is undertime,  then use ts3 as reference
                //threshold is the AM TimeOut
                var pmTimeInThreshold = (dtr.Time2 != def && dtr.Time2.TimeOfDay > ts2 ) ? dtr.Time2.AddHours(1).TimeOfDay : ts3;

                //employee is late
                if (dtr.Time3.TimeOfDay > pmTimeInThreshold)
                {
                    result += dtr.Time3.TimeOfDay.Subtract(pmTimeInThreshold).TotalHours;
                }
            }

            //PM
            //no AM TimIn
            if (dtr.Time1 == def && dtr.Time2 != def)
            {
                result += ts2.Subtract(ts1).TotalHours;
            }

            //no PM TimeIn
            if (dtr.Time3 == def && dtr.Time4 != def)
            {
                result += ts4.Subtract(ts3).TotalHours;
            }

            dtr.Tardiness = result;
        }

        /// <summary>
        /// Update official business handling 4 scenarios of OB:
        /// 1. Full day 
        /// 2. AM Only (no login, no logout, or both)
        /// 3. PM Only (no login, no logout, or both)
        /// 4. AM and PM
        /// Algorithm:
        /// 
        /// </summary>
        /// <param name="dtr"></param>
        /// <param name="obj"></param>
        public void GetOfficialBusiness(DailyTimeRecord dtr, OfficialBusiness obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));

            if (dtr.Employee?.WorkSchedule?.TimeCheckItems?.Count != 4)
                throw new ArgumentOutOfRangeException(nameof(dtr.Employee.WorkSchedule.TimeCheckItems));
            
            var def = default(TimeSpan);

            //double tolerance = 0.001;

            double late = 0;
            double wh = 0;
            double ut = 0;
            double lwop = 0;
            double workhours = 0;
            
            var tc = dtr.Employee.WorkSchedule.TimeCheckItems.OrderBy(x=>x.SequenceId).ToArray();

            var gracePeriod = tc[0].TimeCheck.Add(new TimeSpan(0, 15, 0));

            var startTime = obj.StartTime;
            var endTime = obj.EndTime;
            //double ob = 0;

            var totalHours = endTime.Subtract(startTime).TotalHours;

            //if (totalHours > 8)
            //{
            //    totalHours -= 1;
            //}

            if (startTime < tc[1].TimeCheck && endTime > tc[2].TimeCheck)
            {
                totalHours -= 1;
            }

            //1. Full day
            if (obj.TimeDuration == ObTimeDuration.WholeDay)
            {
                dtr.OfficialBusiness = 8;
                dtr.UnderTime = 0;
                dtr.Tardiness = 0;
                dtr.Lwop = 0;
                dtr.WorkHours = 0;
                return;
            }
            
            #region "OB is AM"
            if (obj.TimeDuration == ObTimeDuration.AmOnly)
            {
                //lwop
                //check if ABSENT in PM
                if (dtr.Time3.TimeOfDay == def && dtr.Time4.TimeOfDay == def)
                {
                    lwop += tc[3].TimeCheck.Subtract(tc[2].TimeCheck).TotalHours;
                }
                else
                {
                    //late in PM
                    if (dtr.Time3.TimeOfDay != def)
                    {
                        if (dtr.Time3.TimeOfDay > tc[2].TimeCheck)
                        {
                            late += dtr.Time3.TimeOfDay.Subtract(tc[2].TimeCheck).TotalHours;
                        }
                    }
                    else
                    {
                        late += tc[3].TimeCheck.Subtract(tc[2].TimeCheck).TotalHours;
                    }

                    //undertime in PM
                    if (dtr.Time4.TimeOfDay != def)
                    {
                        if (dtr.Time4.TimeOfDay < tc[3].TimeCheck)
                        {
                            ut += tc[3].TimeCheck.Subtract(dtr.Time4.TimeOfDay).TotalHours;
                        }
                    }
                    else
                    {
                        ut += tc[3].TimeCheck.Subtract(tc[2].TimeCheck).TotalHours;
                    }

                    workhours = GetPmWorkHours(dtr);
                }


                //work hours in PM

                dtr.Lwop = lwop;
                dtr.UnderTime = ut;
                dtr.Tardiness = late;
                dtr.OfficialBusiness = totalHours;
                dtr.WorkHours = workhours;

                return;
            }
#endregion
            
            #region "OB is PM"
            if (obj.TimeDuration == ObTimeDuration.PmOnly)
            {
                //lwop
                //check if ABSENT in AM
                if (dtr.Time1.TimeOfDay == def && dtr.Time2.TimeOfDay == def)
                {
                    lwop += tc[1].TimeCheck.Subtract(tc[0].TimeCheck).TotalHours;
                }
                
                //late in AM
                if (dtr.Time1.TimeOfDay != def)
                {
                    if (dtr.Time1.TimeOfDay > gracePeriod)
                    {
                        late += dtr.Time1.TimeOfDay.Subtract(tc[0].TimeCheck).TotalHours;
                    }
                }
                else
                {
                    late += tc[1].TimeCheck.Subtract(tc[0].TimeCheck).TotalHours;
                }

                //undertime in AM
                if (dtr.Time2.TimeOfDay != def)
                {
                    if (dtr.Time2.TimeOfDay < tc[1].TimeCheck)
                    {
                        ut += tc[1].TimeCheck.Subtract(dtr.Time2.TimeOfDay).TotalHours;
                    }
                }
                else
                {
                    ut += tc[1].TimeCheck.Subtract(tc[0].TimeCheck).TotalHours;
                }

                dtr.Lwop = lwop;
                dtr.UnderTime = ut;
                dtr.Tardiness = late;
                dtr.OfficialBusiness = totalHours;
                dtr.WorkHours = GetAmWorkHours(dtr); 

                return;
            }
            #endregion

            #region "OB covers AM and PM
            //OB may cover AM only, PM only  or covers AM and PM
            if (obj.TimeDuration == ObTimeDuration.Custom)
            {
                wh = dtr.WorkHours;

                //check if OB covers AM and PM
                if (startTime < tc[1].TimeCheck && endTime > tc[2].TimeCheck)
                {
                    //ok, OB covers AM and PM


                    //late & workhours in AM
                    if (dtr.Time1.TimeOfDay != def)
                    if (dtr.Time1.TimeOfDay != def)
                    {
                        TimeSpan timein;
                        if (dtr.Time1.TimeOfDay > gracePeriod)
                        {
                            late += dtr.Time1.TimeOfDay.Subtract(tc[0].TimeCheck).TotalHours;
                            timein = dtr.Time1.TimeOfDay;
                        }
                        else
                        {
                            timein = tc[0].TimeCheck;
                        }
                        
                        workhours = startTime.Subtract(timein).TotalHours;
                    }

                    TimeSpan timeout;
                    //undertime and workhours in PM
                    if (dtr.Time4.TimeOfDay != def && dtr.Time4.TimeOfDay < tc[3].TimeCheck)
                    {
                        ut += tc[3].TimeCheck.Subtract(dtr.Time4.TimeOfDay).TotalHours;
                        timeout = dtr.Time4.TimeOfDay;
                    }
                    else
                    {
                        timeout = tc[3].TimeCheck;
                    }

                    workhours += timeout.Subtract(endTime).TotalHours;

                }
                else
                {
                    //check if OB is AM Only
                    if (endTime < tc[1].TimeCheck)
                    {
                        //late in PM
                        if (dtr.Time3.TimeOfDay != def)
                        {
                            if (dtr.Time3.TimeOfDay > tc[2].TimeCheck)
                            {
                                late += dtr.Time3.TimeOfDay.Subtract(tc[3].TimeCheck).TotalHours;
                            }
                        }
                        else
                        {
                            late += tc[3].TimeCheck.Subtract(tc[2].TimeCheck).TotalHours;
                        }

                        //undertime in PM
                        if (dtr.Time4.TimeOfDay != def)
                        {
                            if (dtr.Time4.TimeOfDay < tc[3].TimeCheck)
                            {
                                ut += tc[3].TimeCheck.Subtract(dtr.Time4.TimeOfDay).TotalHours;
                            }
                        }
                        else
                        {
                            ut += tc[3].TimeCheck.Subtract(tc[2].TimeCheck).TotalHours;
                        }
                    }

                    //check if OB is PM
                    if (startTime > tc[2].TimeCheck)
                    {
                        //PM
                        //lwop
                        //check if ABSENT in AM
                        if (dtr.Time1.TimeOfDay == def && dtr.Time2.TimeOfDay == def)
                        {
                            lwop += tc[1].TimeCheck.Subtract(tc[0].TimeCheck).TotalHours;
                        }

                        //late in AM
                        if (dtr.Time1.TimeOfDay != def)
                        {
                            if (dtr.Time1.TimeOfDay > gracePeriod)
                            {
                                late += dtr.Time1.TimeOfDay.Subtract(tc[0].TimeCheck).TotalHours;
                            }
                        }
                        else
                        {
                            late += tc[1].TimeCheck.Subtract(tc[0].TimeCheck).TotalHours;
                        }

                        //undertime in AM
                        if (dtr.Time2.TimeOfDay != def)
                        {
                            if (dtr.Time2.TimeOfDay < tc[1].TimeCheck)
                            {
                                ut += tc[1].TimeCheck.Subtract(dtr.Time2.TimeOfDay).TotalHours;
                            }
                        }
                        else
                        {
                            ut += tc[1].TimeCheck.Subtract(tc[0].TimeCheck).TotalHours;
                        }

                        //undertime in PM
                        if (dtr.Time4.TimeOfDay < tc[3].TimeCheck)
                        {
                            ut += tc[3].TimeCheck.Subtract(dtr.Time4.TimeOfDay).TotalHours;
                        }
                    }
                }
            }

#endregion

            dtr.Lwop = lwop;
            dtr.UnderTime = ut;
            dtr.Tardiness = late;
            dtr.OfficialBusiness = totalHours;
            dtr.WorkHours = workhours;
        }

        private void GetDuration(DailyTimeRecord dtr, OfficialBusiness ob, out double late, out double ut, out double wh, out double obhours)
        {
            late=ut=wh=obhours = 0;
            obhours = ob.EndTime.Subtract(ob.StartTime).TotalHours;

        }

        public void GetHoliday(DailyTimeRecord dtr, Holiday holiday)
        {
            var wh = dtr.WorkHours;

            dtr.IsHoliday = true;
            dtr.HolidayMultiplier = holiday.HolidayType == HolidayType.RegularHoliday ? 1 : 0.30;
            dtr.Lwop = 0;

            if (holiday.HolidayType == HolidayType.SpecialNonWorkingDay )
            {
                dtr.Holiday = dtr.WorkHours;

                return;
            }

            dtr.Holiday = 8;
        }
        
        public void GenerateDtr(DailyTimeRecord dtr, List<DateTime> logs)
        {
            if (dtr == null) throw new ArgumentNullException(nameof(dtr));

            if (dtr.Employee?.WorkSchedule?.TimeCheckItems?.Count != 4)
                throw new ArgumentOutOfRangeException(nameof(dtr.Employee.WorkSchedule.TimeCheckItems));

            var def = new TimeSpan(12,0,0);

            var timeChecks = dtr.Employee.WorkSchedule.TimeCheckItems.Select(x => x.TimeCheck)
                        .OrderBy(x => x.TotalMinutes)
                        .ToList();
            
            dtr.Date = logs[0].Date;

            var props = dtr.GetType()
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(x => x.Name.Contains("Time"))
                    .OrderBy(x => x.Name).ToArray();

            var l = logs.Select(x => x.TimeOfDay).ToList();

            if (logs.Count == 4)
            {
                //process 4 logins that equates to a half-day only
                //check if timein and timeout is within lunch break
                
                //check the difference of first 2 logins if less than an hour
                //then 
                if (logs[0].TimeOfDay > timeChecks[1] && logs[0].TimeOfDay < timeChecks[2] &&
                    logs[1].TimeOfDay > timeChecks[1] && logs[1].TimeOfDay < timeChecks[2] 
                    )
                {
                    dtr.Time2 = logs[0];
                    dtr.Time3 = logs[1];

                    if (logs[3].TimeOfDay > timeChecks[3])
                    {
                        dtr.Time4 = logs[2];
                    }
                    else
                    {
                        dtr.Time4 = logs[3];
                    }
                }
                else
                {
                    dtr.Time2 = logs[1];
                    dtr.Time1 = logs[0];
                    dtr.Time3 = logs[2];
                    dtr.Time4 = logs[3];
                }
            }
            else if (logs.Count == 3)
            {
                var list1 = new[] {timeChecks[0], timeChecks[1]}.ToList();

                //check if the first two are between 12-1
                if (l[1] > timeChecks[1] && l[2] < timeChecks[2])
                {
                    if (l[0] < timeChecks[1])
                    {
                        dtr.Time1 = logs[0];
                        dtr.Time2 = logs[1];
                        dtr.Time3 = logs[2];
                    }
                }
                else
                {
                    var index1 = l[0].GetNearestTime(list1);

                    if (index1 == 0)
                    {
                        dtr.Time1 = logs[0];

                        if (l[1] > timeChecks[1] && l[2] < timeChecks[2])
                        {
                            dtr.Time2 = logs[1];
                            dtr.Time3 = logs[2];
                        }
                        else
                        {

                            list1 = new[] { timeChecks[1], timeChecks[2] }.ToList();

                            var index2 = l[1].GetNearestTime(list1);

                            if (index2 == 0)
                            {
                                dtr.Time2 = logs[1];

                                list1 = new[] { timeChecks[2], timeChecks[3] }.ToList();
                                var index3 = l[2].GetNearestTime(list1);
                                if (index3 == 0)
                                {
                                    dtr.Time3 = logs[2];
                                }
                                else
                                {
                                    dtr.Time4 = logs[2];
                                }
                            }
                            else
                            {
                                dtr.Time3 = logs[1];
                                dtr.Time4 = logs[2];
                            }
                        }
                    }
                    else
                    {
                        dtr.Time2 = logs[0];
                        dtr.Time3 = logs[1];
                        dtr.Time4 = logs[2];
                    }
                }
            }
            else if (logs.Count == 2)
            {
                var isPm = l.TrueForAll(x => x > def);

                if (isPm)
                {
                    dtr.Time3 = logs[0];
                    dtr.Time4 = logs[1];
                }
                else
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        var index = l[i].GetNearestTime(timeChecks);
                        props[index].SetValue(dtr, logs[i]);
                    }
                }
            }
            else if (logs.Count == 1)
            {
                var index1 = logs[0].TimeOfDay.GetNearestTime(timeChecks);
                props[index1].SetValue(dtr, logs[0]);
            }
            
            else // 5 or more
            {
                //for (int i = 0; i < timeChecks.Count; i++)
                //{
                //    var index = timeChecks[i].GetNearestTime(l);

                //    props[index].SetValue(dtr, logs[index]);
                //}

                //get am log in and pm logout
                var list = logs;

                dtr.Time1 = list.First();
                dtr.Time4 = list.Last();
                list.Remove(list.First());
                list.Remove(list.Last());

                var list1 = list.Select(x => x.TimeOfDay).ToList();

                //get am logout
                var index1 = timeChecks[1].GetNearestTime(list1);

                dtr.Time2 = list[index1];

                list = list.Except(new[] {dtr.Time2}).ToList();
                list1 = list.Select(x => x.TimeOfDay).ToList();
                
                //pm login
                var index2 = timeChecks[2].GetNearestTime(list1);
                dtr.Time3 = list[index2];

            }
        }
    }
}