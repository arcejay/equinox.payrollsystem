﻿using System;
using System.Text;
using System.Linq;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Model.Core;
using DevExpress.ExpressApp.Model.DomainLogics;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Reports;

namespace Equinox.PayrollSystem.Module {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppModuleBasetopic.aspx.
    public sealed partial class PayrollSystemModule : ModuleBase {
        public PayrollSystemModule() {
            InitializeComponent();
			BaseObject.OidInitializationMode = OidInitializationMode.AfterConstruction;
        }
        public override IEnumerable<ModuleUpdater> GetModuleUpdaters(IObjectSpace objectSpace, Version versionFromDB) {
            ModuleUpdater updater = new DatabaseUpdate.Updater(objectSpace, versionFromDB);

            //PredefinedReportsUpdater predefinedReportsUpdater = new PredefinedReportsUpdater(Application, objectSpace, versionFromDB);
            //predefinedReportsUpdater.AddPredefinedReport<DailyTimeRecordReport>("Daily Time Records Report", typeof(DailyTimeRecord));

            return new ModuleUpdater[] { updater };
        }
        public override void Setup(XafApplication application) {
            base.Setup(application);
            // Manage various aspects of the application UI and behavior at the module level.

            //application.CustomProcessShortcut += (sender, args) =>
            //{
            //    if (args.Shortcut.ViewId == "CompanySettings_DetailView")
            //    {
            //        IObjectSpace objectSpace = Application.CreateObjectSpace();
            //        var companySettings = objectSpace.GetObject(typeof(CompanySettings));

            //        args.View = application.CreateDetailView(objectSpace, companySettings);
            //        args.View.AllowEdit["CanEditCompanySettings"] = true;
            //        args.Handled = true;
            //    }
            //};


            //SequenceGeneratorInitializer.Register(application);

        }
		public override void CustomizeTypesInfo(ITypesInfo typesInfo) {
			base.CustomizeTypesInfo(typesInfo);
			CalculatedPersistentAliasHelper.CustomizeTypesInfo(typesInfo);
		}
    }
}
