﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;

namespace Equinox.PayrollSystem.Module.Reports
{
    [DomainComponent, VisibleInReports]
    public class TardinessReport
    {
        public string Department { get; set; }

        public string Employee { get; set; }

        public double Tardy { get; set; }
    }
}
