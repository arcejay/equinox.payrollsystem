using System;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;

namespace Equinox.PayrollSystem.Module.Reports
{
    [NonPersistent]
    [VisibleInReports]
    public class MonthlySalaryReport : XPObject 
    {
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        public decimal Salary
        {
            get { return GetPropertyValue<decimal>("Salary"); }
            set { SetPropertyValue("Salary", value); }
        }
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        public decimal SssContribution
        {
            get { return GetPropertyValue<decimal>("SSSContribution"); }
            set { SetPropertyValue("SSSContribution", value); }
        }

        public decimal PhilHealth
        {
            get { return GetPropertyValue<decimal>("PhilHealth"); }
            set { SetPropertyValue("PhilHealth", value); }
        }

    }
}