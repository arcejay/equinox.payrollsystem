﻿using System;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.Models
{
    [DomainComponent]
    [NonPersistent]
    public class LeaveCreditModel : XPObject
    {
        public DateTime EntitlementDate
        {
            get { return GetPropertyValue<DateTime>("EntitlementDate"); }
            set { SetPropertyValue("EntitlementDate", value); }
        }

        [ImmediatePostData]
        public LeaveType LeaveType
        {
            get { return GetPropertyValue<LeaveType>("LeaveType"); }
            set
            {
                SetPropertyValue("LeaveType", value);

                UpdateDefaultLeaveEntitlement();
            }
        }

        [ModelDefault("DisplayFormat", "{0:####")]
        public int CreditYear
        {
            get { return GetPropertyValue<int>("CreditYear"); }
            set { SetPropertyValue("CreditYear", value); }
        }

        public MonthNameEnum CreditMonth
        {
            get { return GetPropertyValue<MonthNameEnum>("CreditMonth"); }
            set { SetPropertyValue("CreditMonth", value); }
        }

        [ImmediatePostData, ModelDefault("Caption", "Issue leave credits to all employees?")]
        public bool IsAllEmployee
        {
            get { return GetPropertyValue<bool>("IsAllEmployee"); }
            set
            {
                SetPropertyValue("IsAllEmployee", value);
            }
        }

        [Appearance("EmployeeVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "IsAllEmployee = true", Context = "DetailView")]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }


        [ToolTip("Leave credit in terms of # of days")]
        public double LeaveEntitlement { get; set; }

        public LeaveCreditModel(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            EntitlementDate = DateTime.Now;
            IsAllEmployee = true;
            LeaveType = LeaveType.VacationLeave;
            CreditYear = DateTime.Now.Year;
        }

        private void UpdateDefaultLeaveEntitlement()
        {
            var settings = Session.FindObject<CompanySettings>(null);
            switch (LeaveType)
            {
                case LeaveType.MaternityLeave:
                    LeaveEntitlement = settings.MaternityLeaveEntitlement/12;
                    break;
                case LeaveType.SickLeave:
                    LeaveEntitlement = settings.SickLeaveEntitlement/12;
                    break;
                case LeaveType.VacationLeave:
                    LeaveEntitlement = settings.VacationLeaveEntitlement/12;
                    break;
                case LeaveType.PaternityLeave:
                    LeaveEntitlement = settings.PaternityLeaveEntitlement/12;
                    break;
            }

            OnChanged("LeaveEntitlement");
        }
    }
}