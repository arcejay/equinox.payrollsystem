using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Generators;
using DevExpress.Xpo.Helpers;
using DevExpress.Xpo.Metadata;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.Models
{
    [NonPersistent]
    public class DeductionModel : XPObject
    {
        public DateTime DeductionDate
        {
            get { return GetPropertyValue<DateTime>("DeductionDate"); }
            set { SetPropertyValue("DeductionDate", value); }
        }

        [Size(20)]
        public string Note
        {
            get { return GetPropertyValue<string>("Note"); }
            set { SetPropertyValue("Note", value); }
        }

        [ImmediatePostData(true)]
        public DeductionType DeductionType
        {
            get { return GetPropertyValue<DeductionType>("DeductionType"); }
            set
            {
                SetPropertyValue("DeductionType", value);
                Note = DeductionType.ToString();

                Update(); 

            }
        }
        
        public List<DeductionItemModel> Employees { get; set; }

        public Payroll Payroll
        {
            get { return GetPropertyValue<Payroll>("Payroll"); }
            set { SetPropertyValue("Payroll", value); }
        }

        public DeductionModel(Session session) : base(session)
        {
            var collection = new XPQuery<Employee>(session);
            Employees = new List<DeductionItemModel>();
            foreach (var employee in collection)
            {
                Employees.Add(new DeductionItemModel(session) { Employee = employee });
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            DeductionDate = DateTime.Now;

            Payroll = Session.FindObject<Payroll>(null);

            Update();

        }

        private void Update()
        {
            //process if employees are selected
            //if (Payroll == null) return;

            //var schedule = Session.FindObject<SssContributionSummarySchedule>(new BinaryOperator("Status", Status.Active)).SssContributionItems;
            if (Employees == null) return;

            foreach (var item in Employees)
            {
                var deductionItemModel = item;
                var employee = deductionItemModel.Employee;
                decimal salary = 0;

                switch (DeductionType)
                {
                    case DeductionType.SSS:

                        //get the payroll items for current month
                        var month = Payroll.StartDate.Month;
                        var year = Payroll.StartDate.Year;

                        var payrollItems = employee.PayrollItems.Where(x => x.Payroll.StartDate.Month == month &&
                                                                            x.Payroll.StartDate.Year == year);

                        salary = payrollItems.Sum(x => x.TotalEarnings);

                        deductionItemModel.MonthlyGrossPay = salary;

                        var sss = Session.FindObject<SssContributionItem>(CriteriaOperator.Parse("? >= LowerLimit AND ? <= HigherLimit", salary, salary));

                        //var sss = schedule.First(x => x.LowerLimit >= salary && salary <= x.HigherLimit);
                        if (sss == null) break;

                        deductionItemModel.Amount = sss.EmployeeShare;
                        deductionItemModel.Description = "SSS Employee Share";
                        break;
                    case DeductionType.PhilHealth:

                        month = Payroll.StartDate.Month;
                        year = Payroll.StartDate.Year;
                        payrollItems = employee.PayrollItems.Where(x => x.Payroll.StartDate.Month == month &&
                                                                            x.Payroll.StartDate.Year == year);
                        salary = payrollItems.Sum(x => x.TotalEarnings);

                        deductionItemModel.MonthlyGrossPay = salary;
                        
                        var ph = Session.FindObject<PhilhealthContributionItem>(CriteriaOperator.Parse("? >=LowerLimit AND ? <=HigherLimit", salary, salary));

                        if (ph == null) break;

                        deductionItemModel.Amount = ph.EmployeeShare;
                        deductionItemModel.Description = "PhilHealth Employee Share";
                        break;
                    case DeductionType.PagIbig:
                        deductionItemModel.Description = "HDMF Employee Share";
                        break;
                    case DeductionType.Contribution:
                        deductionItemModel.Description = DeductionType.ToString();
                        deductionItemModel.Description = "Other Contrib Share";
                        break;
                    case DeductionType.Insurance:
                        deductionItemModel.Description = DeductionType.ToString();
                        break;
                }
            }
        }

    }
}