﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.Models
{
    [DomainComponent]
    public class ExtractLogsFromDeviceModel 
    {
        public string DeviceName { get; set; }
        public int DeviceId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [ModelDefault("DisplayFormat", "{0:g}")]  
        public string IpAddress { get; set; }

        [ModelDefault("DisplayFormat", "{0:d}")]
        public int Port { get; set; }
        public DeviceStatus DeviceStatus { get; set; }

        public ExtractLogsFromDeviceModel()
        {
            DateTime date1, date2;

            HelperClass.SetDateRange(DateTime.Now, out date1,out date2);

            StartDate = date1;
            EndDate = date2;
        }
        
    }
}
