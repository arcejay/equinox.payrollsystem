﻿using DevExpress.ExpressApp.DC;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.Models
{
    [NonPersistent]
    public class DailyTimeRecordModel : DateRangeModel
    {
        public DtrStatus Status
        {
            get { return GetPropertyValue<DtrStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        public bool IncludeEmployeeWithoutTimeLogs
        {
            get { return GetPropertyValue<bool>("IncludeEmployeeWithoutTimeLogs"); }
            set { SetPropertyValue("IncludeEmployeeWithoutTimeLogs", value); }
        }


        public DailyTimeRecordModel(Session session) : base(session)
        {
        }
    }
}