using System;
using System.Collections.Generic;
using DevExpress.Data.Filtering;
using DevExpress.Data.Helpers;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.Models
{
    [NonPersistent]
    public class EmployeeItemModel : XPObject
    {
        [VisibleInListView(false)]
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        public EmployeeItemModel(Session session) : base(session)
        {

        }
    }
}