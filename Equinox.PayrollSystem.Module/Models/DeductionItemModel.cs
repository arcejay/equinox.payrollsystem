using DevExpress.ExpressApp;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;

namespace Equinox.PayrollSystem.Module.Models
{
    [DefaultListViewOptions(true, NewItemRowPosition.Top)]
    [NonPersistent]
    public class DeductionItemModel : XPObject
    {
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set
            {
                SetPropertyValue("Employee", value);
            }
        }

        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }


        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }

        public decimal MonthlyGrossPay
        {
            get { return GetPropertyValue<decimal>("MonthlyGrossPay"); }
            set { SetPropertyValue("MonthlyGrossPay", value); }
        }

        public DeductionItemModel(Session session) : base(session)
        {

        }
    }
}