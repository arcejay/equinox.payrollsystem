using System.Collections.Generic;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;

namespace Equinox.PayrollSystem.Module.Models
{
    [NonPersistent]
    public class ObModel : XPObject
    {
        public OfficialBusiness OfficialBusiness
        {
            get { return GetPropertyValue<OfficialBusiness>("OfficialBusiness"); }
            set { SetPropertyValue("OfficialBusiness", value); }
        }

        public List<EmployeeItemModel> Employees { get; set; }

        public ObModel(Session session) : base(session)
        {
            var collection = new XPQuery<Employee>(session);
            Employees = new List<EmployeeItemModel>();

            foreach (var e in collection)
            {
                Employees.Add(new EmployeeItemModel(session)
                {
                    Id = e.Oid,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    MiddleName = e.MiddleName
                });
            }
        }
    }
}