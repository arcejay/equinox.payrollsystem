using System;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;

namespace Equinox.PayrollSystem.Module.Models
{
    [NonPersistent]
    public class CashAdvanceModel : XPObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        [ImmediatePostData]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        public string ReferenceNumber
        {
            get { return GetPropertyValue<string>("ReferenceNumber"); }
            set { SetPropertyValue("ReferenceNumber", value); }
        }

        public CashAdvance CashAdvance
        {
            get { return GetPropertyValue<CashAdvance>("CashAdvance"); }
            set { SetPropertyValue("CashAdvance", value); }
        }

        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }

        [ImmediatePostData]
        public Payroll Payroll
        {
            get { return GetPropertyValue<Payroll>("Payroll"); }
            set { SetPropertyValue("Payroll", value); }
        }

        [DataSourceProperty("Payroll.PayrollItems", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Employee.Oid = '@This.Employee.Oid'")]
        public PayrollItem PayrollItem
        {
            get { return GetPropertyValue<PayrollItem>("PayrollItem"); }
            set { SetPropertyValue("PayrollItem", value); }
        }
        
        public CashAdvanceModel(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Date = DateTime.Now;
        }
    }
}