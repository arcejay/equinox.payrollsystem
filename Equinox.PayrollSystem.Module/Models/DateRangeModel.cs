﻿using System;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.PayrollSystem.Module.Models
{
    [NonPersistent]
    public class DateRangeModel : XPObject
    {
        [ImmediatePostData]
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set
            {
                SetPropertyValue("Date", value);
            }
        }

        public DateTime StartDate
        {
            get { return GetPropertyValue<DateTime>("StartDate"); }
            set { SetPropertyValue("StartDate", value); }
        }
        public DateTime EndDate
        {
            get { return GetPropertyValue<DateTime>("EndDate"); }
            set { SetPropertyValue("EndDate", value); }
        }

        public DateRangeModel(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Date = DateTime.Now;
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "Date")
            {
                DateTime startDate, endDate;
                HelperClass.SetDateRange(Date, out startDate, out endDate);
                StartDate = startDate;
                EndDate = endDate;
            }
        }
    }
}