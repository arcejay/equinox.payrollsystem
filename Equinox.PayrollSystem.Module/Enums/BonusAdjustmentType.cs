﻿using DevExpress.ExpressApp.DC;

namespace Equinox.PayrollSystem.Module.Enums
{
    public enum BonusAdjustmentType
    {
        Incentive,
        SalaryAdjustment,
        [XafDisplayName("13th Month Pay")]
        ThirteenthMonthPay,
        [XafDisplayName("OT")]
        OverTime,
        BirthdayGift
    }
}