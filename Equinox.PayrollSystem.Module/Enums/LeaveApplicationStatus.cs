namespace Equinox.PayrollSystem.Module.Enums
{
    public enum LeaveApplicationStatus
    {
        InProgress, Approved, Denied, Cancelled
    }
}