﻿namespace Equinox.PayrollSystem.Module.Enums
{
    public enum BonusAdjustmentStatus
    {
        Pending,
        Approved,
        Cancelled,
        Released
    }

    public enum MonthNameEnum
    {
        January,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }
}