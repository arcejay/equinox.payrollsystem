using DevExpress.ExpressApp.DC;

namespace Equinox.PayrollSystem.Module.Enums
{
    public enum Status
    {
        Active,
        [XafDisplayName("InActive")]
        Inactive
    }
}