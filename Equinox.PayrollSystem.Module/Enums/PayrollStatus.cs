namespace Equinox.PayrollSystem.Module.Enums
{
    public enum PayrollStatus
    {
        InProgress, Approved, Completed, Cancelled
    }
}