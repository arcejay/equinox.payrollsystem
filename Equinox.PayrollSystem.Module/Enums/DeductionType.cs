﻿using DevExpress.ExpressApp.DC;

namespace Equinox.PayrollSystem.Module.Enums
{
    public enum DeductionType
    {
        [XafDisplayName("SSS Contribution")]
        SSS,
        [XafDisplayName("PhilHealth Contribution")]
        PhilHealth,
        [XafDisplayName("HDMF Contribution")]
        PagIbig,
        Insurance,
        [XafDisplayName("Other Contribution")]
        Contribution,
        CashAdvance

    }
}