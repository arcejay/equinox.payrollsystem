namespace Equinox.PayrollSystem.Module.Enums
{
    public enum DtrStatus
    {
        NotProcessed,
        Processed,
        Approved
    }
}