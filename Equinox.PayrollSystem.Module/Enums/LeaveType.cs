namespace Equinox.PayrollSystem.Module.Enums
{
    public enum LeaveType
    {
        NotSpecified,
        SickLeave,
        VacationLeave,
        MaternityLeave,
        PaternityLeave
    }
}