﻿namespace Equinox.PayrollSystem.Module.Enums
{
    public enum PayrollItemStatus
    {
        Pending,
        Released
    }
}