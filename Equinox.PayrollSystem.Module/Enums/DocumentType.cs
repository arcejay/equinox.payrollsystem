namespace Equinox.PayrollSystem.Module.Enums
{
    public enum DocumentType
    {
        Screenshot = 1, WordDocument = 2, AdobePdf = 3
    };
}