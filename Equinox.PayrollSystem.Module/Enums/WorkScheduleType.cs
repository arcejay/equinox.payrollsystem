using DevExpress.ExpressApp.DC;

namespace Equinox.PayrollSystem.Module.Enums
{
    public enum WorkScheduleType
    {
        [XafDisplayName("4 TimeCheck")]
        FourTimeCheck,
        [XafDisplayName("2 TimeCheck")]
        TwoTimeCheck,
        [XafDisplayName("6 TimeCheck")]
        SixTimeCheck
    }
}