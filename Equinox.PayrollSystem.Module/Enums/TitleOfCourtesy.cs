namespace Equinox.PayrollSystem.Module.Enums
{
    public enum TitleOfCourtesy
    {
        Mr, Miss, Mrs, Engr, Dr, Atty, Arch
    }
}