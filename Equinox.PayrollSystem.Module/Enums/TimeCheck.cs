namespace Equinox.PayrollSystem.Module.Enums
{
    public enum TimeCheck
    {
        Time1 = 0,
        Time2 = 1,
        Time3 = 2,
        Time4 = 3,
        Time5 = 4,
        Time6 = 5,
    }
}