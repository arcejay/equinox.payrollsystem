namespace Equinox.PayrollSystem.Module.Enums
{
    public enum EmploymentStatus
    {
        Trainee, Contractual, Regular, OnLeave, Resigned 
    }
}