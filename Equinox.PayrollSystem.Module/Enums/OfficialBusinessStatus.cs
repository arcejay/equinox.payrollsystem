namespace Equinox.PayrollSystem.Module.Enums
{
    public enum OfficialBusinessStatus
    {
        Pending, DisApproved, Approved
    }
}