﻿namespace Equinox.PayrollSystem.Module.Enums
{
    public enum CashAdvanceStatus
    {
        InProgress,
        Completed
    }
}