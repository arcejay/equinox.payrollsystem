namespace Equinox.PayrollSystem.Module.Enums
{
    public enum DeviceStatus
    {
        Online, Offline
    }
}