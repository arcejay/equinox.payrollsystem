﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.Data.Helpers;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Models;

namespace Equinox.PayrollSystem.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class GeneratePayrollViewController : ViewController
    {
        private int _count = 0;

        public GeneratePayrollViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            //var payroll =(Payroll) View.CurrentObject;
            //var objectSpace = Application.CreateObjectSpace();
            //var totalEmployeeCount = objectSpace.GetObjectsCount(typeof (Employee),
            //    CriteriaOperator.Parse("*"));
            //ListEditor listEditor = ((ListView)View).Editor;
            //GridControl gridControl = (GridControl)listEditor.Control;

            //((ListView)View).Editor.AllowEdit = true;

            TotalHoursPayrollAction.ExecuteCompleted += TotalHoursPayrollAction_ExecuteCompleted;
            LeavePayrollAction.ExecuteCompleted += LeavePayrollAction_ExecuteCompleted;
            ReleaseSalaryPayrollAction.ExecuteCompleted += ReleaseSalaryPayrollAction_ExecuteCompleted;
            ReleaseSalaryPayrollAction.TargetObjectsCriteria = "PayrollStatus == 'Approved'";
            ReleaseSalaryPayrollAction.ConfirmationMessage = "Do you wish to release the salary for selected employee?";

        }

        private void TotalHoursPayrollAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            if (_count > 0)
            {
                XtraMessageBox.Show(_count + " payroll items were processed successfully.");
                return;
            }

            XtraMessageBox.Show("No items were processed.");
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            TotalHoursPayrollAction.ExecuteCompleted -= TotalHoursPayrollAction_ExecuteCompleted;
            LeavePayrollAction.ExecuteCompleted -= LeavePayrollAction_ExecuteCompleted;

            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void GeneratePayrollAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var listId = Application.FindListViewId(typeof (EmployeeItemModel));
            var employees = objectSpace.GetObjects<Employee>();

            var payroll = (Payroll) View.CurrentObject;

            var payrollItems = objectSpace.FindObject<Payroll>(new BinaryOperator("Oid", payroll.Oid)).PayrollItems.Select(x=>x.Employee);

            var source = new CollectionSource(objectSpace, typeof(EmployeeItemModel));

            var employeesToDisplay = employees.Except(payrollItems);

            foreach (Employee employee in employeesToDisplay)
            {
                var item = objectSpace.CreateObject<EmployeeItemModel>();
                item.Id = employee.BiometricId;
                item.FirstName = employee.FirstName;
                item.MiddleName = employee.MiddleName;
                item.LastName = employee.LastName;
                source.Add(item);
            }

            var view = Application.CreateListView(listId, source, false);
            view.Editor.AllowEdit = true;
            e.View = view;

        }

        private void GeneratePayrollAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var objectSpace = View.ObjectSpace;
            var selectedObjects = e.PopupWindowViewSelectedObjects;
            
            var payroll = objectSpace.GetObjectByKey<Payroll>(((Payroll) View.CurrentObject).Oid);

            foreach (EmployeeItemModel employee in selectedObjects)
            {
                if (payroll.PayrollItems.Where(x => x.Employee != null).Any(x => x.Employee.Oid == employee.Id))
                {
                    continue;
                }

                var payrollItem = objectSpace.CreateObject<PayrollItem>();
                var emp = objectSpace.FindObject<Employee>(new BinaryOperator("BiometricId", employee.Id));
                payrollItem.Employee = emp;

                //var dailyRate = (employee.SalaryGrade.Salary + employee.StepIncrement.Amount)*12/(312);

                //payrollItem.DailyRate = dailyRate;
                payroll.PayrollItems.Add(payrollItem);
            }

            objectSpace.CommitChanges();

            View.Refresh();
        }

        private void TotalHoursPayrollAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            _count = 0;

            var objectSpace = Application.CreateObjectSpace();

            var payroll = (Payroll)View.CurrentObject;


            var payrollItems = payroll.PayrollItems;

            //objectSpace.GetObjects<PayrollItem>(new BinaryOperator("Payroll.Oid", payroll.Oid));

            //associate DailyTimeRecord w/ payroll item
            foreach (var payrollItem in payrollItems)
            {
                var dtrs = payrollItem.Employee.DailyTimeRecords;
                dtrs.Filter = new BetweenOperator("Date", payroll.StartDate, payroll.EndDate);
                dtrs.Load();
                var hourlyRate = (double)payrollItem.DailyRate/8;

                payrollItem.TotalHours = dtrs.Sum(x => x.WorkHours);
                payrollItem.Tardiness = dtrs.Sum(x => x.Tardiness);
                payrollItem.UnderTime = dtrs.Sum(x => x.UnderTime);
                payrollItem.LeaveWithoutPay = dtrs.Sum(x => x.Lwop);
                payrollItem.Holiday = dtrs.Sum(x => x.Holiday);
                payrollItem.HolidayAmount = (decimal)dtrs.Sum(x =>x.Holiday * x.HolidayMultiplier * hourlyRate);
                payrollItem.OfficialBusiness = dtrs.Sum(x => x.OfficialBusiness);

                _count++;
            }

            objectSpace.CommitChanges();

            View.Refresh();
        }

        private void LeavePayrollAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            if (_count > 0)
            {
                XtraMessageBox.Show(_count + " leave applications processed successfuly!");
            }
        }

        private void LeavePayrollAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            _count = 0;

            var objectSpace = Application.CreateObjectSpace();

            var payroll = (Payroll)View.CurrentObject;

            var payrollItems = objectSpace.GetObjects<PayrollItem>(new BinaryOperator("Payroll.Oid", payroll.Oid));

            foreach (var payrollItem in payrollItems)
            {
                //var dtrs = payrollItem.Employee.DailyTimeRecords;
                //dtrs.Filter = new BetweenOperator("Date", payroll.StartDate, payroll.EndDate);
                //dtrs.Load();

                double leave = 0;

                var criteria = CriteriaOperator.Parse("StartDate >= ? AND EndDate <= ? AND Employee.Oid =? AND Status =?",
                    payroll.StartDate, payroll.EndDate, payrollItem.Employee.Oid, LeaveApplicationStatus.Approved);

                var leaveApplications = payrollItem.Employee.LeaveApplications;
                leaveApplications.Filter = criteria;
                leaveApplications.Load();

                if (leaveApplications.Count == 0) continue;

                foreach (var la in leaveApplications)
                {
                    leave += la.NumberOfDays;

                    _count++;
                }

                payrollItem.LeaveWithPay = leave;

            }

            objectSpace.CommitChanges();
        }

        private void ReleaseSalaryPayrollAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var parentView = ((DetailView)View).FindItem("PayrollItems") as ListPropertyEditor;

            var payrollItem = parentView?.ListView.SelectedObjects[0] as PayrollItem;

            if (payrollItem != null)
            {
                _count += 1;

                var payrollItemToEdit =
                    objectSpace.FindObject<PayrollItem>(new BinaryOperator("Oid", payrollItem.Oid));

                payrollItemToEdit.Status = PayrollItemStatus.Released;

                objectSpace.CommitChanges();
                View.Refresh();
            }
        }

        private void ReleaseSalaryPayrollAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            if (_count > 0)
            {
                XtraMessageBox.Show("Operation completed successfully!");
            }
        }
    }
}
