﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo.Helpers;
using DevExpress.XtraEditors;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Extensions;
using Equinox.PayrollSystem.Module.Models;
using Equinox.PayrollSystem.Module.Services;

namespace Equinox.PayrollSystem.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class OfficialBusinessViewController : ViewController
    {
        private int _count = 0;
        private int _employeeCount = 0;

        public OfficialBusinessViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            this.ProcessObAction.ExecuteCompleted += CalculateOfficialBusinessAction_ExecuteCompleted;

            ProcessObAction.TargetViewType = ViewType.ListView;
            ProcessObAction.TargetObjectType = typeof(DailyTimeRecord);
        }

        private void CalculateOfficialBusinessAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            XtraMessageBox.Show(_count + " records processed successfully.");
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View
            addObToEmployeesAction.ExecuteCompleted += AddObToEmployeesAction_ExecuteCompleted;
            addObToEmployeesAction.Execute += AddObToEmployeesAction_Execute;
            addObToEmployeesAction.CustomizePopupWindowParams += AddObToEmployeesAction_CustomizePopupWindowParams;
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            addObToEmployeesAction.ExecuteCompleted -= AddObToEmployeesAction_ExecuteCompleted;
            addObToEmployeesAction.Execute -= AddObToEmployeesAction_Execute;
            addObToEmployeesAction.CustomizePopupWindowParams -= AddObToEmployeesAction_CustomizePopupWindowParams;

            base.OnDeactivated();

        }

        private void ProcessObAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            e.View = Application.CreateListView(objectSpace, typeof(Payroll), true);
        }

        private void ProcessObAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var payroll = (Payroll)e.PopupWindowViewCurrentObject;

            var objectSpace = Application.CreateObjectSpace();
            var criteria = CriteriaOperator.Parse("StartDate >= ? AND EndDate < ?", payroll.StartDate.Date, payroll.EndDate.AddDays(1));

            var officialBusinessesToProcess = objectSpace.GetObjects<OfficialBusiness>(criteria, true);

            _count = 0;

            //get list of official business
            foreach (var ob in officialBusinessesToProcess)
            {
                var obj = ob;
                var employee = ob.Employee;

                //generate dates in official business
                var dates = HelperClass.GenerateDates(obj.StartDate, obj.EndDate);

                //foreach (var employee in obj.Employees)
                //{
                var dtrService = HelperClass.GetDtrService(employee);

                var crit = CriteriaOperator.And(new InOperator("Date", dates), new BinaryOperator("Employee", employee));

                var dtrs = objectSpace.GetObjects<DailyTimeRecord>(crit);

                if (dtrs != null)
                {
                    foreach (var dtr in dtrs)
                    {
                        dtrService.GetOfficialBusiness(dtr, obj);
                    }
                }
                else //employee has no DTR, but OB is specified, DTR must be created
                {
                    foreach (var date in dates)
                    {
                        var dtr = objectSpace.CreateObject<DailyTimeRecord>();
                        dtr.Employee = employee;
                        dtr.Date = date;

                        dtrService.GetOfficialBusiness(dtr, obj);
                    }
                }
                //}

                _count++;
            }

            objectSpace.CommitChanges();
        }

        private void AddObToEmployeesAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            XtraMessageBox.Show(_employeeCount + " records processed successfully.");
        }

        private void AddObToEmployeesAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var listId = Application.FindListViewId(typeof(EmployeeItemModel));
            var employees = objectSpace.GetObjects<Employee>();

            var currentObj = (OfficialBusiness)View.CurrentObject;

            var ob = objectSpace.FindObject<OfficialBusiness>(new BinaryOperator("Oid", currentObj.Oid));

            var exceptEmployees = new List<Employee>() { ob.Employee };

            var source = new CollectionSource(objectSpace, typeof(EmployeeItemModel));

            var employeesToDisplay = employees.Except(exceptEmployees);

            foreach (Employee employee in employeesToDisplay)
            {
                var item = objectSpace.CreateObject<EmployeeItemModel>();
                item.Id = employee.Oid;
                item.FirstName = employee.FirstName;
                item.MiddleName = employee.MiddleName;
                item.LastName = employee.LastName;
                source.Add(item);
            }

            var view = Application.CreateListView(listId, source, false);
            view.Editor.AllowEdit = true;
            e.View = view;

        }

        private void AddObToEmployeesAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var model = (OfficialBusiness)View.CurrentObject;
            var selectedEmployees = e.PopupWindowViewSelectedObjects;

            var ob = objectSpace.FindObject<OfficialBusiness>(new BinaryOperator("Oid", model.Oid));

            foreach (EmployeeItemModel emp in selectedEmployees)
            {
                var employee = objectSpace.FindObject<Employee>(new BinaryOperator("Oid", emp.Id));
                var payroll = ob.PayrollItem.Payroll;

                var payrollItem = objectSpace.FindObject<PayrollItem>(
                    CriteriaOperator.And(new BinaryOperator("Payroll.Oid", payroll.Oid),
                        new BinaryOperator("Employee.Oid", employee.Oid)));

                var newOb = objectSpace.CreateObject<OfficialBusiness>();
                newOb.Employee = employee;
                newOb.CustomerName = ob.CustomerName;
                newOb.StartDate = ob.StartDate;
                newOb.EndDate = ob.EndDate;
                newOb.StartTime = ob.StartTime;
                newOb.Status = ob.Status;
                newOb.Description = ob.Description;
                newOb.OfficialBusinessDate = ob.OfficialBusinessDate;
                newOb.ReferenceNumber = ob.ReferenceNumber;
                //newOb.Payroll = ob.Payroll;
                newOb.PayrollItem = payrollItem;
                newOb.TimeDuration = ob.TimeDuration;

                if (ob.Attachments.Count > 0)
                {
                    newOb.Attachments.AddRange(ob.Attachments);
                }

                _employeeCount++;
            }

            objectSpace.CommitChanges();

            View.Refresh();


        }
    }
}
