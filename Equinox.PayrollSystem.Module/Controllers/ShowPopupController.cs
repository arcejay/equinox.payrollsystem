using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.Models;

namespace Equinox.PayrollSystem.Module.Controllers
{
    public class ShowPopupController : ViewController
    {
        public ShowPopupController()
        {
            PopupWindowShowAction showPopupAction = new PopupWindowShowAction(
                this,
                "ShowPopup",
                DevExpress.Persistent.Base.PredefinedCategory.View
                );

            TargetObjectType = typeof (DeductionModel);

            showPopupAction.CustomizePopupWindowParams += this.showPopupAction_CustomizePopupWindowParams;
        }
        private void showPopupAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            e.View = Application.CreateListView(e.Application.CreateObjectSpace(), typeof(Payroll), false);
        }
    }
}