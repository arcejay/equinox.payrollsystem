﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Reports;

namespace Equinox.PayrollSystem.Module.Controllers.Reports
{
    class NonPersistentClassWindowController : WindowController
    {
        public NonPersistentClassWindowController() : base()
        {
            TargetWindowType = WindowType.Main;
        }

        protected override void OnActivated()
        {
            base.OnActivated();

            Application.ObjectSpaceCreated += ApplicationOnObjectSpaceCreated; 
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();

            Application.ObjectSpaceCreated -= ApplicationOnObjectSpaceCreated;
        }

        private void ApplicationOnObjectSpaceCreated(object sender, ObjectSpaceCreatedEventArgs e)
        {
            if (e.ObjectSpace is NonPersistentObjectSpace)
            {
                ((NonPersistentObjectSpace)e.ObjectSpace).ObjectsGetting += OnObjectsGetting;
            }

        }

        private void OnObjectsGetting(object sender, ObjectsGettingEventArgs e)
        {
            var list = new BindingList<TardinessReport>();

            var objectSpace = Application.CreateObjectSpace();

            var employees = objectSpace.GetObjects(typeof (Employee));

            foreach (Employee employee in employees)
            {
                var tardinessReport = new TardinessReport();
                tardinessReport.Employee = employee.FullName;
                tardinessReport.Department = employee.Department.Name;
                tardinessReport.Tardy = employee.DailyTimeRecords.Sum(x => x.Tardiness);
                list.Add(tardinessReport);
                
            }

            e.Objects = list;
            //e.Handled = true;
           
        }
    }
}
