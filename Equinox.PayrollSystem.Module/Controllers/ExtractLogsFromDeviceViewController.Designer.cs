﻿namespace Equinox.PayrollSystem.Module.Controllers
{
    partial class ExtractLogsFromDeviceViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ExtractLogFromDeviceAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // ExtractLogFromDeviceAction
            // 
            this.ExtractLogFromDeviceAction.AcceptButtonCaption = null;
            this.ExtractLogFromDeviceAction.CancelButtonCaption = null;
            this.ExtractLogFromDeviceAction.Caption = "Extract Logs from Device";
            this.ExtractLogFromDeviceAction.Category = "RecordEdit";
            this.ExtractLogFromDeviceAction.ConfirmationMessage = null;
            this.ExtractLogFromDeviceAction.Id = "ExtractLogFromDeviceAction";
            this.ExtractLogFromDeviceAction.ImageName = "BO_Unknown";
            this.ExtractLogFromDeviceAction.TargetObjectsCriteria = "Status = \'Online\'";
            this.ExtractLogFromDeviceAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.Device);
            this.ExtractLogFromDeviceAction.ToolTip = null;
            this.ExtractLogFromDeviceAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ExtractLogFromDeviceAction_CustomizePopupWindowParams);
            this.ExtractLogFromDeviceAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.ExtractLogFromDeviceAction_Execute);
            // 
            // ExtractLogsFromDeviceViewController
            // 
            this.Actions.Add(this.ExtractLogFromDeviceAction);
            this.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.Device);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ExtractLogFromDeviceAction;
    }
}
