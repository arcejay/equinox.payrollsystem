﻿namespace Equinox.PayrollSystem.Module.Controllers
{
    partial class AddLeaveCreditViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AddLeaveCreditViewShowAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // AddLeaveCreditViewShowAction
            // 
            this.AddLeaveCreditViewShowAction.AcceptButtonCaption = null;
            this.AddLeaveCreditViewShowAction.CancelButtonCaption = null;
            this.AddLeaveCreditViewShowAction.Caption = "Add VL/SL Credits";
            this.AddLeaveCreditViewShowAction.Category = "RecordEdit";
            this.AddLeaveCreditViewShowAction.ConfirmationMessage = null;
            this.AddLeaveCreditViewShowAction.Id = "AddLeaveCredit";
            this.AddLeaveCreditViewShowAction.ImageName = "BO_Unknown";
            this.AddLeaveCreditViewShowAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.HR.LeaveCredit);
            this.AddLeaveCreditViewShowAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.AddLeaveCreditViewShowAction.ToolTip = null;
            this.AddLeaveCreditViewShowAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.AddLeaveCreditViewShowAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.AddLeaveCreditViewShowAction_CustomizePopupWindowParams);
            this.AddLeaveCreditViewShowAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.AddLeaveCreditViewShowAction_Execute);
            // 
            // AddLeaveCreditViewController
            // 
            this.Actions.Add(this.AddLeaveCreditViewShowAction);
            this.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.HR.LeaveCredit);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction AddLeaveCreditViewShowAction;
    }
}
