﻿namespace Equinox.PayrollSystem.Module.Controllers
{
    partial class DeductionViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupDeductionAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // popupDeductionAction
            // 
            this.popupDeductionAction.AcceptButtonCaption = null;
            this.popupDeductionAction.CancelButtonCaption = null;
            this.popupDeductionAction.Caption = "Add Deduction";
            this.popupDeductionAction.Category = "RecordEdit";
            this.popupDeductionAction.ConfirmationMessage = null;
            this.popupDeductionAction.Id = "AddDeductionAction";
            this.popupDeductionAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.Accounting.Deduction);
            this.popupDeductionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.popupDeductionAction.ToolTip = "Add deductions for selected employees";
            this.popupDeductionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.popupDeductionAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.popupDeductionWindowShowAction_CustomizePopupWindowParams);
            this.popupDeductionAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.popupDeductionWindowShowAction_Execute);
            // 
            // DeductionViewController
            // 
            this.Actions.Add(this.popupDeductionAction);
            this.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.Accounting.Deduction);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction popupDeductionAction;
    }
}
