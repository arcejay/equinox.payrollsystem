﻿namespace Equinox.PayrollSystem.Module.Controllers
{
    partial class OfficialBusinessViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProcessObAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.addObToEmployeesAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // ProcessObAction
            // 
            this.ProcessObAction.AcceptButtonCaption = null;
            this.ProcessObAction.CancelButtonCaption = null;
            this.ProcessObAction.Caption = "Update OB";
            this.ProcessObAction.Category = "RecordEdit";
            this.ProcessObAction.ConfirmationMessage = null;
            this.ProcessObAction.Id = "ProcessObAction";
            this.ProcessObAction.ImageName = "BO_Unknown";
            this.ProcessObAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.DailyTimeRecord);
            this.ProcessObAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.ProcessObAction.ToolTip = "Process approved OB, Onsite Services, etc.";
            this.ProcessObAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.ProcessObAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ProcessObAction_CustomizePopupWindowParams);
            this.ProcessObAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.ProcessObAction_Execute);
            // 
            // addObToEmployeesAction
            // 
            this.addObToEmployeesAction.AcceptButtonCaption = null;
            this.addObToEmployeesAction.CancelButtonCaption = null;
            this.addObToEmployeesAction.Caption = "Batch Create";
            this.addObToEmployeesAction.Category = "RecordEdit";
            this.addObToEmployeesAction.ConfirmationMessage = null;
            this.addObToEmployeesAction.Id = "addObToEmployeesAction";
            this.addObToEmployeesAction.ImageName = "BO_Unknown";
            this.addObToEmployeesAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.HR.OfficialBusiness);
            this.addObToEmployeesAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.addObToEmployeesAction.ToolTip = "Add OB to multiple employees";
            this.addObToEmployeesAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            // 
            // OfficialBusinessViewController
            // 
            this.Actions.Add(this.ProcessObAction);
            this.Actions.Add(this.addObToEmployeesAction);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ProcessObAction;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction addObToEmployeesAction;
    }
}
