﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BiometricReaderService;
using BiometricReaderService.Common;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.LookAndFeel;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting.Native;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Models;
using TimeLog = Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.TimeLog;

//using TimeLog = Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.TimeLog;

namespace Equinox.PayrollSystem.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ExtractLogsFromDeviceViewController : ViewController
    {
        private ObservableCollection<BiometricReaderService.Common.TimeLog> _tempCollection;
        private WaitCursorController _waitCursorController;
        private int _duplicateRecord = 0;
        private int _newRecord = 0;

        public ExtractLogsFromDeviceViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            //TargetObjectType = typeof (ExtractLogsFromDeviceModel);
            this.ExtractLogFromDeviceAction.ExecuteCompleted += ExtractLogFromDeviceAction_ExecuteCompleted;
        }

        private void ExtractLogFromDeviceAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            var message =
                string.Format("Operation completed: \n- {0} records were added\n- {1} duplicates were ignored.",
                    _newRecord, _duplicateRecord);

            if (_newRecord > 0 && _duplicateRecord > 0)
            {
                XtraMessageBox.Show(message);
            }

            if (_newRecord == 0 && _duplicateRecord > 0)
            {
                XtraMessageBox.Show(_duplicateRecord + " duplicate records ignored");
            }

            if (_newRecord > 0 && _duplicateRecord == 0)
            {
                XtraMessageBox.Show(_newRecord + " successfully added.");
            }

            if (_newRecord == 0 && _duplicateRecord == 0)
            {
                XtraMessageBox.Show( "Date may be out of range. "," Out of Range",  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.


            _tempCollection = new ObservableCollection<BiometricReaderService.Common.TimeLog>();

            _waitCursorController = Frame.GetController<WaitCursorController>();

        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();

        }

        private void ExtractLogFromDeviceAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();
            
            var model = new ExtractLogsFromDeviceModel();
            var device = (Device)View.CurrentObject;

            model.IpAddress = device.IpAddress;
            model.Port = device.Port;
            model.DeviceName = device.Name;
            model.DeviceId = device.DeviceId;
            model.DeviceStatus = device.Status;
            
            e.View = Application.CreateDetailView(objectSpace, model, true);

        }

        private void ExtractLogFromDeviceAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            //get the selected device
            var model = (ExtractLogsFromDeviceModel)e.PopupWindowViewCurrentObject;

            var zkService = new ZkemService();

            var success = zkService.Connect((er) => { }, model.IpAddress, model.Port);

            //connected, so proceed
            if (success)
            {
                _newRecord = _duplicateRecord = 0;

                var results = zkService.ReadLogs((ex) => { }, model.StartDate, model.EndDate);

                var groupedByEnrollid = results.GroupBy(x => x.EnrollNumber);

                foreach (var x in groupedByEnrollid)
                {
                    var employee = (Employee)objectSpace.FindObject(typeof(Employee),
                        CriteriaOperator.Parse("BiometricId = " + x.Key));

                    if (employee == null) continue;

                    x.ForEach(timeLog =>
                    {
                        var crit = CriteriaOperator.And(
                            new BinaryOperator("BiometricId", timeLog.EnrollNumber),
                            new BinaryOperator("TimeInOut", timeLog.Time));

                        var dtr = objectSpace.FindObject<TimeLog>(crit);

                        if (dtr != null) // found duplicate record
                        {
                            _duplicateRecord++;
                        }
                        else
                        {
                            dtr = objectSpace.CreateObject<TimeLog>();

                            dtr.BiometricId = timeLog.EnrollNumber;
                            dtr.TimeInOut = timeLog.Time;
                            var mode = timeLog.InOutMode == 0 ? 1 : 0;
                            dtr.ClockInOutMode = (ClockInOutMode)mode;

                            dtr.Employee = employee;
                            _newRecord++;
                        }
                    });
                }
            }

            zkService.Disconnect();

            objectSpace.CommitChanges();

            View.Refresh();
        }
    }
}
