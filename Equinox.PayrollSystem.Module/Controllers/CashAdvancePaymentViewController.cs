﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Models;

namespace Equinox.PayrollSystem.Module.Controllers
{
    public class CashAdvancePaymentViewController : ViewController
    {
        private PopupWindowShowAction _cashAdvancePaymentWindowShowAction = null;

        public CashAdvancePaymentViewController()
        {
             _cashAdvancePaymentWindowShowAction =new PopupWindowShowAction(this,
                "CashAdvancePaymentWindowShowAction",
                PredefinedCategory.RecordEdit
                );

            TargetObjectType = typeof (CashAdvance);
            TargetViewType = ViewType.DetailView;
            
            
            _cashAdvancePaymentWindowShowAction.Caption = "Add Payment";
            _cashAdvancePaymentWindowShowAction.ImageName = "BO_Unknown";
            _cashAdvancePaymentWindowShowAction.ToolTip = "Add CA Payment";
            _cashAdvancePaymentWindowShowAction.TargetObjectType = typeof (CashAdvance);
            _cashAdvancePaymentWindowShowAction.TargetViewType = ViewType.DetailView;

            //_cashAdvancePaymentWindowShowAction.TargetObjectsCriteria = "[Status] != Fullypaid";
        }

        protected override void OnActivated()
        {
            base.OnActivated();

            _cashAdvancePaymentWindowShowAction.Execute += _cashAdvancePaymentWindowShowAction_Execute;
            _cashAdvancePaymentWindowShowAction.CustomizePopupWindowParams += _cashAdvancePaymentWindowShowAction_CustomizePopupWindowParams;
        }

        protected override void OnDeactivated()
        {
            _cashAdvancePaymentWindowShowAction.Execute -= _cashAdvancePaymentWindowShowAction_Execute;
            _cashAdvancePaymentWindowShowAction.CustomizePopupWindowParams -=
                _cashAdvancePaymentWindowShowAction_CustomizePopupWindowParams;

            base.OnDeactivated();
        }

        private void _cashAdvancePaymentWindowShowAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();
            var obj = objectSpace.CreateObject<CashAdvanceModel>();
            var currentObj = (CashAdvance)View.CurrentObject;
            var ca = objectSpace.FindObject<CashAdvance>(new BinaryOperator("Oid", currentObj.Oid));
            obj.Employee = ca.Employee;
            obj.CashAdvance = ca;
            obj.Amount = ca.InstallmentPayment;

            var view = Application.CreateDetailView(objectSpace, obj, true);
            
            e.View = view;
        }


        private void _cashAdvancePaymentWindowShowAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();
            var model = (CashAdvanceModel)e.PopupWindowViewCurrentObject;
            var payment = objectSpace.CreateObject<CashAdvancePayment>();
            var cashAdvance = objectSpace.FindObject<CashAdvance>(new BinaryOperator("Oid", model.CashAdvance.Oid));

            payment.ReferenceNumber = model.ReferenceNumber;
            payment.CashAdvance = cashAdvance;
            payment.Amount = model.Amount;
            payment.PaymentDate = model.Date;

            //add entry to deduction
            var deduction = objectSpace.CreateObject<Deduction>();
            deduction.Employee = payment.CashAdvance.Employee;
            deduction.DeductionType = DeductionType.CashAdvance;
            deduction.Amount = model.Amount;
            deduction.Description =$"Cash advance payment {cashAdvance.CashAdvancePayments.Count}/{cashAdvance.NumberOfPayments}";

            deduction.PayrollItem = objectSpace.FindObject<PayrollItem>(new BinaryOperator("Oid", model.PayrollItem.Oid));

            objectSpace.CommitChanges();

            View.Refresh();

        }
    }
}
