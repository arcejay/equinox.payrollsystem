﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;

namespace Equinox.PayrollSystem.Module.Controllers
{

    public class WaitCursorController : Controller
    {
        private Cursor savedCursor;

        private void UnsubscribeFromEvents()
        {
            foreach (Controller controller in Frame.Controllers)
            {
                foreach (ActionBase action in controller.Actions)
                {
                    action.Disposed -= new EventHandler(action_Disposed);
                    action.Executing -= new System.ComponentModel.CancelEventHandler(action_Executing);
                    action.Executed -= new EventHandler<ActionBaseEventArgs>(action_Executed);
                    action.HandleException -= new EventHandler<HandleExceptionEventArgs>(action_HandleException);
                    action.ExecuteCanceled -= new EventHandler<ActionBaseEventArgs>(action_ExecuteCanceled);
                }
            }
        }
        private void action_Disposed(object sender, EventArgs e)
        {
            ActionBase action = (ActionBase)sender;
            action.Disposed -= new EventHandler(action_Disposed);
            action.Executing -= new System.ComponentModel.CancelEventHandler(action_Executing);
            action.Executed -= new EventHandler<ActionBaseEventArgs>(action_Executed);
            action.HandleException -= new EventHandler<HandleExceptionEventArgs>(action_HandleException);
            action.ExecuteCanceled -= new EventHandler<ActionBaseEventArgs>(action_ExecuteCanceled);

        }
        private void SubscribeToEvents()
        {
            foreach (Controller controller in Frame.Controllers)
            {
                foreach (ActionBase action in controller.Actions)
                {
                    action.Disposed += new EventHandler(action_Disposed);
                    action.Executing += new System.ComponentModel.CancelEventHandler(action_Executing);
                    action.Executed += new EventHandler<ActionBaseEventArgs>(action_Executed);
                    action.HandleException += new EventHandler<HandleExceptionEventArgs>(action_HandleException);
                    action.ExecuteCanceled += new EventHandler<ActionBaseEventArgs>(action_ExecuteCanceled);
                }
            }
        }
        private void Frame_ViewChanging(object sender, ViewChangingEventArgs e)
        {
            UnsubscribeFromEvents();
        }
        private void Frame_ViewChanged(object sender, EventArgs e)
        {
            SubscribeToEvents();
        }
        private void SetWaitCursor()
        {
            savedCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
        }
        private void RestoreCursor()
        {
            Cursor.Current = savedCursor;
        }
        private void action_Executing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SetWaitCursor();
        }
        private void action_Executed(object sender, ActionBaseEventArgs e)
        {
            RestoreCursor();
        }
        private void action_ExecuteCanceled(object sender, ActionBaseEventArgs e)
        {
            RestoreCursor();
        }
        private void action_HandleException(object sender, HandleExceptionEventArgs e)
        {
            RestoreCursor();
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            if (Frame.View != null)
            {
                SubscribeToEvents();
            }
            Frame.ViewChanging += new EventHandler<ViewChangingEventArgs>(Frame_ViewChanging);
            Frame.ViewChanged += new EventHandler<ViewChangedEventArgs>(Frame_ViewChanged);
        }
        protected override void OnDeactivated()
        {
            UnsubscribeFromEvents();
            Frame.ViewChanged -= new EventHandler<ViewChangedEventArgs>(Frame_ViewChanged);
            Frame.ViewChanging -= new EventHandler<ViewChangingEventArgs>(Frame_ViewChanging);
            if (savedCursor != null)
            {
                savedCursor.Dispose();
                savedCursor = null;
            }
            base.OnDeactivated();
        }
    }
}
