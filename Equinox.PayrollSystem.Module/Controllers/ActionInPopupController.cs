using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Models;

namespace Equinox.PayrollSystem.Module.Controllers
{
    public class ActionInPopupController : ViewController
    {
        public ActionInPopupController()
        {
            SimpleAction actionInPopup = new SimpleAction(this,
                "Update",
                PredefinedCategory.PopupActions
                );

            this.TargetObjectType = typeof(DeductionModel);

            //Dennis: Refer to the http://documentation.devexpress.com/#Xaf/CustomDocument2815 help article to see how to reorder Actions within the PopupActions container.
            actionInPopup.Execute += actionInPopup_Execute;

            actionInPopup.ExecuteCompleted += ActionInPopup_ExecuteCompleted;
        }

        private void ActionInPopup_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            //var controller = Frame.GetController<DeductionViewController>();

            //controller.Actions["AddDeductionAction"].Enabled.SetItemValue("EnableAfterExecuted", true);

        }

        void actionInPopup_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var model = (DeductionModel)View.CurrentObject;

            if (model.Payroll == null) return;

            var payroll = objectSpace.FindObject<Payroll>(new BinaryOperator("Oid", model.Payroll.Oid));

            //process if employees are selected
            if (payroll == null) return;

            foreach (var deductionModel in model.Employees)
            {
                var month = model.Payroll.StartDate.Day;
                var year = model.Payroll.StartDate.Year;

                var payrollItems = deductionModel.Employee.PayrollItems.Where(x => x.Payroll.StartDate.Month == month &&
                                                                    x.Payroll.StartDate.Year == year);
                var salary = payrollItems.Sum(x => x.TotalEarnings);
                
                switch (model.DeductionType)
                {
                    case DeductionType.SSS:
                        var sss = objectSpace.FindObject<SssContributionItem>(CriteriaOperator.Parse("? >= LowerLimit AND ? <= HigherLimit", salary, salary));
                        if (sss == null) break;
                        deductionModel.Amount = sss.EmployeeShare;
                        deductionModel.Description = "SSS Employee Share";
                        break;
                    case DeductionType.PhilHealth:

                        var ph = objectSpace.FindObject<PhilhealthContributionItem>(
                            CriteriaOperator.Parse("? >=LowerLimit AND ? <=HigherLimit", salary, salary));

                        if (ph == null) break;
                        deductionModel.Amount = ph.EmployeeShare;
                        deductionModel.Description = "PhilHealth Employee Share";
                        break;
                    case DeductionType.PagIbig:
                        deductionModel.Description = "HDMF Employee Share";
                        break;
                    case DeductionType.Contribution:
                        deductionModel.Description = string.IsNullOrEmpty(model.Note) ? model.DeductionType.ToString() : model.Note;
                        break;
                    case DeductionType.Insurance:
                        deductionModel.Description = string.IsNullOrEmpty(model.Note) ? model.DeductionType.ToString() : model.Note;
                        break;
                }
            }
        }
    }
}