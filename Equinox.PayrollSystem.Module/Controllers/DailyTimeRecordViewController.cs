﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.XtraEditors;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Extensions;
using Equinox.PayrollSystem.Module.Models;
using Equinox.PayrollSystem.Module.Services;

namespace Equinox.PayrollSystem.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public class DailyTimeRecordViewController : ViewController
    {
        int _count = 0;
        private int _duplicateRecord = 0;
        private PopupWindowShowAction DailyTimeRecordShowAction = new PopupWindowShowAction();

        public DailyTimeRecordViewController()
        {
            this.DailyTimeRecordShowAction.AcceptButtonCaption = null;
            this.DailyTimeRecordShowAction.CancelButtonCaption = null;
            this.DailyTimeRecordShowAction.Caption = "Generate DTR";
            this.DailyTimeRecordShowAction.Category = "RecordEdit";
            this.DailyTimeRecordShowAction.ConfirmationMessage = null;
            this.DailyTimeRecordShowAction.Id = "DailyTimeRecordShowAction";
            this.DailyTimeRecordShowAction.ImageName = "BO_Unknown";
            this.DailyTimeRecordShowAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.TimeLog);
            this.DailyTimeRecordShowAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.DailyTimeRecordShowAction.ToolTip = "Generate daily time records for a payroll period";
            this.DailyTimeRecordShowAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            //this.DailyTimeRecordShowAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.DailyTimeRecordShowAction_CustomizePopupWindowParams);
            //this.DailyTimeRecordShowAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.DailyTimeRecordShowAction_Execute);
            // 
            // DailyTimeRecordViewController
            // 
            this.Actions.Add(this.DailyTimeRecordShowAction);
            this.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.TimeLog);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

            Actions.Add(DailyTimeRecordShowAction);

            
        }

        private void DailyTimeRecordShowAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            var message =
                string.Format("Operation completed: \n- {0} records were added\n- {1} duplicates were ignored.",
                    _count, _duplicateRecord);


            if (_count > 0 && _duplicateRecord > 0)
            {
                XtraMessageBox.Show(message);
            }

            if (_count == 0 && _duplicateRecord > 0)
            {
                XtraMessageBox.Show(_duplicateRecord + " duplicate records ignored");
            }

            if (_count > 0 && _duplicateRecord == 0)
            {
                XtraMessageBox.Show(_count + " successfully added.");
            }

            if (_count == 0 && _duplicateRecord == 0)
            {
                XtraMessageBox.Show("Date may be out of range. ", " Out of Range", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            DailyTimeRecordShowAction.ExecuteCompleted += DailyTimeRecordShowAction_ExecuteCompleted;
            DailyTimeRecordShowAction.CustomizePopupWindowParams += DailyTimeRecordShowAction_CustomizePopupWindowParams;
            DailyTimeRecordShowAction.Execute += DailyTimeRecordShowAction_Execute;
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            DailyTimeRecordShowAction.ExecuteCompleted -= DailyTimeRecordShowAction_ExecuteCompleted;
            DailyTimeRecordShowAction.CustomizePopupWindowParams -= DailyTimeRecordShowAction_CustomizePopupWindowParams;
            DailyTimeRecordShowAction.Execute -= DailyTimeRecordShowAction_Execute;

            base.OnDeactivated();
        }

        private void DailyTimeRecordShowAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            _count = 0;

            var objectSpace = Application.CreateObjectSpace();

            var model = objectSpace.CreateObject<DateRangeModel>();

            //model.Date = DateTime.Now;

            e.View = Application.CreateDetailView(objectSpace, model);

        }

        private void DailyTimeRecordShowAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var model = (DateRangeModel)e.PopupWindowViewCurrentObject;
            
            var criteria = new BetweenOperator("TimeInOut", model.StartDate, model.EndDate.AddDays(1));

            var collection = objectSpace.GetObjects<TimeLog>( criteria, true);

            var timelogs = collection.GroupBy(x => x.Employee);

            //process each employee
            foreach (var employee in timelogs)
            {
                var timeChecks = employee.Key.WorkSchedule.TimeCheckItems.Select(x => x.TimeCheck)
                        .OrderBy(x => x.TotalMinutes)
                        .ToList();

                //process each timelog for days where the employee is present.

                var daily = employee.GroupBy(x => x.TimeInOut.Date).ToList();

                foreach (var workhour in daily)
                {
                    var list = workhour.Select(x => x.TimeInOut).OrderBy(x => x).ToList();

                    //check if DTR already exists
                    var crit = CriteriaOperator.And(
                        new BinaryOperator("Employee", employee.Key),
                        new BinaryOperator("Date",list[0].Date)
                        );

                    var dtrExists = objectSpace.FindObject<DailyTimeRecord>(crit);

                    if (dtrExists != null)
                    {
                        _duplicateRecord++;
                    }
                    else
                    {
                        var dtr = objectSpace.CreateObject<DailyTimeRecord>();
                        dtr.Employee = employee.Key;

                        IDtrService dtrService = HelperClass.GetDtrService(dtr.Employee);

                        dtrService.GenerateDtr(dtr, list);

                        _count++;
                    }
                    
                }

                //process dtr where employee is absent
                var daysInPeriod = HelperClass.GenerateDates(model.StartDate, model.EndDate);
                //var daysInPeriod = Enumerable.Range(model.StartDate.Day, model.EndDate.Day - model.StartDate.Day)
                        //.Select(x => new DateTime(model.StartDate.Year, model.StartDate.Month, x));
                
                var daysPresent = daily.Select(x => x.Key).ToList();

                var daysAbsent = daysInPeriod.Except(daysPresent);

                foreach (var dateTime in daysAbsent)
                {
                    var crit = CriteriaOperator.And(
                        new BinaryOperator("Employee", employee.Key),
                        new BinaryOperator("Date", dateTime.Date)
                        );

                    var dtrExists = objectSpace.FindObject<DailyTimeRecord>(crit);

                    if (dtrExists != null) continue;

                    var d = objectSpace.CreateObject<DailyTimeRecord>();
                    d.Employee = employee.Key;
                    d.Date = dateTime;
                }
            }

            objectSpace.CommitChanges();
        }

        
    }
}
