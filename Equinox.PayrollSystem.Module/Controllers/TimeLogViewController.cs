﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiometricReaderService;
using BiometricReaderService.Common;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting.Native;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;
using TimeLog = Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.TimeLog;

namespace Equinox.PayrollSystem.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TimeLogFileViewController : ViewController
    {
        private IObjectSpace _objectSpace;
        private WindowTemplateController _controller;
        private int _rowCount = 0;
        
        public TimeLogFileViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            //TargetObjectType = typeof (TimeLogFile);

            this.ShowTimeLogFileAction.ExecuteCompleted += ShowTimeLogFileAction_ExecuteCompleted;
            
        }

        private void ShowTimeLogFileAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            XtraMessageBox.Show(_rowCount + " logs were imported successfully!");
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            _controller = Frame.GetController<WindowTemplateController>();

            _controller.CustomizeWindowStatusMessages += _controller_CustomizeWindowStatusMessages;
        }

        private void _controller_CustomizeWindowStatusMessages(object sender, CustomizeWindowStatusMessagesEventArgs e)
        {
            e.StatusMessages.Clear();
            e.StatusMessages.Add(_rowCount+ " logs were processed.");
        }

       private void ShowTimeLogFileAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {

            _objectSpace = Application.CreateObjectSpace();

            var currentObject = (TimeLogFile)View.CurrentObject;

            var obj = _objectSpace.GetObjectByKey(typeof (TimeLogFile), currentObject.Oid);

            e.View = Application.CreateDetailView(_objectSpace, obj);

        }

        private async void ShowTimeLogFileAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            try
            {
                var objectSpace = Application.CreateObjectSpace();

                var currentObject =
                    objectSpace.FindObject<TimeLogFile>(CriteriaOperator.Parse("Oid = ?",
                        ((TimeLogFile) View.CurrentObject).Oid));


                //read from stream
                var content = await ReadFileData(currentObject.FileData);

                //parse and add to RawDailyTimeRecord
                var rows = content.Split("\n\r".ToCharArray()).Where(x=> !string.IsNullOrWhiteSpace(x)).ToArray();


                rows.ForEach(row =>
                {
                    var dtr = objectSpace.CreateObject<TimeLog>();

                    var cols = row.Split(',');

                    dtr.BiometricId = int.Parse(cols[0]);
                    dtr.TimeInOut = DateTime.Parse(cols[1] + " " + cols[2]);

                    var mode = cols[3] == "I" ? 1 : 0;
                    dtr.ClockInOutMode = (ClockInOutMode)mode;
                    var employee = (Employee)objectSpace.FindObject(typeof(Employee),
                        CriteriaOperator.Parse("BiometricId = " + dtr.BiometricId));

                    if (employee != null)
                    {
                        dtr.Employee = employee;
                    }

                    _rowCount++;

                });
                
                currentObject.TimeLogFileStatus = TimeLogFileStatus.Processed;

                objectSpace.CommitChanges();

                _controller.UpdateWindowStatusMessage();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<string> ReadFileData(TimeLogFileData fileData)
        {
            using (var stream = new MemoryStream())
            {
                fileData.SaveToStream(stream);

                var ms = stream;

                return await Task.Run(() => Encoding.UTF8.GetString(ms.ToArray()));

                //using (var reader = new StreamReader(stream))
                //{
                //    return await reader.ReadToEndAsync();
                //}


            }
        }
    }
}
