﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.LookAndFeel;
using DevExpress.Persistent.Validation;
using DevExpress.XtraEditors;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Extensions;
using Equinox.PayrollSystem.Module.Models;

namespace Equinox.PayrollSystem.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DeductionViewController : ViewController
    {
        public DeductionViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            this.popupDeductionAction.ImageName = "BO_Unknown";

        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            //this.popupDeductionAction.Enabled.SetItemValue("DisabledState", false);

        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void popupDeductionWindowShowAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var deductionModel = objectSpace.CreateObject<DeductionModel>();

            var view  = Application.CreateDetailView(objectSpace, deductionModel);

            e.View = view;
        }


        private void popupDeductionWindowShowAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var model = (DeductionModel)e.PopupWindowViewCurrentObject;

            //process if employees are selected
            if (model.Payroll == null)
            {
                XtraMessageBox.Show("Unable to continue. Payroll must be set.", "Payroll is null");
                return;
            };

            if (string.IsNullOrEmpty(model.Note))
            {
                XtraMessageBox.Show("A short description of this deduction must be specified");
                return;
            }

            var payroll = objectSpace.FindObject<Payroll>(new BinaryOperator("Oid", model.Payroll.Oid));

            if (payroll == null) return;

            foreach (var payrollItem in payroll.PayrollItems)
            {
                var deduction = objectSpace.CreateObject<Deduction>();
                deduction.Employee = payrollItem.Employee;
                deduction.DeductionType = model.DeductionType;
                deduction.DeductionDate = model.DeductionDate;
                deduction.PayrollItem = payrollItem;
                deduction.DeductionType = model.DeductionType;
                deduction.Amount = model.Employees.First(x => x.Employee.Oid == payrollItem.Employee.Oid).Amount;
                deduction.Description = model.DeductionType.ToString();

                //switch (model.DeductionType)
                //{
                //    case DeductionType.SSS:
                //        var sss = deduction.GetSssContrib();
                //        if (sss == null) break;
                //        deduction.Amount = model.
                //        deduction.Description = "SSS Employee Share";
                //        break;
                //    case DeductionType.PhilHealth:
                //        var ph = deduction.GetPhilHealthContrib();
                //        if (ph == null) break;
                //        deduction.Amount = ph.EmployeeShare;
                //        deduction.Description = "PhilHealth Employee Share";  //take note of the maximum limit
                //        break;
                //    case DeductionType.PagIbig:
                //        deduction.Description = "HDMF Employee Share";
                //        break;
                //    case DeductionType.Contribution:
                //        var d1 = model.Employees.First(x => x.Employee.Oid == payrollItem.Employee.Oid);
                //        deduction.Description = d1.Description;
                //        deduction.Amount = d1.Amount;
                //        break;
                //    case DeductionType.Insurance:
                //        var d2 = model.Employees.First(x => x.Employee.Oid == payrollItem.Employee.Oid);
                //        deduction.Description = d2.Description;
                //        deduction.Amount = d2.Amount;
                //        break;
                //}
            }

            objectSpace.CommitChanges();

            View.Refresh();
        }
    }
}
