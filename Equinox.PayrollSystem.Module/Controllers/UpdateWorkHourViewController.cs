﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using DevExpress.XtraEditors;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Models;
using Equinox.PayrollSystem.Module.Services;

namespace Equinox.PayrollSystem.Module.Controllers
{
    public class UpdateWorkHourViewController : ViewController
    {
        private int _count = 0;

        public UpdateWorkHourViewController()
        {
            var updateHoursWindowShowAction = new PopupWindowShowAction(this, "UpdateWorkHours", PredefinedCategory.RecordEdit);
            updateHoursWindowShowAction.TargetObjectType = typeof (DailyTimeRecord);
            updateHoursWindowShowAction.ImageName = "BO_Unknown";
            updateHoursWindowShowAction.TargetViewType = ViewType.ListView;

            updateHoursWindowShowAction.CustomizePopupWindowParams +=
                updateHoursWindowShowAction_CustomizePopupWindowParams;

            updateHoursWindowShowAction.Execute += updateHoursWindowShowAction_Execute;

            updateHoursWindowShowAction.ExecuteCompleted += UpdateHoursWindowShowAction_ExecuteCompleted;
        }

        private void UpdateHoursWindowShowAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
        {
            var message = _count == 0
                ? "No records were updated."
                : _count + " records were updated successfully!";

            XtraMessageBox.Show(message);
        }

        private void updateHoursWindowShowAction_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var model = objectSpace.CreateObject<DailyTimeRecordModel>();

            e.View = Application.CreateDetailView(objectSpace, model, View);
        }

        private void updateHoursWindowShowAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var model = (DailyTimeRecordModel)e.PopupWindowViewCurrentObject;

            IDtrService dtrService = new WorkSheduleWith4TimeCheckDtrService();
            IDtrService dtrService2 = new WorkScheduleWith2TimeCheckDtrService();

            var objectSpace = Application.CreateObjectSpace();

            var criteria = CriteriaOperator.Parse("[Date] >= ? And [Date] <= ?", model.StartDate, model.EndDate);

            var dtrs = objectSpace.GetObjects<DailyTimeRecord>(criteria, true);

            _count = 0;

            foreach (DailyTimeRecord dailyTimeRecord in dtrs)
            {
                var dtr = dailyTimeRecord;
                var crit = CriteriaOperator.Parse("StartDate >= ? AND EndDate <= ? AND HolidaySchedule.Year = ?", 
                    dtr.Date.Date, dtr.Date.Date, dtr.Date.Year);

                var holiday = objectSpace.FindObject<Holiday>(crit);
                dtr.OfficialBusiness = 0;
                switch (dtr.Employee.WorkSchedule.RequiredTimeCheck)
                {
                    case WorkScheduleType.FourTimeCheck:
                        dtrService.GetWorkHour(dtr);
                        dtrService.GetTardiness(dtr);
                        dtrService.GetUnderTime(dtr);
                        if (holiday != null)
                            dtrService.GetHoliday(dtr, holiday);

                        dtr.Status = DtrStatus.Processed;
                        break;
                    case WorkScheduleType.TwoTimeCheck:
                        dtrService2.GetWorkHour(dtr);
                        dtrService2.GetTardiness(dtr);
                        dtrService2.GetUnderTime(dtr);
                        if (holiday != null)
                            dtrService2.GetHoliday(dtr, holiday);
                        dtr.Status = DtrStatus.Processed;
                        break;
                    case WorkScheduleType.SixTimeCheck:
                        break;
                }
                _count++;
            }

            objectSpace.CommitChanges();
        }
    }
}
