﻿namespace Equinox.PayrollSystem.Module.Controllers
{
    partial class TimeLogFileViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShowTimeLogFileAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // ShowTimeLogFileAction
            // 
            this.ShowTimeLogFileAction.AcceptButtonCaption = "";
            this.ShowTimeLogFileAction.CancelButtonCaption = null;
            this.ShowTimeLogFileAction.Caption = "Process Log File";
            this.ShowTimeLogFileAction.Category = "RecordEdit";
            this.ShowTimeLogFileAction.ConfirmationMessage = null;
            this.ShowTimeLogFileAction.Id = "ShowTimeLogFileAction";
            this.ShowTimeLogFileAction.ImageName = "BO_Unknown";
            this.ShowTimeLogFileAction.TargetObjectsCriteria = "TimeLogFileStatus = \'NotProcessed\'";
            this.ShowTimeLogFileAction.ToolTip = null;
            this.ShowTimeLogFileAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ShowTimeLogFileAction_CustomizePopupWindowParams);
            this.ShowTimeLogFileAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.ShowTimeLogFileAction_Execute);
            // 
            // RawDailyTimeRecordViewController
            // 
            this.Actions.Add(this.ShowTimeLogFileAction);
            this.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.TimeLogFile);
            this.TypeOfView = typeof(DevExpress.ExpressApp.View);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ShowTimeLogFileAction;
    }
}
