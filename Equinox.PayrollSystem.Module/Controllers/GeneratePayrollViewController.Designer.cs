﻿namespace Equinox.PayrollSystem.Module.Controllers
{
    partial class GeneratePayrollViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AddEmployeesPayrollAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.TotalHoursPayrollAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.LeavePayrollAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ReleaseSalaryPayrollAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AddEmployeesPayrollAction
            // 
            this.AddEmployeesPayrollAction.AcceptButtonCaption = null;
            this.AddEmployeesPayrollAction.CancelButtonCaption = null;
            this.AddEmployeesPayrollAction.Caption = "Employees";
            this.AddEmployeesPayrollAction.Category = "RecordEdit";
            this.AddEmployeesPayrollAction.ConfirmationMessage = null;
            this.AddEmployeesPayrollAction.Id = "AddEmployeesPayrollAction";
            this.AddEmployeesPayrollAction.ImageName = "BO_Unknown";
            this.AddEmployeesPayrollAction.TargetObjectsCriteria = "PayrollStatus == \'InProgress\'";
            this.AddEmployeesPayrollAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.Accounting.Payroll);
            this.AddEmployeesPayrollAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.AddEmployeesPayrollAction.ToolTip = "Generates payroll items for selected employees";
            this.AddEmployeesPayrollAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.AddEmployeesPayrollAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.GeneratePayrollAction_CustomizePopupWindowParams);
            this.AddEmployeesPayrollAction.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.GeneratePayrollAction_Execute);
            // 
            // TotalHoursPayrollAction
            // 
            this.TotalHoursPayrollAction.Caption = "Total Hours";
            this.TotalHoursPayrollAction.Category = "RecordEdit";
            this.TotalHoursPayrollAction.ConfirmationMessage = null;
            this.TotalHoursPayrollAction.Id = "TotalHoursPayrollAction";
            this.TotalHoursPayrollAction.ImageName = "BO_Unknown";
            this.TotalHoursPayrollAction.TargetObjectsCriteria = "PayrollStatus == \'InProgress\'";
            this.TotalHoursPayrollAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.Accounting.Payroll);
            this.TotalHoursPayrollAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TotalHoursPayrollAction.ToolTip = "Calculates total work hours, tardiness, and absenses";
            this.TotalHoursPayrollAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.TotalHoursPayrollAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TotalHoursPayrollAction_Execute);
            // 
            // LeavePayrollAction
            // 
            this.LeavePayrollAction.Caption = "Leave";
            this.LeavePayrollAction.Category = "RecordEdit";
            this.LeavePayrollAction.ConfirmationMessage = null;
            this.LeavePayrollAction.Id = "LeavePayrollAction";
            this.LeavePayrollAction.ImageName = "BO_Unknown";
            this.LeavePayrollAction.TargetObjectsCriteria = "PayrollStatus == \'InProgress\'";
            this.LeavePayrollAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.Accounting.Payroll);
            this.LeavePayrollAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.LeavePayrollAction.ToolTip = "Calculate sick leave, vacation leave, etc.";
            this.LeavePayrollAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.LeavePayrollAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.LeavePayrollAction_Execute);
            // 
            // ReleaseSalaryPayrollAction
            // 
            this.ReleaseSalaryPayrollAction.Caption = "Release Salary";
            this.ReleaseSalaryPayrollAction.Category = "RecordEdit";
            this.ReleaseSalaryPayrollAction.ConfirmationMessage = null;
            this.ReleaseSalaryPayrollAction.Id = "ReleaseSalaryPayrollAction";
            this.ReleaseSalaryPayrollAction.ImageName = "BO_Unknown";
            this.ReleaseSalaryPayrollAction.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ReleaseSalaryPayrollAction.TargetObjectsCriteria = "";
            this.ReleaseSalaryPayrollAction.TargetObjectType = typeof(Equinox.PayrollSystem.Module.BusinessObjects.Accounting.Payroll);
            this.ReleaseSalaryPayrollAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.ReleaseSalaryPayrollAction.ToolTip = "Release salary for selected employee in payroll items";
            this.ReleaseSalaryPayrollAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.ReleaseSalaryPayrollAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReleaseSalaryPayrollAction_Execute);
            // 
            // GeneratePayrollViewController
            // 
            this.Actions.Add(this.AddEmployeesPayrollAction);
            this.Actions.Add(this.TotalHoursPayrollAction);
            this.Actions.Add(this.LeavePayrollAction);
            this.Actions.Add(this.ReleaseSalaryPayrollAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction AddEmployeesPayrollAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TotalHoursPayrollAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReleaseSalaryPayrollAction;
        public DevExpress.ExpressApp.Actions.SimpleAction LeavePayrollAction;
    }
}
