﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.XtraPrinting.Native;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Models;

namespace Equinox.PayrollSystem.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AddLeaveCreditViewController : ViewController
    {
        public AddLeaveCreditViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void AddLeaveCreditViewShowAction_CustomizePopupWindowParams(object sender,
            CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();

            var model = objectSpace.CreateObject<LeaveCreditModel>();

            e.View = Application.CreateDetailView(objectSpace, model);
        }

        private void AddLeaveCreditViewShowAction_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var model = (LeaveCreditModel)e.PopupWindowViewCurrentObject;
            var objectSpace = Application.CreateObjectSpace();

            //issue leave credit to all employees
            if (model.IsAllEmployee)
            {
                var employees = objectSpace.GetObjects<Employee>();

                employees.ForEach(employee =>
                {
                    var leaveCredit = objectSpace.CreateObject<LeaveCredit>();
                    leaveCredit.Employee = employee;
                    leaveCredit.CreditYear = model.CreditYear;
                    leaveCredit.Employee = employee;
                    leaveCredit.LeaveType = model.LeaveType;
                    leaveCredit.LeaveEntitlement = model.LeaveEntitlement;
                    leaveCredit.DateEntitlement = model.EntitlementDate;
                    leaveCredit.CreditMonth = model.CreditMonth;
                });
            }
            else
            {
                var employee = objectSpace.GetObjectByKey<Employee>(model.Employee.Oid);
                var leaveCredit = objectSpace.CreateObject<LeaveCredit>();
                leaveCredit.Employee = employee;
                leaveCredit.CreditYear = model.CreditYear;
                leaveCredit.Employee = employee;
                leaveCredit.LeaveType = model.LeaveType;
                leaveCredit.LeaveEntitlement = model.LeaveEntitlement;
                leaveCredit.DateEntitlement = model.EntitlementDate;
                leaveCredit.CreditMonth = model.CreditMonth;

            }

            objectSpace.CommitChanges();

            View.Refresh();
        }
    }
}
