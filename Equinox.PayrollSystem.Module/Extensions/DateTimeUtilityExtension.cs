﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Equinox.PayrollSystem.Module.Extensions
{
    public static class DateTimeUtilityExtension
    {
        /// <summary>
        /// Get distance in timespan
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns>Timespan</returns>
        public static TimeSpan GetDistance(this DateTime date1, DateTime date2)
        {
            return date1.Subtract(date2);
        }

        

        /// <summary>
        /// Get the nearest DateTime 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="dateList"></param>
        /// <returns></returns>
        public static DateTime GetNearestDate(this DateTime date, IEnumerable<DateTime> dateList)
        {
            DateTime nearest = default(DateTime);

            var maxSpan = TimeSpan.MaxValue.TotalMinutes;
            foreach (var dateTime in dateList)
            {
                var result = date.GetDistance(dateTime).TotalMinutes;

                if (result < maxSpan)
                {
                    maxSpan = result;
                    nearest = dateTime;
                }
            }

            return nearest;
        }

        /// <summary>
        /// Get the index of the nearest timespan in a list
        /// </summary>
        /// <param name="span"></param>
        /// <param name="timeSpanList"></param>
        /// <returns>index of nearest timecheck</returns>
        public static int GetNearestTime(this TimeSpan span, List<TimeSpan> timeSpanList)
        {
            int index = int.MaxValue;
            double value = double.MaxValue;
            TimeSpan ts = TimeSpan.MaxValue;

            for (int i = 0; i < timeSpanList.Count; i++)
            {
                var result = (int)Math.Abs(timeSpanList[i].Subtract(span).TotalMinutes);

                if (result < value)
                {
                    value = result;
                    index = i;
                }
            }

            return index;
        }

        /// <summary>
        /// Calculate distance between two time span in total minutes
        /// </summary>
        /// <param name="span1"></param>
        /// <param name="span2"></param>
        /// <returns></returns>
        public static double GetDistance(this TimeSpan span1, TimeSpan span2)
        {
            return Math.Abs(span1.Subtract(span2).TotalMinutes);
        }

    }
}
