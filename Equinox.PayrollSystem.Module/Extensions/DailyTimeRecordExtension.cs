﻿using System;
using System.Linq;
using DevExpress.Data.Filtering;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.Extensions
{
    public enum DtrOperationType
    {
        WorkHour, Tardiness, UnderTime
    }

    public static class DailyTimeRecordExtension
    {
        public static DateTime[] ToArray(this DailyTimeRecord dtr)
        {
            return  new[]{ dtr.Time1, dtr.Time2, dtr.Time3, dtr.Time4, dtr.Time5, dtr.Time6 };
        }
    
        /// <summary>
        /// Get total # of hours worked not including late, undertime, and absences
        /// </summary>
        /// <param name="dtr"></param>
        /// <param name="gracePeriod"></param>
        public static void GetWorkHours(this DailyTimeRecord dtr, double gracePeriod = 15)
        {
            var def = default(DateTime);

            var workSchedule = dtr.Employee.WorkSchedule;

            var timeChecks = workSchedule.TimeCheckItems;
            
            var timeins = dtr.ToArray().Where(x => !x.Equals(def)).Select(x => x.TimeOfDay).ToList();

            //if no instance of time in, exit
            if (timeins.Count  <2 ) return;

            double result = 0;
            
            for (int i = 0; i < timeChecks.Count; i = i + 2)
            {
                var tc1 = timeChecks[i].TimeCheck;
                var tc2 = timeChecks[i + 1].TimeCheck;

                var index1 = tc1.GetNearestTime(timeins);
                var index2 = tc2.GetNearestTime(timeins);

                var t1 = timeins[index1];
                var t2 = timeins[index2];

                var temp = GetWorkHour(tc1, tc2, t1, t2);
                
                result += temp;
            }

            dtr.WorkHours = result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tc1">Reference time in as specified in work schedule</param>
        /// <param name="tc2">Reference time out as specified in work schedule</param>
        /// <param name="t1">Actual time in of the employee as extracted from time keeping device</param>
        /// <param name="t2">Actual time out of the employee as extracted from time keeping device</param>
        /// <param name="gracePeriod"></param>
        /// <returns></returns>
        private static double GetWorkHour(TimeSpan tc1, TimeSpan tc2, TimeSpan t1, TimeSpan t2, int gracePeriod = 15)
        {
            //check if there is Time In and TimeOut
            if (t1 != default(TimeSpan) && t2 != default(TimeSpan))
            {
                TimeSpan time1;

                //check t1 if greater than tc1 (employee is late)
                if (t1 > tc1)
                {
                    //if late, does he exceed the gracePeriod?
                    //if yes, use t1
                    //if no, use tc1
                    time1 = t1.Subtract(tc1).TotalMinutes > gracePeriod ? t1 : tc1;
                }
                else
                {
                    //tc1 > t1 (employee is early)
                    time1 = tc1;
                }

                //employee logout later, then use tc2
                //if employee logout early, then use t2
                var time2 = (t2 > tc2) ? tc2 : t2;

                var result =  time2.Subtract(time1).TotalHours;

                //take into consideration the # of hours in a period, subtract lunch break of 1 hour if the 
                //total is 9 hours
                return tc2.Subtract(tc1).Hours == 9 ? result -1 : result;
            }

            //either no Time In or Time Out, therefore, return 0
            return 0;
        }

        public static void GetTotalHours2(this DailyTimeRecord dtr, double gracePeriod = 15)
        {
            var workSchedule = dtr.Employee.WorkSchedule;

            double workHours = 0;

            TimeSpan t1;
            TimeSpan t2;
            TimeSpan t3;
            TimeSpan t4;

            var ts1 = workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = workSchedule.TimeCheckItems[1].TimeCheck;

            if (workSchedule.TimeCheckItems.Count == 2)
            {
                if (dtr.Time1.TimeOfDay != default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    //check time1 if greater than ts1 (employee is late)
                    if (dtr.Time1.TimeOfDay > ts1)
                    {
                        t1 = dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod ? dtr.Time1.TimeOfDay : ts1;
                    }
                    else
                    {
                        //ts1 > t1 (employee is early)
                        t1 = ts1;
                    }

                    //employee logout later
                    if (dtr.Time2.TimeOfDay > ts2)
                    {
                        t2 = ts2;
                    }
                    else
                    {
                        //employee logout early
                        t2 = dtr.Time2.TimeOfDay;
                    }

                    workHours += t2.Subtract(t1).TotalHours;
                }
            }
            else if (workSchedule.TimeCheckItems.Count == 4)
            {
                var ts3 = workSchedule.TimeCheckItems[2].TimeCheck;
                var ts4 = workSchedule.TimeCheckItems[3].TimeCheck;

                if (dtr.Time1.TimeOfDay != default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    //check time1 if greater than ts1 (employee is late)
                    if (dtr.Time1.TimeOfDay > ts1)
                    {
                        t1 = dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod ? dtr.Time1.TimeOfDay : ts1;
                    }
                    else
                    {
                        //ts1 > t1 (employee is early)
                        t1 = ts1;
                    }

                    //employee logout later

                    if (dtr.Time2.TimeOfDay > ts2)
                    {
                        t2 = ts2;
                    }
                    else
                    {
                        //employee logout early
                        t2 = dtr.Time2.TimeOfDay;
                    }

                    workHours += t2.Subtract(t1).TotalHours;
                }

                if (dtr.Time3.TimeOfDay != default(TimeSpan) && dtr.Time4.TimeOfDay != default(TimeSpan))
                {
                    //employee is late, no graceperiod in the afternoon
                    var pmTimeInThreshold = dtr.Time2.TimeOfDay.Add(new TimeSpan(1, 0, 0));

                    //if (dtr.Time3.TimeOfDay > ts3)
                    if (dtr.Time3.TimeOfDay > pmTimeInThreshold)
                    {
                        //t3 = dtr.Time3.TimeOfDay.Subtract(dtr.Time2.TimeOfDay).TotalMinutes > gracePeriod ? dtr.Time3.TimeOfDay : ts3;
                        t3 = dtr.Time3.TimeOfDay;
                    }
                    else
                    {
                        t3 = ts3;
                    }

                    if (dtr.Time4.TimeOfDay > ts4)
                    {
                        t4 = ts4;
                    }
                    else
                    {
                        //employee logs out early
                        t4 = dtr.Time4.TimeOfDay;
                    }

                    workHours += t4.Subtract(t3).TotalHours;
                }
            }

            //check Leave with pay

            dtr.WorkHours = workHours;
        }

        public static void GetHolidayAndLwop(this DailyTimeRecord dtr)
        {
            var def = default(DateTime);

            var day = dtr.Date;
            var criteria = CriteriaOperator.Parse("StartDate >= ? AND EndDate <= ?", day, day);
            

            //check if sunday
            if (dtr.Date.DayOfWeek == DayOfWeek.Sunday)
            {
                dtr.Lwop = 0;
                return;
            }

            var holiday = dtr.Session.FindObject<Holiday>(criteria);

            //holiday check
            if (holiday != null)
            {
                dtr.Lwop = 0;
                dtr.IsHoliday = true;
                dtr.HolidayMultiplier = holiday.HolidayType == HolidayType.RegularHoliday ? 1f : 0.3f;

                dtr.Holiday = dtr.WorkHours > 0 ? dtr.WorkHours : 8;
                return;
            }

            //check if on leave and is approved
            var leaveApplications = dtr.Employee.LeaveApplications;

            leaveApplications.Filter = CriteriaOperator.Parse("StartDate >= ? AND EndDate <= ? AND Status =?",
                    dtr.Date, dtr.Date, LeaveApplicationStatus.Approved);
            leaveApplications.Load();

            if (leaveApplications.Count > 0)
            {
                dtr.Lwop = 0;
                return;
            }


            //official business check
            var officialBusiness = dtr.Session.FindObject<OfficialBusiness>(criteria);

            if (officialBusiness != null)
            {
                dtr.Lwop = 0;
                dtr.OfficialBusiness = officialBusiness.EndTime.Subtract(officialBusiness.StartTime).TotalHours;
                return;
            }

            //default
            dtr.Lwop = (dtr.Time1 == def && 
                dtr.Time2 == def && 
                dtr.Time3 == def && 
                dtr.Time4 == def) ? 8 : 0;

        }

        public static void GetTardiness(this DailyTimeRecord dtr, double gracePeriod = 15)
        {

            var def = default(DateTime);

            var workSchedule = dtr.Employee.WorkSchedule;

            var timeChecks = workSchedule.TimeCheckItems;

            var timeins = dtr.ToArray().Where(x => !x.Equals(def)).Select(x => x.TimeOfDay).ToList();

            //calculate tardiness for dtr with 4 time ins

            //am only

            //pm only

            //if no instance of time in, exit
            if (timeins.Count % 2 == 1)
            {
                if (dtr.Time1 == def)
                {
                    
                }
                return;
            };

            double result = 0;

            for (int i = 0; i < timeChecks.Count; i = i + 2)
            {
                var tc1 = timeChecks[i].TimeCheck;
                var tc2 = timeChecks[i + 1].TimeCheck;

                var index1 = tc1.GetNearestTime(timeins);
                var index2 = tc2.GetNearestTime(timeins);

                var t1 = timeins[index1];
                var t2 = timeins[index2];

                //this is AM
                if (i == 0 && t1.Subtract(tc1).TotalMinutes > gracePeriod)
                {
                    result += t1.Subtract(tc1).TotalHours;
                }
                else
                {
                    var tc1Ref = t1 > tc1 ? t1.Add(new TimeSpan(1, 0, 0)) : tc1;
                    var diff = t1.Subtract(tc1Ref).TotalHours;

                    //use the AM logout as reference for PM Timein
                    result += diff > 1 ? diff : 0;
                }
            }

            dtr.Tardiness = result;
        }

        public static void GetTardiness2(this DailyTimeRecord dtr, double gracePeriod = 15)
        {
            var workSchedule = dtr.Employee.WorkSchedule;
            var def = default(TimeSpan);

            double tardiness = 0;

            var ts1 = workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = workSchedule.TimeCheckItems[1].TimeCheck;

            if (workSchedule.TimeCheckItems.Count == 2)
            {
                if (dtr.Time1.TimeOfDay != default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    //check time1 if greater than ts1 (employee is late)
                    if (dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod)
                    {
                        tardiness += dtr.Time1.TimeOfDay.Subtract(ts1).TotalHours;
                    }
                }

                //PM
                //no AM TimIn
                if (dtr.Time1.TimeOfDay == default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    tardiness += ts2.Subtract(ts1).TotalHours;
                }
            }
            else if (workSchedule.TimeCheckItems.Count == 4)
            {

                var ts3 = workSchedule.TimeCheckItems[2].TimeCheck;
                var ts4 = workSchedule.TimeCheckItems[3].TimeCheck;


                //AM Tardiness
                if (dtr.Time1.TimeOfDay != default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    //check time1 if greater than ts1 (employee is late)
                    if (dtr.Time1.TimeOfDay.Subtract(ts1).TotalMinutes > gracePeriod)
                    {
                        tardiness += dtr.Time1.TimeOfDay.Subtract(ts1).TotalHours;
                    }
                }

                //PM Tardiness
                if (dtr.Time3.TimeOfDay != default(TimeSpan) && dtr.Time4.TimeOfDay != default(TimeSpan))
                {
                    //threshold is the AM TimeOut
                    var pmTimeInThreshold = dtr.Time2.TimeOfDay != def ? dtr.Time2.TimeOfDay.Add(new TimeSpan(1, 0, 0)) : ts2;

                    //employee is late
                    if (dtr.Time3.TimeOfDay > pmTimeInThreshold)
                    {
                        tardiness += dtr.Time3.TimeOfDay.Subtract(pmTimeInThreshold).TotalHours;
                    }
                }

                //PM
                //no AM TimIn
                if (dtr.Time1.TimeOfDay == default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    tardiness += ts2.Subtract(ts1).TotalHours;
                }

                //no PM TimeIn
                if (dtr.Time3.TimeOfDay == default(TimeSpan) && dtr.Time4.TimeOfDay != default(TimeSpan))
                {
                    tardiness += ts4.Subtract(ts3).TotalHours;
                }
            }

            dtr.Tardiness = tardiness;
        }

        public static void GetUnderTime(this DailyTimeRecord dtr,  double gracePeriod = 15)
        {
            var workSchedule = dtr.Employee.WorkSchedule;

            double undertime = 0;

            var ts1 = workSchedule.TimeCheckItems[0].TimeCheck;
            var ts2 = workSchedule.TimeCheckItems[1].TimeCheck;

            if (workSchedule.TimeCheckItems.Count == 2)
            {
                //AM
                if (dtr.Time1.TimeOfDay != default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    //employee logout later
                    if (dtr.Time2.TimeOfDay < ts2)
                    {
                        //employee logout early
                        undertime += ts2.Subtract(dtr.Time2.TimeOfDay).TotalHours;
                    }
                }

                //NO LOGOUT
                //AM
                if (dtr.Time2.TimeOfDay == default(TimeSpan) && dtr.Time1.TimeOfDay != default(TimeSpan))
                {
                    undertime += ts2.Subtract(ts1).TotalHours;
                }
            }
            else if (workSchedule.TimeCheckItems.Count == 4)
            {
                var ts3 = workSchedule.TimeCheckItems[2].TimeCheck;
                var ts4 = workSchedule.TimeCheckItems[3].TimeCheck;

                //AM
                if (dtr.Time1.TimeOfDay != default(TimeSpan) && dtr.Time2.TimeOfDay != default(TimeSpan))
                {
                    //employee logout later
                    if (dtr.Time2.TimeOfDay < ts2)
                    {
                        //employee logout early
                        undertime += ts2.Subtract(dtr.Time2.TimeOfDay).TotalHours;
                    }
                }

                //PM
                if (dtr.Time3.TimeOfDay != default(TimeSpan) && dtr.Time4.TimeOfDay != default(TimeSpan))
                {
                    if (dtr.Time4.TimeOfDay < ts4)
                    {
                        //employee logs out early
                        undertime += ts4.Subtract(dtr.Time4.TimeOfDay).TotalHours;
                    }
                }

                //NO LOGOUT
                //AM
                if (dtr.Time2.TimeOfDay == default(TimeSpan) && dtr.Time1.TimeOfDay != default(TimeSpan))
                {
                    undertime += ts2.Subtract(ts1).TotalHours;
                }

                if (dtr.Time4.TimeOfDay == default(TimeSpan) && dtr.Time3.TimeOfDay != default(TimeSpan))
                {
                    undertime += ts4.Subtract(ts3).TotalHours;
                }
            }

            dtr.UnderTime = undertime;
        }

        public static void GetOfficialBusiness(this DailyTimeRecord dtr, OfficialBusiness obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));

            var def = default(TimeSpan);

            var totalHours = obj.EndTime.Subtract(obj.StartTime).TotalHours;
            var employee = dtr.Employee;

            //double ob = 0;

            //OB is full day
            if (totalHours >= 8)
            {
                dtr.OfficialBusiness = totalHours - 1;
                dtr.UnderTime = 0;
                dtr.Tardiness = 0;
                dtr.Lwop = 0;
            }
            else
            {
                //OB is not full day, so exact OB hours must be calculated.
                var timeCheckCount = employee.WorkSchedule.TimeCheckItems.Count;

                var tcs = dtr.ToArray().Select(x => x.TimeOfDay).ToList();

                if (timeCheckCount == 2)
                {
                    var t1 = dtr.Time1.TimeOfDay == def ? obj.StartTime : dtr.Time1.TimeOfDay;
                    var t2 = dtr.Time2.TimeOfDay == def ? obj.EndTime : dtr.Time2.TimeOfDay;

                    dtr.OfficialBusiness = t2.Subtract(t1).TotalHours;


                }
                else if (timeCheckCount == 4)
                {
                    
                        //check if OB is AM
                        if (dtr.Time2 > obj.EndDate)
                        {

                        }
                        else
                        {
                            //OB is PM

                        }
                    //if time1 is null, use ob start time
                    var t1 = dtr.Time1.TimeOfDay == def ? obj.StartTime : dtr.Time1.TimeOfDay;


                    var t2 = dtr.Time2.TimeOfDay == def ? obj.EndTime : dtr.Time2.TimeOfDay;


                    var min = obj.StartTime.GetNearestTime(tcs);
                    var max = obj.EndTime.GetNearestTime(tcs);

                    var t = tcs[max].Subtract(tcs[min]).TotalHours;

                    dtr.OfficialBusiness = t - totalHours;

                }

            }
        }

    }
}