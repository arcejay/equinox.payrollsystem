﻿using System;
using System.Linq;
using DevExpress.Data.Filtering;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;

namespace Equinox.PayrollSystem.Module.Extensions
{
    public static class DeductionExtension
    {
        public static SssContributionItem GetSssContrib(this Deduction deduction)
        {
            if (deduction == null) new ArgumentNullException(nameof(deduction));

            if (deduction?.Employee == null) return null;

            var month = deduction.PayrollItem.Payroll.StartDate.Month;
            var year = deduction.PayrollItem.Payroll.StartDate.Year;

            var payrollItems = deduction.Employee.PayrollItems.Where(x => x.Payroll.StartDate.Month == month &&
                                                                x.Payroll.StartDate.Year == year);

            var salary = payrollItems.Sum(x => x.TotalEarnings);
            
            var sss = deduction.Session.FindObject<SssContributionItem>(CriteriaOperator.Parse("? >= LowerLimit AND ? <= HigherLimit", salary, salary));

            return sss;
        }

        public static PhilhealthContributionItem GetPhilHealthContrib(this Deduction deduction)
        {
            if (deduction == null) new ArgumentNullException(nameof(deduction));

            if (deduction?.Employee == null) return null;

            var month = deduction.PayrollItem.Payroll.StartDate.Month;
            var year = deduction.PayrollItem.Payroll.StartDate.Year;

            var payrollItems = deduction.Employee.PayrollItems.Where(x => x.Payroll.StartDate.Month == month &&
                                                                x.Payroll.StartDate.Year == year);

            var salary = payrollItems.Sum(x => x.TotalEarnings);

            var crit = CriteriaOperator.Parse("? >=LowerLimit AND ? <=HigherLimit", salary, salary);

            var ph = deduction.Session.FindObject<PhilhealthContributionItem>(crit);

            return ph;
        }

    }
}