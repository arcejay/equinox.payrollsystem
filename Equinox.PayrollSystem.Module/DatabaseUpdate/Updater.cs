﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;

namespace Equinox.PayrollSystem.Module.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : ModuleUpdater {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            //string name = "MyName";
            //DomainObject1 theObject = ObjectSpace.FindObject<DomainObject1>(CriteriaOperator.Parse("Name=?", name));
            //if(theObject == null) {
            //    theObject = ObjectSpace.CreateObject<DomainObject1>();
            //    theObject.Name = name;
            //}


            //SequenceGeneratorInitializer.Initialize();
            //SequenceGenerator.RegisterSequences(XafTypesInfo.Instance.PersistentTypes);

            SecuritySystemUser sampleUser = ObjectSpace.FindObject<SecuritySystemUser>(new BinaryOperator("UserName", "User"));
            if(sampleUser == null) {
                sampleUser = ObjectSpace.CreateObject<SecuritySystemUser>();
                sampleUser.UserName = "User";
                sampleUser.SetPassword("");
            }
            SecuritySystemRole defaultRole = CreateDefaultRole();
            sampleUser.Roles.Add(defaultRole);

            SecuritySystemUser userAdmin = ObjectSpace.FindObject<SecuritySystemUser>(new BinaryOperator("UserName", "Admin"));
            if(userAdmin == null) {
                userAdmin = ObjectSpace.CreateObject<SecuritySystemUser>();
                userAdmin.UserName = "VN7\\whald";
                // Set a password if the standard authentication type is used
                userAdmin.SetPassword("");
            }
			// If a role with the Administrators name doesn't exist in the database, create this role
            SecuritySystemRole adminRole = ObjectSpace.FindObject<SecuritySystemRole>(new BinaryOperator("Name", "Administrators"));
            if(adminRole == null) {
               adminRole = ObjectSpace.CreateObject<SecuritySystemRole>();
                adminRole.Name = "Administrators";
            }
            adminRole.IsAdministrative = true;
			userAdmin.Roles.Add(adminRole);

            if (ObjectSpace.FindObject<SssContributionSummarySchedule>(null) == null)
            {
                CreateSssTable();
            }

            if (ObjectSpace.FindObject<PhilhealthContributionSummarySchedule>(null) == null)
            {
                CreatePhilHealthTable();
            }

            ObjectSpace.CommitChanges();
        }


        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();

            //RENAME PERSISTENT OBJECT
            //if (CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0"))
            //{
            //    RenameTable("Department", "Division");
            //    UpdateXPObjectType(
            //        "MySolution.Module.Department", "MySolution.Module.Division", "MySolution.Module");
            //}

            //Rename the Persistent Class Participant in Many-to-Many Relationship 
            //if (IsTableExists("PositionPositions_DepartmentDepartments"))
            //{
            //    RenameColumn("PositionPositions_DepartmentDepartments", "Departments", "Divisions");
            //    RenameTable("PositionPositions_DepartmentDepartments", "PositionPositions_DivisionDivisions");
            //}

            //REMOVE THE PERSISTENT CLASS
            //if (CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0"))
            //{
            //    DropTable("Division", true);
            //    DeleteObjectType("MySolution.Module.Division");
            //}

            //REMOVE THE PERSISTENT PROPERTY
            //if (CurrentDBVersion < new Version("1.1.0.0")) && CurrentDBVersion > new Version("0.0.0.0")) {
            //    DropColumn("Department", "Room");
            //}

            //Change the Persistent Property's Data Type 
            //if (CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0"))
            //{
            //    ExecuteNonQueryCommand(
            //        "alter table Department alter column Description nvarchar(200)", true);
            //}


            //if (ObjectSpace.GetObjectsCount(typeof (CompanySettings), null) == 0)
            //{
            //    ObjectSpace.CreateObject<CompanySettings>();
            //}

            //if ((CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")))
            //{
            //    ExecuteNonQueryCommand(
            //    "alter table Device alter column DeviceId int", true);
            //}

            //if ((CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")))
            //{
            //    RenameTable("DailyTimeRecord", "RawDailyTimeRecord");
            //    UpdateXPObjectType(
            //        "Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.DailyTimeRecord",
            //        "Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping.RawDailyTimeRecord", "Equinox.PayrollSystem.Module");
            //}

            ////11032015
            //if ((CurrentDBVersion < new Version("1.0.5785.30356") && CurrentDBVersion > new Version("0.0.0.0")))
            //{
            //    ExecuteNonQueryCommand(
            //    "alter table Deduction alter column Description varchar(64)", true);
            //}
        }

        private SecuritySystemRole CreateDefaultRole()
        {
            SecuritySystemRole defaultRole =
                ObjectSpace.FindObject<SecuritySystemRole>(new BinaryOperator("Name", "Default"));
            if (defaultRole == null)
            {
                defaultRole = ObjectSpace.CreateObject<SecuritySystemRole>();
                defaultRole.Name = "Default";

                defaultRole.AddObjectAccessPermission<SecuritySystemUser>("[Oid] = CurrentUserId()",
                    SecurityOperations.ReadOnlyAccess);
                defaultRole.AddMemberAccessPermission<SecuritySystemUser>("ChangePasswordOnFirstLogon",
                    SecurityOperations.Write);
                defaultRole.AddMemberAccessPermission<SecuritySystemUser>("StoredPassword", SecurityOperations.Write);
                defaultRole.SetTypePermissionsRecursively<SecuritySystemRole>(SecurityOperations.Read,
                    SecuritySystemModifier.Allow);
                defaultRole.SetTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess,
                    SecuritySystemModifier.Allow);
                defaultRole.SetTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess,
                    SecuritySystemModifier.Allow);
                defaultRole.SetTypePermissionsRecursively<ModelDifference>(SecurityOperations.Create,
                    SecuritySystemModifier.Allow);
                defaultRole.SetTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.Create,
                    SecuritySystemModifier.Allow);
            }
            return defaultRole;

        }


        private void CreatePhilHealthTable()
        {
            //var table = ObjectSpace.CreateObject<PhilhealthContributionSummarySchedule>();

            

        }

        private void CreateSssTable()
        {
            
        }

    }
}
