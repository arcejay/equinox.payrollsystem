﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [NavigationItem(NavigationItemName.HumanResources)]
    [DefaultClassOptions]
    [DefaultProperty("FullName"),ImageName("BO_Person")]
    public class Employee : XPObject
    {
        [ImageEditor(ImageEditorMode.PictureEdit, ImageEditorMode.PictureEdit)]
        public byte[] Photo
        {
            get { return GetPropertyValue<byte[]>("Photo"); }
            set { SetPropertyValue("Photo", value); }
        }

        public TitleOfCourtesy Title
        {
            get { return GetPropertyValue<TitleOfCourtesy>("Title"); }
            set { SetPropertyValue("Title", value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string FirstName
        {
            get { return GetPropertyValue<string>("FirstName"); }
            set { SetPropertyValue("FirstName", value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string MiddleName
        {
            get { return GetPropertyValue<string>("MiddleName"); }
            set { SetPropertyValue("MiddleName", value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string LastName
        {
            get { return GetPropertyValue<string>("LastName"); }
            set { SetPropertyValue("LastName", value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ModelDefault("DisplayFormat", "{0:d4}")]
        public int BiometricId
        {
            get { return GetPropertyValue<int>("BiometricId"); }
            set { SetPropertyValue("BiometricId", value); }
        }
        
        public string FullName
        {
            get
            {
                string mi = string.Empty;

                if (!string.IsNullOrEmpty(MiddleName))
                {
                    var temp = MiddleName.ToUpper().Split().Select(x => x[0].ToString());
                    mi = string.Join("", temp);
                }

                var list = new List<string>() {FirstName, mi +".", LastName};
                var fullName = string.Join(" ", list);

                //return !string.IsNullOrEmpty(fullName) ? Title.ToString() + " " + fullName : string.Empty;
                return !string.IsNullOrWhiteSpace(fullName) ? fullName : string.Empty;
            }
        }

        public DateTime HireDate
        {
            get { return GetPropertyValue<DateTime>("HireDate"); }
            set { SetPropertyValue("HireDate", value); }
        }
        
        public SalaryGrade SalaryGrade
        {
            get { return GetPropertyValue<SalaryGrade>("SalaryGrade"); }
            set
            {
                SetPropertyValue("SalaryGrade", value); 
            }
        }

        public StepIncrement StepIncrement
        {
            get { return GetPropertyValue<StepIncrement>("StepIncrement"); }
            set { SetPropertyValue("StepIncrement", value); }
        }

        [DataSourceCriteria("ReportsTo != '@This'")]
        [Association(AssociationName.EmployeeSubordinates)]
        public Employee ReportsTo
        {
            get { return GetPropertyValue<Employee>("ReportsTo"); }
            set { SetPropertyValue("ReportsTo", value); }
        }

        [Association(AssociationName.WorkScheduleEmployees)]
        public WorkSchedule WorkSchedule
        {
            get { return GetPropertyValue<WorkSchedule>("WorkSchedule"); }
            set { SetPropertyValue("WorkSchedule", value); }
        }

        [Association(AssociationName.PositionEmployees)]
        public Position Position
        {
            get { return GetPropertyValue<Position>("Position"); }
            set { SetPropertyValue("Position", value); }
        }

        [Association(AssociationName.EmployeeDailyTimeRecords)]
        public XPCollection<DailyTimeRecord> DailyTimeRecords => GetCollection<DailyTimeRecord>("DailyTimeRecords");
        
        //[Association(AssociationName.OfficialBusinessEmployees)]
        //public XPCollection<OfficialBusiness> OfficialBusinessTrips => GetCollection<OfficialBusiness>("OfficialBusinessTrips");

        [Association(AssociationName.EmployeeOfficialBusinesses)]
        public XPCollection<OfficialBusiness> OfficialBusinesses => GetCollection<OfficialBusiness>("OfficialBusinesses");
        
        public EmploymentStatus Status
        {
            get { return GetPropertyValue<EmploymentStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        [Appearance("SubordinatesVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "Subordinates.Count == 0", Context = "DetailView")]
        [Association(AssociationName.EmployeeSubordinates)]
        public XPCollection<Employee> Subordinates => GetCollection<Employee>("Subordinates");

        [Association(AssociationName.EmployeeCashAdvances)]
        public XPCollection<CashAdvance> CashAdvances => GetCollection<CashAdvance>("CashAdvances");
        
        public Company Company
        {
            get { return GetPropertyValue<Company>("Company"); }
            set { SetPropertyValue("Company", value); }
        }

        [Appearance("DepartmentVisibility", Enabled = true, Criteria = "Company not NULL", Context = "DetailView")]
        [Association(AssociationName.DepartmentEmployees)]
        [DataSourceProperty("Company.Departments")]
        public Department Department
        {
            get { return GetPropertyValue<Department>("Department"); }
            set { SetPropertyValue("Department", value); }
        }

        [Association(AssociationName.EmployeeLeaveCredits)]
        public XPCollection<LeaveCredit> LeaveEntitlements => GetCollection<LeaveCredit>("LeaveEntitlements");

        [Association(AssociationName.EmployeePayrollItems)]
        public XPCollection<PayrollItem> PayrollItems => GetCollection<PayrollItem>("PayrollItems");

        [ModelDefault("Caption", "Bonus, Incentives And Adjustments")]
        [Association(AssociationName.EmployeeBonusAdjustments)]
        public XPCollection<BonusAdjustment> BonusAdjustments => GetCollection<BonusAdjustment>("BonusAdjustments");

        [Association(AssociationName.EmployeeLeaveApplications)]
        public XPCollection<LeaveApplication> LeaveApplications => GetCollection<LeaveApplication>("LeaveApplications");

        [Association(AssociationName.EmployeeServiceRecords)]
        public XPCollection<ServiceRecord> ServiceRecords => GetCollection<ServiceRecord>("ServiceRecords");

        #region "Reports"

        [VisibleInDetailView(false), VisibleInListView(false)]
        public double NetVacationLeave   
        {
            get
            {
                return LeaveApplications.Where(x=>x.LeaveType == LeaveType.VacationLeave).Sum(x => x.NumberOfDays);
            }
        }
        
        [VisibleInDetailView(false), VisibleInListView(false)]
        public double NetSickLeave
        {
            get
            {
                return LeaveApplications.Where(x => x.LeaveType == LeaveType.SickLeave).Sum(x => x.NumberOfDays);
            }
        }
        
        [VisibleInDetailView(false), VisibleInListView(false)]
        public double AvailableSlCredit
        {
            get
            {
                var crit = CriteriaOperator.Parse("Oid =? AND LeaveType = ?", Oid, LeaveType.SickLeave);
                LeaveEntitlements.Criteria = crit;
                var result = LeaveEntitlements.Sum(x => x.LeaveEntitlement);
                return result;
            }
        }
        
        [VisibleInDetailView(false), VisibleInListView(false)]
        public double AvailableVlCredit
        {
            get
            {
                var crit  = CriteriaOperator.Parse("Oid = ? AND LeaveType = ?", Oid, LeaveType.VacationLeave);
                LeaveEntitlements.Criteria = crit;
                var result = LeaveEntitlements.Sum(x => x.LeaveEntitlement);
                return result;
            }
        }

        #endregion

        public Employee(Session session) : base(session)
        {

        }

        
    }
}