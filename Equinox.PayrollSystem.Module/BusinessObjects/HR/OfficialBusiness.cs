using System;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [NavigationItem(NavigationItemName.HumanResources)]
    [DefaultClassOptions]
    public class OfficialBusiness : XPObject
    {
        public DateTime OfficialBusinessDate    
        {
            get { return GetPropertyValue<DateTime>("OfficialBusinessDate"); }
            set { SetPropertyValue("OfficialBusinessDate", value); }
        }

        //[PersistentAlias("concat('OB-',PadLeft(ToStr(SequentialNumber),6,'0'))")]
        //public string ObNumber => Convert.ToString(EvaluateAlias("ObNumber"));

        public DateTime StartDate
        {
            get { return GetPropertyValue<DateTime>("StartDate"); }
            set
            {
                SetPropertyValue("StartDate", value);

                EndDate = value;
            }
        }

        [Appearance("StartTimeReadOnly", Criteria = "[TimeDuration] <> 'Custom'",Enabled = false, Context = "DetailView")]
        [ModelDefault("DisplayFormat", "{0:hh:mm tt}")]
        public TimeSpan StartTime
        {
            get { return GetPropertyValue<TimeSpan>("StartTime"); }
            set { SetPropertyValue("StartTime", value); }
        }

        [Appearance("EndTimeReadOnly", Criteria = "[TimeDuration] <> 'Custom'", Enabled = false, Context = "DetailView")]
        [ModelDefault("DisplayFormat","{0:hh:mm tt}")]
        public TimeSpan EndTime
        {
            get { return GetPropertyValue<TimeSpan>("EndTime"); }
            set { SetPropertyValue("EndTime", value); }
        }
        
        public DateTime EndDate
        {
            get { return GetPropertyValue<DateTime>("EndDate"); }
            set { SetPropertyValue("EndDate", value); }
        }

        [RuleRequiredField]
        public string ReferenceNumber
        {
            get { return GetPropertyValue<string>("ReferenceNumber"); }
            set { SetPropertyValue("ReferenceNumber", value); }
        }

        [RuleRequiredField]
        public string CustomerName
        {
            get { return GetPropertyValue<string>("CustomerName"); }
            set { SetPropertyValue("CustomerName", value); }
        }

        [Size(256)]
        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }
        
        [ImmediatePostData(true)]
        public ObTimeDuration TimeDuration
        {
            get { return GetPropertyValue<ObTimeDuration>("TimeDuration"); }
            set { SetPropertyValue("TimeDuration", value); }
        }


        public OfficialBusinessStatus Status
        {
            get { return GetPropertyValue<OfficialBusinessStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        //To be removed
        //[Association(AssociationName.OfficialBusinessEmployees)]
        //public XPCollection<Employee> Employees => GetCollection<Employee>("Employees");

        [Association(AssociationName.EmployeeOfficialBusinesses)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [VisibleInListView(false)]
        [NonPersistent]
        public Payroll Payroll
        {
            get { return GetPropertyValue<Payroll>("Payroll"); }
            set { SetPropertyValue("Payroll", value); }
        }

        [DataSourceProperty("Payroll.PayrollItems")]
        [DataSourceCriteria("Employee.Oid = '@This.Employee.Oid'")]
        [Association(AssociationName.PayrollItemOfficialBusinesses)]
        public PayrollItem PayrollItem
        {
            get { return GetPropertyValue<PayrollItem>("PayrollItem"); }
            set { SetPropertyValue("PayrollItem", value); }
        }


        [Association(AssociationName.OfficialBusinessAttachments), DevExpress.Xpo.Aggregated]
        public XPCollection<ObFileAttachment> Attachments => GetCollection<ObFileAttachment>("Attachments");

        private XPCollection<AuditDataItemPersistent> _auditTrail;
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (_auditTrail == null)
                {
                    _auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }

                return _auditTrail;
            }
        }

        public OfficialBusiness(Session session) : base(session)
        {

        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "TimeDuration")
            {
                switch (TimeDuration)
                {
                        case ObTimeDuration.WholeDay:
                        StartTime = new TimeSpan(9,0,0);
                        EndTime = StartTime.Add(new TimeSpan(1 + (int) ObTimeDuration.WholeDay, 0, 0));
                            break;
                        case ObTimeDuration.AmOnly:
                        StartTime = new TimeSpan(9, 0, 0);
                        EndTime = StartTime.Add(new TimeSpan((int)ObTimeDuration.AmOnly, 0, 0));
                        break;
                        case ObTimeDuration.PmOnly:
                        StartTime = new TimeSpan(13, 0, 0);
                        EndTime = StartTime.Add(new TimeSpan((int)ObTimeDuration.PmOnly, 0, 0));
                        break;
                        case ObTimeDuration.Custom:
                        break;
                }
            }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            OfficialBusinessDate = DateTime.Now;
            StartDate = DateTime.Now;
            EndDate = StartDate;
            TimeDuration = ObTimeDuration.WholeDay;
        }
    }

    public enum ObTimeDuration
    {
        WholeDay = 8,
        [XafDisplayName("AM Only")]
        AmOnly = 3,
        [XafDisplayName("PM Only")]
        PmOnly = 5,
        Custom
    }
}