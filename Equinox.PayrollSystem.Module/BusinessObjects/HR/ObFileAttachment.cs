using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    public class ObFileAttachment : FileAttachmentBase
    {
        [Association(AssociationName.OfficialBusinessAttachments)]
        public OfficialBusiness OfficialBusiness
        {
            get { return GetPropertyValue<OfficialBusiness>("OfficialBusiness"); }
            set { SetPropertyValue("OfficialBusiness", value); }
        }

        public ObFileAttachment(Session session) : base(session)
        {
        }
    }
}