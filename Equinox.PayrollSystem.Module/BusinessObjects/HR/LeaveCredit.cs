using System;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [NavigationItem(NavigationItemName.HumanResources)]
    [DefaultClassOptions]
    public class LeaveCredit : XPObject
    {
        public DateTime DateEntitlement
        {
            get { return GetPropertyValue<DateTime>("DateEntitlement"); }
            set { SetPropertyValue("DateEntitlement", value); }
        }

        [Association(AssociationName.EmployeeLeaveCredits)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        public LeaveType LeaveType
        {
            get { return GetPropertyValue<LeaveType>("LeaveType"); }
            set { SetPropertyValue("LeaveType", value); }
        }

        [ModelDefault("DisplayFormat", "{0:0.##}")]
        public double LeaveEntitlement
        {
            get { return GetPropertyValue<double>("LeaveEntitlement"); }
            set { SetPropertyValue("LeaveEntitlement", value); }
        }

        [ModelDefault("DisplayFormat", "{0:d}")]
        public int CreditYear
        {
            get { return GetPropertyValue<int>("CreditYear"); }
            set { SetPropertyValue("CreditYear", value); }
        }

        public MonthNameEnum CreditMonth
        {
            get { return GetPropertyValue<MonthNameEnum>("CreditMonth"); }
            set { SetPropertyValue("CreditMonth", value); }
        }

        public LeaveCredit(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            DateEntitlement = DateTime.Now;

            CreditYear = DateTime.Now.Year;
            CreditMonth = (MonthNameEnum)Enum.Parse(typeof(MonthNameEnum), DateTime.Now.Month.ToString());
        }
    }
}