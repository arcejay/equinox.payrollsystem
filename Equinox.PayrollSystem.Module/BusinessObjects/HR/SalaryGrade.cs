﻿using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Lookup)]
    [DefaultProperty("Grade")]
    public class SalaryGrade : XPObject
    {
        public string Grade
        {
            get { return GetPropertyValue<string>("Grade"); }
            set { SetPropertyValue("Grade", value); }
        }

        public decimal Salary
        {
            get { return GetPropertyValue<decimal>("Salary"); }
            set { SetPropertyValue("Salary", value); }
        }


        public SalaryGrade(Session session) : base(session)
        {

        }
    }
}