using System;
using System.Linq;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [NavigationItem(NavigationItemName.HumanResources)]
    [DefaultClassOptions]
    public class LeaveApplication : XPObject
    {
        public DateTime ApplicationDate
        {
            get { return GetPropertyValue<DateTime>("ApplicationDate"); }
            set { SetPropertyValue("ApplicationDate", value); }
        }

        [Appearance("StartDateVisibility", Enabled = false, Criteria = "Status ='Approved'", Context = "DetailView")]
        public DateTime StartDate
        {
            get { return GetPropertyValue<DateTime>("StartDate"); }
            set
            {
                SetPropertyValue("StartDate", value);
                EndDate = value;
                OnChanged("EndDate");
            }
        }

        [Association(AssociationName.EmployeeLeaveApplications)]
        [RuleRequiredField(DefaultContexts.Save)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [Appearance("EndDateVisibility", Enabled = false, Criteria = "Status ='Approved'", Context = "DetailView")]
        [RuleRange("DateRange", DefaultContexts.Save, "StartDate", "AddDays(StartDate, 30)", ParametersMode.Expression)]
        public DateTime EndDate
        {
            get { return GetPropertyValue<DateTime>("EndDate"); }
            set { SetPropertyValue("EndDate", value); }
        }

        [PersistentAlias("DateDiffDay(StartDate, AddDays(EndDate, 1))")]
        public int NumberOfDays
        {
            get
            {
                //if (StartDate == default(DateTime) || EndDate == default(DateTime))
                //    return 0;

                //var dates = HelperClass.GenerateDates(StartDate, EndDate)
                //    .Where(x => x.DayOfWeek != DayOfWeek.Sunday);

                ////var result = (int)EndDate.Subtract(StartDate).TotalDays + 1;

                //return dates.Count();

                return Convert.ToInt16(EvaluateAlias("NumberOfDays"));
            }
        }

        public LeaveApplicationStatus Status
        {
            get { return GetPropertyValue<LeaveApplicationStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        public LeaveType LeaveType
        {
            get { return GetPropertyValue<LeaveType>("LeaveType"); }
            set { SetPropertyValue("LeaveType", value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Size((256))]
        public string ReasonForFilingLeave
        {
            get { return GetPropertyValue<string>("ReasonForFilingLeave"); }
            set { SetPropertyValue("ReasonForFilingLeave", value); }
        }
        
        [Appearance("FileAttachmentVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "LeaveType <> 'VacationLeave'", Context = "DetailView")]
        public FileAttachment Attachment
        {
            get { return GetPropertyValue<FileAttachment>("Attachment"); }
            set { SetPropertyValue("Attachment", value); }
        }


        public LeaveApplication(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Status = LeaveApplicationStatus.InProgress;
            ApplicationDate = DateTime.Now;
        }
    }

    public class FileAttachment : FileAttachmentBase
    {
        public DocumentType DocumentType
        {
            get { return GetPropertyValue<DocumentType>("DocumentType"); }
            set { SetPropertyValue("DocumentType", value); }
        }

        public FileAttachment(Session session) : base(session)
        {
        }
    }
}