﻿using System;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [NavigationItem(NavigationItemName.HumanResources)]
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class Position : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        [Association(AssociationName.PlantillaPositions)]
        public Plantilla Plantilla
        {
            get { return GetPropertyValue<Plantilla>("Plantilla"); }
            set { SetPropertyValue("Plantilla", value); }
        }

        [Association(AssociationName.PositionEmployees)]
        public XPCollection<Employee> Employees => GetCollection<Employee>("Employees");

        public Position(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}