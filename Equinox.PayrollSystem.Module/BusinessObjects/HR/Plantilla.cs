﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [NavigationItem(NavigationItemName.HumanResources)]
    [DefaultProperty("Name")]
    public class Plantilla : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public int TotalNumberOfPosition
        {
            get { return GetPropertyValue<int>("TotalNumberOfPosition"); }
            set { SetPropertyValue("TotalNumberOfPosition", value); }
        }

        [ImmediatePostData]
        public SalaryGrade SalaryGrade
        {
            get { return GetPropertyValue<SalaryGrade>("SalaryGrade"); }
            set { SetPropertyValue("SalaryGrade", value); }
        }

        public Department Department
        {
            get { return GetPropertyValue<Department>("Department"); }
            set { SetPropertyValue("Department", value); }
        }


        [PersistentAlias("DailyRate*26")]
        public decimal BasicSalary
        {
            get
            {
                var result = EvaluateAlias("BasicSalary");

                if (result != null)
                    return (decimal)result;

                return 0;
            }
        }

        public decimal DailyRate
        {
            get { return GetPropertyValue<decimal>("DailyRate"); }
            set { SetPropertyValue("DailyRate", value); }
        }



        [NonPersistent]
        public int FilledPosition => Positions.Count;

        public int AvailablePosition => TotalNumberOfPosition > 0 ? TotalNumberOfPosition - FilledPosition : 0;


        [Association(AssociationName.PlantillaPositions)]
        public XPCollection<Position> Positions => GetCollection<Position>("Positions");


        public Plantilla(Session session) : base(session)
        {

        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "SalaryGrade" && !IsLoading)
            {
                var companySettings = Session.FindObject<CompanySettings>(null);

                DailyRate = (SalaryGrade.Salary)*12/(companySettings.NumberOfWorkDaysPerWeek*52);

            }
        }
    }
}
