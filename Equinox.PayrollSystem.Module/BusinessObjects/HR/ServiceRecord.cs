﻿using System;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [NavigationItem(NavigationItemName.HumanResources)]
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class ServiceRecord : XPObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        public string Position
        {
            get { return GetPropertyValue<string>("Position"); }
            set { SetPropertyValue("Position", value); }
        }

        public DateTime StartDate
        {
            get { return GetPropertyValue<DateTime>("StartDate"); }
            set { SetPropertyValue("StartDate", value); }
        }

        public DateTime EndDate
        {
            get { return GetPropertyValue<DateTime>("EndDate"); }
            set { SetPropertyValue("EndDate", value); }
        }

        public decimal BasicSalary
        {
            get { return GetPropertyValue<decimal>("BasicSalary"); }
            set { SetPropertyValue("BasicSalary", value); }
        }

        [Association(AssociationName.EmployeeServiceRecords)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        public ServiceRecord(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Date = DateTime.Now;
        }
    }
}