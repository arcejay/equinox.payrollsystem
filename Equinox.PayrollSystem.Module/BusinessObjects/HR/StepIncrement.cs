using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.PayrollSystem.Module.BusinessObjects.HR
{
    [DefaultClassOptions]
    [DefaultProperty("Step")]
    public class StepIncrement : XPObject
    {
        public string Step
        {
            get { return GetPropertyValue<string>("Step"); }
            set { SetPropertyValue("Step", value); }
        }

        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }
        

        public StepIncrement(Session session) : base(session)
        {

        }

    }
}