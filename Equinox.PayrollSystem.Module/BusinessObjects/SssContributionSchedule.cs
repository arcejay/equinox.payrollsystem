﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects
{
    [ModelDefault("Caption", "SSS Contrib Schedule")]
    [DefaultProperty("Description")]
    [DefaultClassOptions, NavigationItem(NavigationItemName.Lookup)]
    public class SssContributionSummarySchedule : XPObject
    {

        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        public string Reference
        {
            get { return GetPropertyValue<string>("Reference"); }
            set { SetPropertyValue("Reference", value); }
        }

        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public Status Status    
        {
            get { return GetPropertyValue<Status>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        [ModelDefault("Caption", "SSS Contribution Table")]
        [Association, Aggregated]
        public XPCollection<SssContributionItem> SssContributionItems => GetCollection<SssContributionItem>("SssContributionItems");


        public SssContributionSummarySchedule(Session session) : base(session)
        {

        }
    }


    public class PhilhealthContributionItem : XPObject
    {
        public int SalaryBracket
        {
            get { return GetPropertyValue<int>("SalaryBracket"); }
            set
            {
                SetPropertyValue("SalaryBracket", value);
                TotalMonthlyPremium = 200 + 25*(value - 1);
                EmployerShare = TotalMonthlyPremium/2;
                EmployeeShare = EmployerShare;
            }
        }

        public decimal LowerLimit
        {
            get { return GetPropertyValue<decimal>("LowerLimit"); }
            set
            {
                SetPropertyValue("LowerLimit", value);
                HigherLimit = value + (decimal)999.99;
                SalaryBase = LowerLimit;
            }
        }

        public decimal HigherLimit
        {
            get { return GetPropertyValue<decimal>("HigherLimit"); }
            set
            {
                SetPropertyValue("HigherLimit", value);
                SalaryBase = Math.Round((HigherLimit+LowerLimit)/2);
            }
        }

        public decimal SalaryBase
        {
            get { return GetPropertyValue<decimal>("SalaryBase"); }
            set
            {
                SetPropertyValue("SalaryBase", value);
            }
        }

        public decimal TotalMonthlyPremium
        {
            get { return GetPropertyValue<decimal>("TotalMonthlyPremium"); }
            set { SetPropertyValue("TotalMonthlyPremium", value); }
        }

        public decimal EmployeeShare
        {
            get { return GetPropertyValue<decimal>("EmployeeShare"); }
            set { SetPropertyValue("EmployeeShare", value); }
        }

        public decimal EmployerShare
        {
            get { return GetPropertyValue<decimal>("EmployerShare"); }
            set { SetPropertyValue("EmployerShare", value); }
        }


        [Association]
        public PhilhealthContributionSummarySchedule PhilhealthContributionSummarySchedule
        {
            get { return GetPropertyValue<PhilhealthContributionSummarySchedule>("PhilhealthContributionSummarySchedule"); }
            set { SetPropertyValue("PhilhealthContributionSummarySchedule", value); }
        }
        

        public PhilhealthContributionItem(Session session) : base(session)
        {

        }
    }

    
}
