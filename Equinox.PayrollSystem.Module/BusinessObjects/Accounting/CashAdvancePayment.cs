using System;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Accounting
{
    public class CashAdvancePayment : XPObject
    {
        public DateTime PaymentDate
        {
            get { return GetPropertyValue<DateTime>("PaymentDate"); }
            set { SetPropertyValue("PaymentDate", value); }
        }

        [Size(10)]
        public string ReferenceNumber
        {
            get { return GetPropertyValue<string>("ReferenceNumber"); }
            set { SetPropertyValue("ReferenceNumber", value); }
        }


        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }

        [Association(AssociationName.CashAdvanceCashAdvancePayments)]
        public CashAdvance CashAdvance
        {
            get { return GetPropertyValue<CashAdvance>("CashAdvance"); }
            set
            {
                SetPropertyValue("CashAdvance", value);

                if (!IsLoading && !IsSaving && value != null)
                {
                    Amount = value.InstallmentPayment;
                }
            }
        }
        

        public CashAdvancePayment(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            PaymentDate = DateTime.Now;
            
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "CashAdvance" && !IsLoading)
            {
                int paymentCount = 0;

                if (CashAdvance != null)
                {

                    if (CashAdvance.CashAdvancePayments != null)
                    {
                        paymentCount = CashAdvance.CashAdvancePayments.Count;
                    }

                    if (paymentCount < CashAdvance.NumberOfPayments)
                    {
                        Amount = CashAdvance.InstallmentPayment;
                    }
                }

            }
        }
    }
}