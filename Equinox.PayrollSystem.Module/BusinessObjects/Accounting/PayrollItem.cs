using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Extensions;
using Equinox.PayrollSystem.Module.StateMachines;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Accounting
{
    [NavigationItem(NavigationItemName.Payroll)]
    [DefaultProperty("Name")]
    public class PayrollItem : XPObject 
    {
        [PersistentAlias("Employee.FullName +' [' +Payroll.PayrollPeriod +']'")]
        public string Name
        {
            get
            {
                var result = EvaluateAlias("Name");
                return result?.ToString() ?? string.Empty;
            }
        }
        
        [Association(AssociationName.EmployeePayrollItems)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double TotalHours
        {
            get { return GetPropertyValue<double>("TotalHours"); }
            set { SetPropertyValue("TotalHours", value); }
        }
        
        [ModelDefault("Caption", "LWOP")]
        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double LeaveWithoutPay
        {
            get { return GetPropertyValue<double>("LeaveWithoutPay"); }
            set { SetPropertyValue("LeaveWithoutPay", value); }
        }

        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double Tardiness
        {
            get { return GetPropertyValue<double>("Tardiness"); }
            set { SetPropertyValue("Tardiness", value); }
        }

        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double UnderTime 
        {
            get { return GetPropertyValue<double>("UnderTime"); }
            set { SetPropertyValue("UnderTime", value); }
        }

        [ModelDefault("Caption", "Leave")]
        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double LeaveWithPay  
        {
            get { return GetPropertyValue<double>("LeaveWithPay"); }
            set { SetPropertyValue("LeaveWithPay", value); }
        }
        
        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double Holiday
        {
            get { return GetPropertyValue<double>("Holiday"); }
            set { SetPropertyValue("Holiday", value); }
        }

        [ModelDefault("Caption", "OB")]
        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double OfficialBusiness
        {
            get { return GetPropertyValue<double>("OfficialBusiness"); }
            set { SetPropertyValue("OfficialBusiness", value); }
        }


        [ModelDefault("Caption", "Adj")]
        [NonPersistent]
        public decimal Adjustments
        {
            get
            {
                var result =  BonusAdjustments
                        .Where(x => x.Status == BonusAdjustmentStatus.Approved)
                        .Sum(x => x.Amount);

                return result;

            }
        }

        [ModelDefault("Caption", "Ded")]
        [NonPersistent]
        public decimal TotalDeduction
        {
            get
            {
                var result = Deductions
                    .Where(x => x.DeductionDate >= Payroll.StartDate && x.DeductionDate <= Payroll.EndDate.AddDays(1))
                    .Sum(x => x.Amount);

                return result;
            }
        }

        [Delayed]
        [ModelDefault("Caption", "CA")]
        [ToolTip("Total employee cash advances", "Cash Advance")]
        public decimal CashAdvance
        {
            get
            {
                var result = this.Deductions.Where(x => x.DeductionType == DeductionType.CashAdvance).Sum(x => x.Amount);
                return result;
            }
        }

        [Delayed]
        [ModelDefault("Caption", "SSS")]
        [ToolTip("SSS Employee Share", "SSS")]
        public decimal SssContrib
        {
            get { return Deductions.Where(x=>x.DeductionType == DeductionType.SSS).Sum(x=>x.Amount); }
        }

        [Delayed]
        [ModelDefault("Caption", "PH")]
        [ToolTip("PhilHealth Employee Share", "PhilHealth")]
        public decimal PhilHealth
        {
            get { return Deductions.Where(x=>x.DeductionType == DeductionType.PhilHealth).Sum(x=>x.Amount); }
        }

        [Association(AssociationName.PayrollItemDeductions)]
        [VisibleInListView(false)]
        public XPCollection<Deduction> Deductions => GetCollection<Deduction>("Deductions");

        [Association(AssociationName.PayrollItemBonusAdjustments)]
        public XPCollection<BonusAdjustment> BonusAdjustments => GetCollection<BonusAdjustment>("BonusAdjustments");

        public decimal BasicPay
        {
            get
            {
                var payroll = Payroll;

                var days = HelperClass.GenerateDates(payroll.StartDate, payroll.EndDate);

                int count = days.Count(x => x.DayOfWeek != DayOfWeek.Sunday);

                return DailyRate * count;
            }
        }

        [NonPersistent]
        public decimal DailyRate
        {
            get
            {
                decimal dailyRate = 0;
                if (Employee?.SalaryGrade != null && Employee?.StepIncrement != null)
                {
                    dailyRate = (Employee.SalaryGrade.Salary + Employee.StepIncrement.Amount) * 12 / (312);
                }

                return dailyRate;
            }
        }
        
        [NonPersistent]
        public decimal TotalEarnings
        {
            get
            {
                var hourlyRate = (double)DailyRate / 8;

                //var dtr = Employee.DailyTimeRecords;

                //var crit = CriteriaOperator.And(
                //    new BetweenOperator("Date", Payroll.StartDate, Payroll.EndDate),
                //    new BinaryOperator("IsHoliday", true));
                //dtr.Filter = crit;
                //dtr.Load();

                //var holiday = dtr.Sum(x => (decimal)(x.Holiday * x.HolidayMultiplier * hourlyRate));

                return (decimal)((TotalHours + LeaveWithPay + OfficialBusiness) * hourlyRate)
                                            + HolidayAmount
                                            + Adjustments;
            }
        }

        [NonPersistent]
        public decimal NetPay => TotalEarnings - TotalDeduction;

        [Association(AssociationName.PayrollPayrollItems)]
        public Payroll Payroll
        {
            get { return GetPropertyValue<Payroll>("Payroll"); }
            set { SetPropertyValue("Payroll", value); }
        }

        [Association(AssociationName.PayrollItemDailyTimeRecords)]
        public XPCollection<DailyTimeRecord> DailyTimeRecords => GetCollection<DailyTimeRecord>("DailyTimeRecords");

        [Association(AssociationName.PayrollItemOfficialBusinesses)]
        public XPCollection<OfficialBusiness> OfficialBusinesses => GetCollection<OfficialBusiness>("OfficialBusinesses");

        public PayrollItemStatus Status
        {
            get { return GetPropertyValue<PayrollItemStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }
        
        public PayrollItem(Session session) : base(session)
        {

        }

        #region "Reports"

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [VisibleInLookupListView(false)]
        public decimal HolidayAmount
        {
            get { return GetPropertyValue<decimal>("HolidayAmount"); }
            set { SetPropertyValue("HolidayAmount", value); }
        }

        #endregion
    }
    
}