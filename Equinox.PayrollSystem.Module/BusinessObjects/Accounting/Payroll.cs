using System;
using System.Collections;
using System.ComponentModel;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Accounting
{
    [NavigationItem(NavigationItemName.Payroll)]
    [DefaultProperty("PayrollPeriod")]
    [DefaultClassOptions]
    public class Payroll : XPObject
    {
        public DateTime PayrollDate
        {
            get { return GetPropertyValue<DateTime>("PayrollDate"); }
            set { SetPropertyValue("PayrollDate", value); }
        }


        public DateTime StartDate
        {
            get { return GetPropertyValue<DateTime>("StartDate"); }
            set { SetPropertyValue("StartDate", value); }
        }

        public DateTime EndDate
        {
            get { return GetPropertyValue<DateTime>("EndDate"); }
            set { SetPropertyValue("EndDate", value); }
        }

        [NonPersistent]
        [ModelDefault("caption", "Payroll Period")]
        public string PayrollPeriod => $"{StartDate:d} - {EndDate:d}";

        [Aggregated, Association(AssociationName.PayrollPayrollItems)]
        public XPCollection<PayrollItem> PayrollItems => GetCollection<PayrollItem>("PayrollItems");

        public PayrollStatus PayrollStatus
        {
            get { return GetPropertyValue<PayrollStatus>("PayrollStatus"); }
            set { SetPropertyValue("PayrollStatus", value); }
        }


        public Payroll(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            PayrollDate = DateTime.Now;

            DateTime startDate, endDate;
            HelperClass.SetDateRange(PayrollDate, out startDate, out endDate);
            StartDate = startDate;
            EndDate = endDate;
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();

            foreach (object aggregated in new ArrayList(PayrollItems))
            {
                Session.Delete(aggregated);
            }
        }

        //[Action(PredefinedCategory.RecordEdit, AutoCommit = true, 
        //    Caption = "Total Hours", 
        //    ConfirmationMessage = "Calculate total hours?", TargetObjectsCriteria = "PayrollItems.Count() > 0",
        //    ImageName = "BO_Unknown", ToolTip = "Calculate total work hours")]
        //public void CalculateTotalHours()
        //{

        //}

        //[Action(PredefinedCategory.RecordEdit, AutoCommit = true,
        //    Caption = "Deductions",
        //    ConfirmationMessage = "Calculate deductions?",
        //    ImageName = "BO_Unknown", ToolTip = "Calculate deductions for this payroll period")]
        //public void CalculateTotalDeductions()
        //{

        //}

        //[Action(PredefinedCategory.RecordEdit, AutoCommit = true,
        //    Caption = "Leave",
        //    ConfirmationMessage = "Calculate leave?",
        //    ImageName = "BO_Unknown", ToolTip = "Calculate approved VL/SL and LWOP")]
        //public void CalculateLeave()
        //{
        
    }
}