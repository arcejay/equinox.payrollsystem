﻿using System;
using System.Linq;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Accounting
{
    [NavigationItem(NavigationItemName.Accounting)]
    [DefaultClassOptions]
    public class CashAdvance : XPObject
    {
        public DateTime CashAdvanceDate 
        {
            get { return GetPropertyValue<DateTime>("CashAdvanceDate"); }
            set { SetPropertyValue("CashAdvanceDate", value); }
        }

        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }

        [Association(AssociationName.EmployeeCashAdvances)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        public decimal InstallmentPayment
        {
            get { return GetPropertyValue<decimal>("InstallmentPayment"); }
            set { SetPropertyValue("InstallmentPayment", value); }
        }

        public int NumberOfPayments
        {
            get { return GetPropertyValue<int>("NumberOfPayments"); }
            set
            {
                if (SetPropertyValue("NumberOfPayments", value))
                {
                    InstallmentPayment = Amount/value;
                }
            }
        }

        public CashAdvanceStatus Status
        {
            get { return GetPropertyValue<CashAdvanceStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        [Size(128)]
        public string Particulars
        {
            get { return GetPropertyValue<string>("Particulars"); }
            set { SetPropertyValue("Particulars", value); }
        }
        
        public decimal Balance
        {
            get
            {
                var result = Amount - CashAdvancePayments.Sum(x => x.Amount);
                return result;
            }
        }

        //[ModelDefault("AllowEdit","False")]
        [Association(AssociationName.CashAdvanceCashAdvancePayments), Aggregated]
        public XPCollection<CashAdvancePayment> CashAdvancePayments => GetCollection<CashAdvancePayment>("CashAdvancePayments");

        public CashAdvance(Session session) : base(session)
        {
            CashAdvancePayments.CollectionChanged += CashAdvancePayments_CollectionChanged;
        }

        //private void CashAdvancePayments_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        //{
        //    if (CashAdvancePayments != null)
        //    {
                
        //    }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            CashAdvanceDate = DateTime.Now;

            
        }

        private void CashAdvancePayments_CollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            //Status = Balance == 0 ? CashAdvanceStatus.FullyPaid : CashAdvanceStatus.OnGoing;
            
        }

        

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "Amount")
            {
                
            }
        }
    }
}
