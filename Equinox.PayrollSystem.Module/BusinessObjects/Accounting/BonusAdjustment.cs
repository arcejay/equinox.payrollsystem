using System;
using System.ComponentModel;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Accounting
{
    [NavigationItem(NavigationItemName.Accounting)]
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class BonusAdjustment : XPObject
    {
        public DateTime ReleaseDate
        {
            get { return GetPropertyValue<DateTime>("ReleaseDate"); }
            set { SetPropertyValue("ReleaseDate", value); }
        }

        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public double NumberOfHours
        {
            get { return GetPropertyValue<double>("NumberOfHours"); }
            set { SetPropertyValue("NumberOfHours", value); }
        }

        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }

        public BonusAdjustmentType AdjustmentType
        {
            get { return GetPropertyValue<BonusAdjustmentType>("AdjustmentType"); }
            set { SetPropertyValue("AdjustmentType", value); }
        }
        
        [Size(256)]
        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }
        
        [DataSourceProperty("Employee.PayrollItems")]
        [Association(AssociationName.PayrollItemBonusAdjustments)]
        public PayrollItem PayrollItem
        {
            get { return GetPropertyValue<PayrollItem>("PayrollItem"); }
            set { SetPropertyValue("PayrollItem", value); }
        }
        
        [Association(AssociationName.EmployeeBonusAdjustments)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [NonPersistent]
        public decimal DailyRate
        {
            get
            {
                decimal rate = 0;

                if (Employee?.SalaryGrade != null && Employee?.StepIncrement != null)
                {
                    var totalSalary = Employee.SalaryGrade.Salary + Employee.StepIncrement.Amount;
                    rate =  totalSalary * 12 / (52 * 6);
                }

                return rate;
            }
            
        }

        [ModelDefault("AllowEdit", "False")]
        [ModelDefault("Caption", "Paid")]
        public bool IsPaid
        {
            get { return GetPropertyValue<bool>("IsPaid"); }
            set { SetPropertyValue("IsPaid", value); }
        }


        public BonusAdjustmentStatus Status
        {
            get { return GetPropertyValue<BonusAdjustmentStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        public BonusAdjustment(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            ReleaseDate = DateTime.Now;
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "NumberOfHours")
            {
                Amount = (decimal)NumberOfHours*DailyRate/8;
            }

            if (propertyName == "Status")
            {
                if (Status == BonusAdjustmentStatus.Released)
                {
                    IsPaid = true;
                }

            }
        }
    }
}