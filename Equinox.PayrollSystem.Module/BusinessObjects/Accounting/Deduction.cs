﻿using System;
using System.ComponentModel;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Extensions;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Accounting
{
    [NavigationItem(NavigationItemName.Accounting)]
    [DefaultClassOptions]
    [DefaultProperty("Description")]
    public class Deduction : XPObject
    {
        public DateTime DeductionDate
        {
            get { return GetPropertyValue<DateTime>("DeductionDate"); }
            set { SetPropertyValue("DeductionDate", value); }
        }

        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }

        [Size(64)]
        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public DeductionType DeductionType
        {
            get { return GetPropertyValue<DeductionType>("DeductionType"); }
            set
            {
                SetPropertyValue("DeductionType", value);
            }
        }

        [Association(AssociationName.PayrollItemDeductions)]
        public PayrollItem PayrollItem
        {
            get { return GetPropertyValue<PayrollItem>("PayrollItem"); }
            set { SetPropertyValue("PayrollItem", value); }
        }

        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [Appearance("IsDeductedVisibility", Enabled = false, Context = "DetailView")]
        public bool IsDeducted
        {
            get { return GetPropertyValue<bool>("IsDeducted"); }
            set { SetPropertyValue("IsDeducted", value); }
        }


        public Deduction(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            DeductionDate = DateTime.Today;
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "DeductionType")
            {

                switch (DeductionType)
                {
                    case DeductionType.SSS:
                       // Amount = this.GetSssContrib().EmployeeShare;
                        break;
                    case DeductionType.PhilHealth:
                        //Amount = this.GetPhilHealthContrib().EmployeeShare;
                        break;
                    case DeductionType.PagIbig:
                        break;
                    case DeductionType.Insurance:
                        break;
                }
            }
        }
    }

    public class PhilHealthDeduction : Deduction
    {
        public decimal EmployeeShare
        {
            get { return GetPropertyValue<decimal>("EmployeeShare"); }
            set { SetPropertyValue("EmployeeShare", value); }
        }

        public decimal EmployerShare
        {
            get { return GetPropertyValue<decimal>("EmployerShare"); }
            set { SetPropertyValue("EmployerShare", value); }
        }

        public PhilHealthDeduction(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            base.DeductionType = DeductionType.PhilHealth;
        }
    }

    public class SssDeduction : Deduction
    {
        public decimal EmployeeShare
        {
            get { return GetPropertyValue<decimal>("EmployeeShare"); }
            set { SetPropertyValue("EmployeeShare", value); }
        }

        public decimal EmployerShare
        {
            get { return GetPropertyValue<decimal>("EmployerShare"); }
            set { SetPropertyValue("EmployerShare", value); }
        }


        public SssDeduction(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            base.DeductionType = DeductionType.SSS;
        }
    }

}
