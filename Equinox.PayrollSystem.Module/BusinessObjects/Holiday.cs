﻿using System;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects
{
    [NavigationItem(NavigationItemName.Lookup)]
    [DefaultClassOptions]
    public class HolidaySchedule : XPObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }
        [ModelDefault("EditMask", "g")]
        [ModelDefault("DisplayFormat", "{0:####}")]
        [RuleRange(DefaultContexts.Save, 2015, 2050)]
        public int Year
        {
            get { return GetPropertyValue<int>("Year"); }
            set { SetPropertyValue("Year", value); }
        }


        [Association(AssociationName.HolidayScheduleHolidays)]
        public XPCollection<Holiday> Holidays => GetCollection<Holiday>("Holidays");

        public HolidaySchedule(Session session) : base(session)
        {
            
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Date = DateTime.Now;
            Year = Date.Year;
        }
    }

    public class Holiday : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public DateTime StartDate
        {
            get { return GetPropertyValue<DateTime>("StartDate"); }
            set { SetPropertyValue("StartDate", value); }
        }

        public DateTime EndDate
        {
            get { return GetPropertyValue<DateTime>("EndDate"); }
            set { SetPropertyValue("EndDate", value); }
        }

        public HolidayType HolidayType
        {
            get { return GetPropertyValue<HolidayType>("HolidayType"); }
            set { SetPropertyValue("HolidayType", value); }
        }

        [Association(AssociationName.HolidayScheduleHolidays)]
        public HolidaySchedule HolidaySchedule
        {
            get { return GetPropertyValue<HolidaySchedule>("HolidaySchedule"); }
            set { SetPropertyValue("HolidaySchedule", value); }
        }
        
        public Holiday(Session session) : base(session)
        {

        }
    }

    public enum HolidayType
    {
        RegularHoliday,  SpecialNonWorkingDay
    }
}