﻿using System;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping
{
    [NavigationItem(NavigationItemName.TimeKeeping)]
    public class WorkSchedule : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public WorkScheduleType  RequiredTimeCheck
        {
            get { return GetPropertyValue<WorkScheduleType>("RequiredTimeCheck"); }
            set { SetPropertyValue("RequiredTimeCheck", value); }
        }


        [Association]
        public XPCollection<TimeCheckItem> TimeCheckItems => GetCollection<TimeCheckItem>("TimeCheckItems");

        [Association(AssociationName.WorkScheduleEmployees)]
        public XPCollection<Employee> Employees => GetCollection<Employee>("Employees");


        public WorkSchedule(Session session) : base(session)
        {

        }
    }


    public class TimeCheckItem  : XPObject
    {
        public bool Enable
        {
            get { return GetPropertyValue<bool>("Enable"); }
            set { SetPropertyValue("Enable", value); }
        }

        public int SequenceId   
        {
            get { return GetPropertyValue<int>("SequenceId"); }
            set { SetPropertyValue("SequenceId", value); }
        }

        public TimeSpan TimeCheck
        {
            get { return GetPropertyValue<TimeSpan>("TimeCheck"); }
            set { SetPropertyValue("TimeCheck", value); }
        }

        [Association]
        public WorkSchedule WorkSchedule
        {
            get { return GetPropertyValue<WorkSchedule>("WorkSchedule"); }
            set { SetPropertyValue("WorkSchedule", value); }
        }


        public TimeCheckItem(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}