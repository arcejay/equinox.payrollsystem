using System;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping
{
    [NavigationItem(NavigationItemName.TimeKeeping)]
    [DefaultClassOptions]
    public class TimeLogFile : XPObject
    {
        [RuleRequiredField(DefaultContexts.Save)]
        public TimeLogFileData FileData
        {
            get { return GetPropertyValue<TimeLogFileData>("FileData"); }
            set { SetPropertyValue("FileData", value); }
        }

        public DateTime StartDate
        {
            get { return GetPropertyValue<DateTime>("StartDate"); }
            set { SetPropertyValue("StartDate", value); }
        }

        public DateTime EndDate
        {
            get { return GetPropertyValue<DateTime>("EndDate"); }
            set { SetPropertyValue("EndDate", value); }
        }

        public string Note
        {
            get { return GetPropertyValue<string>("Note"); }
            set { SetPropertyValue("Note", value); }
        }

        [Association(AssociationName.DeviceTimeLogs)]
        public Device Device
        {
            get { return GetPropertyValue<Device>("Device"); }
            set { SetPropertyValue("Device", value); }
        }

        public TimeLogFileStatus TimeLogFileStatus
        {
            get { return GetPropertyValue<TimeLogFileStatus>("TimeLogFileStatus"); }
            set { SetPropertyValue("TimeLogFileStatus", value); }
        }


        public TimeLogFile(Session session) : base(session)
        {
            
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            SetDateRange();
        }


        private void SetDateRange()
        {
            //set start date
            var todaysDate = DateTime.Now.Day;


            //get current year
            var year = DateTime.Now.Year;

            //date range should be the previous month
            if (todaysDate < 15)
            {
                //get previous month
                var month = DateTime.Now.Month - 1;

                //set date range to 15 - last day of the previous month
                var maxday = DateTime.DaysInMonth(DateTime.Now.Year, month);

                StartDate = new DateTime(year, month, 16);
                EndDate = new DateTime(year, month, maxday);
            }
            else
            {
                var month = DateTime.Now.Month;

                StartDate = new DateTime(year, month, 1);
                EndDate = new DateTime(year, month, 15);

            }
        }
    }

    public enum TimeLogFileStatus
    {
        NotProcessed, Processed
    }
}