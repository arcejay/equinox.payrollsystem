﻿using System;
using System.Linq;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraPrinting.Native;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping
{
    [NavigationItem(NavigationItemName.TimeKeeping)]
    [DefaultClassOptions]
    public class TimeLog : XPObject
    {
        public ClockInOutMode ClockInOutMode
        {
            get { return GetPropertyValue<ClockInOutMode>("ClockInOutMode"); }
            set { SetPropertyValue("ClockInOutMode", value); }
        }


        [Indexed("BiometricId;GCRecord", Unique = true)]
        [ModelDefault("DisplayFormat", "{0:g}")]
        public DateTime TimeInOut
        {
            get { return GetPropertyValue<DateTime>("TimeInOut"); }
            set { SetPropertyValue("TimeInOut", value); }
        }

        [ModelDefault("DisplayFormat", "{0:d4}")]
        public int  BiometricId
        {
            get { return GetPropertyValue<int>("BiometricId"); }
            set { SetPropertyValue("BiometricId", value); }
        }

        public string Comment
        {
            get { return GetPropertyValue<string>("Comment"); }
            set { SetPropertyValue("Comment", value); }
        }

        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }
        
        public TimeLog(Session session) : base(session)
        {

        }

        [Action(PredefinedCategory.RecordEdit, AutoCommit = true, Caption = "Fix Duplicates", ConfirmationMessage = "Do you wish to remove duplicate records?",
            ImageName = "BO_Unknown")]
        public void RemoveDuplicates()
        {
            var timeLogs = this.Session.Query<TimeLog>();

            var results = timeLogs.GroupBy(x => x.BiometricId + x.TimeInOut.ToLongDateString() + x.ClockInOutMode);

            //foreach (var group in results)
            //{
            //    if (group.Count() > 1)
            //    {
            //        group.Skip(1).ForEach(x =>
            //        {
            //            //Session.Delete(x);
            //        });
            //    }
            //}

            Session.CommitTransaction();
        }
    }
}