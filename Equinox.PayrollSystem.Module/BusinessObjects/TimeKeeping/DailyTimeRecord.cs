using System;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Enums;
using Equinox.PayrollSystem.Module.Extensions;

namespace Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping
{
    [NavigationItem(NavigationItemName.TimeKeeping)]
    [DefaultClassOptions]
    //[Appearance("FontColorRed", AppearanceItemType = "ViewItem", TargetItems = "*", Context = "ListView",
    //Criteria = "(Time1 is NULL OR Time2 is NULL OR Time3 is NULL OR Time4 is NULL) AND Day <> 'Sunday'", FontColor = "Red")]
    [Appearance("SundayIsGreen", AppearanceItemType = "ViewItem", TargetItems = "*", Context = "ListView",
    Criteria = "Day == 'Sunday'", BackColor = "Green")]
    [Appearance("AbsentIsGrayBackground", AppearanceItemType = "ViewItem", TargetItems = "*", Context = "ListView",
    Criteria = "Time1 is NULL AND Time2 is NULL AND Time3 is NULL AND Time4 is NULL AND Day <> 'Sunday'", BackColor = "Gray", Priority = 2)]
    [Appearance("HolidayIsOrangeBackground", AppearanceItemType = "ViewItem", TargetItems = "*", Context = "ListView",
    Criteria = "IsHoliday == TRUE AND Day <> 'Sunday'", BackColor = "Orange", Priority = 1)]
    public class DailyTimeRecord : XPObject
    {

        //[Indexed("Employee;GCRecord", Unique = true)]
        [ModelDefault("DisplayFormat", "{0:MM-dd-yyyy}")]
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        [ModelDefault("Caption", "Time In (AM)")]
        [ModelDefault("DisplayFormat", "{0:t}")]
        public DateTime Time1
        {
            get { return GetPropertyValue<DateTime>("Time1"); }
            set { SetPropertyValue("Time1", value); }
        }

        [ModelDefault("Caption", "Time Out (AM)")]
        [ModelDefault("DisplayFormat", "{0:t}")]
        public DateTime Time2
        {
            get { return GetPropertyValue<DateTime>("Time2"); }
            set { SetPropertyValue("Time2", value); }
        }

        [ModelDefault("Caption", "Time In (PM)")]
        [ModelDefault("DisplayFormat", "{0:t}")]
        public DateTime Time3
        {
            get { return GetPropertyValue<DateTime>("Time3"); }
            set { SetPropertyValue("Time3", value); }
        }
        
        [ModelDefault("Caption", "Time Out (PM)")]
        [ModelDefault("DisplayFormat", "{0:t}")]
        public DateTime Time4
        {
            get { return GetPropertyValue<DateTime>("Time4"); }
            set { SetPropertyValue("Time4", value); }
        }


        [ModelDefault("Caption", "Time In (OT)")]
        [ModelDefault("DisplayFormat", "{0:t}")]
        public DateTime Time5
        {
            get { return GetPropertyValue<DateTime>("Time5"); }
            set { SetPropertyValue("Time5", value); }
        }


        [ModelDefault("Caption", "Time Out (OT)")]
        [ModelDefault("DisplayFormat", "{0:t}")]
        public DateTime Time6
        {
            get { return GetPropertyValue<DateTime>("Time6"); }
            set { SetPropertyValue("Time6", value); }
        }

        [NonPersistent]
        public string Day => Date.DayOfWeek.ToString();

        [Association(AssociationName.PayrollItemDailyTimeRecords)]
        public PayrollItem PayrollItem
        {
            get { return GetPropertyValue<PayrollItem>("PayrollItem"); }
            set { SetPropertyValue("PayrollItem", value); }
        }

        [Association(AssociationName.EmployeeDailyTimeRecords)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double WorkHours
        {
            get
            {
               return GetPropertyValue<double>("WorkHours");
            }
            set { SetPropertyValue("WorkHours", value); }
        }
        
        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double Tardiness
        {
            get { return GetPropertyValue<double>("Tardiness"); }
            set { SetPropertyValue("Tardiness", value); }
        }


        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double UnderTime
        {
            get { return GetPropertyValue<double>("UnderTime"); }
            set { SetPropertyValue("UnderTime", value); }
        }

        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double Lwop
        {
            get { return GetPropertyValue<double>("Lwop"); }
            set { SetPropertyValue("Lwop", value); }
        }

        [ModelDefault("Caption", "OB")]
        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double OfficialBusiness
        {
            get { return GetPropertyValue<double>("OfficialBusiness"); }
            set { SetPropertyValue("OfficialBusiness", value); }
        }

        [ModelDefault("DisplayFormat", "{0:#0.#0}")]
        public double Holiday
        {
            get { return GetPropertyValue<double>("Holiday"); }
            set { SetPropertyValue("Holiday", value); }
        }

        [VisibleInListView(false), VisibleInDetailView(false)]
        public double HolidayMultiplier
        {
            get { return GetPropertyValue<double>("HolidayMultiplier"); }
            set { SetPropertyValue("HolidayMultiplier", value); }
        }

        [VisibleInListView(false), VisibleInDetailView(false)]
        public bool IsHoliday
        {
            get { return GetPropertyValue<bool>("IsHoliday"); }
            set { SetPropertyValue("IsHoliday", value); }
        }

        public DtrStatus Status
        {
            get { return GetPropertyValue<DtrStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        private double TOLERANCE = 0.02;

        public bool IsValid
        {
            get
            {
                if (Math.Abs(WorkHours - 8) < TOLERANCE) return true;

                var value = Math.Round(Tardiness + UnderTime + Lwop);

                var result = Math.Abs(value - 8) < TOLERANCE ? true : false;

                return result;
            }
        }

        public DailyTimeRecord(Session session) : base(session)
        {

        }
    }
}