﻿using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping
{
    [NavigationItem(NavigationItemName.TimeKeeping)]
    public class Device : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }
        
        public int DeviceId
        {
            get { return GetPropertyValue<int>("DeviceId"); }
            set { SetPropertyValue("DeviceId", value); }
        }

        public string Location
        {
            get { return GetPropertyValue<string>("Location"); }
            set { SetPropertyValue("Location", value); }
        }

        [ModelDefault("DisplayFormat","{0:d}")]
        public int Port
        {
            get { return GetPropertyValue<int>("Port"); }
            set { SetPropertyValue("Port", value); }
        }

        public string IpAddress
        {
            get { return GetPropertyValue<string>("IpAddress"); }
            set { SetPropertyValue("IpAddress", value); }
        }

        public BiometricDeviceType DeviceType
        {
            get { return GetPropertyValue<BiometricDeviceType>("DeviceType"); }
            set { SetPropertyValue("DeviceType", value); }
        }

        [Appearance("RedStatusColoredInListView", AppearanceItemType = "ViewItem", TargetItems = "Status", Criteria = "Status = 'Offline'", Context = "ListView", FontColor = "Red", Priority = 1)]
        [Appearance("GreenStatusColoredInListView", AppearanceItemType = "ViewItem", TargetItems = "Status",Criteria = "Status = 'Online'", Context = "ListView", FontColor = "Green" ,Priority = 1)]
        public DeviceStatus Status
        {
            get { return GetPropertyValue<DeviceStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }


        [Association(AssociationName.DeviceTimeLogs)]
        public XPCollection<TimeLogFile> TimeLogs => GetCollection<TimeLogFile>("TimeLogs");

        public DeviceConnectionType DeviceConnectionType
        {
            get { return GetPropertyValue<DeviceConnectionType>("DeviceConnectionType"); }
            set { SetPropertyValue("DeviceConnectionType", value); }
        }

        public Device(Session session) : base(session)
        {
            
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();

            CheckIpStatus();
        }

        [Action(PredefinedCategory.RecordEdit,AutoCommit = true, Caption = "Check", ImageName = "BO_Unknown")]
        public async void CheckIpStatus()
        {
            Status = await HelperClass.CheckStatusAsync(IpAddress);
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Port = 4370;
            IpAddress = "192.168.10.205";

        }
    }

    public enum DeviceConnectionType
    {
        Usb, Tcp, Serial
    }

    public enum BiometricDeviceType
    {
        Zk, Realand, Others
    }
}