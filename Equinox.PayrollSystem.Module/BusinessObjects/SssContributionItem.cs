using DevExpress.Xpo;

namespace Equinox.PayrollSystem.Module.BusinessObjects
{
    public class SssContributionItem : XPObject
    {
        public decimal MonthlySalaryCredit
        {
            get { return GetPropertyValue<decimal>("MonthlySalaryCredit"); }
            set { SetPropertyValue("MonthlySalaryCredit", value); }
        }

        public decimal LowerLimit
        {
            get { return GetPropertyValue<decimal>("LowerLimit"); }
            set
            {
                SetPropertyValue("LowerLimit", value);
                HigherLimit = value + (decimal)499.99;
                MonthlySalaryCredit = value + 250;
            }
        }

        public decimal HigherLimit
        {
            get { return GetPropertyValue<decimal>("HigherLimit"); }
            set { SetPropertyValue("HigherLimit", value); }
        }

        public decimal EmployeeShare
        {
            get { return GetPropertyValue<decimal>("EmployeeShare"); }
            set { SetPropertyValue("EmployeeShare", value); }
        }

        public decimal EmployerShare
        {
            get { return GetPropertyValue<decimal>("EmployerShare"); }
            set { SetPropertyValue("EmployerShare", value); }
        }

        [Association]
        public SssContributionSummarySchedule SssContributionSummary
        {
            get { return GetPropertyValue<SssContributionSummarySchedule>("SssContributionSummary"); }
            set { SetPropertyValue("SssContributionSummary", value); }
        }

        public SssContributionItem(Session session) : base(session)
        {

        }
    }
}