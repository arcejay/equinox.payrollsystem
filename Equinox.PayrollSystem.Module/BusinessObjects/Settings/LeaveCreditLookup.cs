﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Settings
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Lookup)]
    public class LeaveCreditLookup : XPObject
    {
        public LeaveType LeaveType
        {
            get { return GetPropertyValue<LeaveType>("LeaveType"); }
            set { SetPropertyValue("LeaveType", value); }
        }

        [RuleRange(1,100)]
        public int LeaveCredit
        {
            get { return GetPropertyValue<int>("LeaveCredit"); }
            set { SetPropertyValue("LeaveCredit", value); }
        }

        [RuleRange(1, 1000)]
        public int YearsInService
        {
            get { return GetPropertyValue<int>("YearsInService"); }
            set { SetPropertyValue("YearsInService", value); }
        }

        private XPCollection<AuditDataItemPersistent> _auditTrail;

        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (_auditTrail == null)
                {
                    _auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return _auditTrail;
            }
        }

        public LeaveCreditLookup(Session session) : base(session)
        {

        }
    }
}