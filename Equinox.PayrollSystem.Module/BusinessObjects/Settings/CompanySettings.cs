using System.Drawing;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Settings
{
    [NavigationItem(NavigationItemName.Settings)]
    //[RuleObjectExists("AnotherSingletonExists", DefaultContexts.Save, "True", InvertResult = true,
    //CustomMessageTemplate = "Another Singleton already exists.")]
    //[RuleCriteria("CannotDeleteSingleton", DefaultContexts.Delete, "False",
    //CustomMessageTemplate = "Cannot delete Singleton.")]
    public class CompanySettings : XPObject
    {
        public Company Company
        {
            get { return GetPropertyValue<Company>("Company"); }
            set { SetPropertyValue("Company", value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

        }

        [ImageEditor(ImageEditorMode.DropDownPictureEdit, ImageEditorMode.PictureEdit)]
        public byte[] CompanyLogo
        {
            get { return GetPropertyValue<byte[]>("CompanyLogo"); }
            set { SetPropertyValue("CompanyLogo", value); }
        }
            
        public Employee  GeneralManager 
        {
            get { return GetPropertyValue<Employee>("GeneralManager"); }
            set { SetPropertyValue("GeneralManager", value); }
        }

        public string TIN
        {
            get { return GetPropertyValue<string>("TIN"); }
            set { SetPropertyValue("TIN", value); }
        }

        public string SssEmployeeNumber
        {
            get { return GetPropertyValue<string>("SssEmployeeNumber"); }
            set { SetPropertyValue("SssEmployeeNumber", value); }
        }

        public PayrollPeriod PayrollPeriod
        {
            get { return GetPropertyValue<PayrollPeriod>("PayrollPeriod"); }
            set { SetPropertyValue("PayrollPeriod", value); }
        }

        public double VacationLeaveEntitlement
        {
            get { return GetPropertyValue<double>("VacationLeaveEntitlement"); }
            set { SetPropertyValue("VacationLeaveEntitlement", value); }
        }

        public double SickLeaveEntitlement
        {
            get { return GetPropertyValue<double>("SickLeaveEntitlement"); }
            set { SetPropertyValue("SickLeaveEntitlement", value); }
        }

        public double PaternityLeaveEntitlement
        {
            get { return GetPropertyValue<double>("PaternityLeaveEntitlement"); }
            set { SetPropertyValue("PaternityLeaveEntitlement", value); }
        }

        public double MaternityLeaveEntitlement
        {
            get { return GetPropertyValue<double>("MaternityLeaveEntitlement"); }
            set { SetPropertyValue("MaternityLeaveEntitlement", value); }
        }

        public int GracePeriod
        {
            get { return GetPropertyValue<int>("GracePeriod"); }
            set { SetPropertyValue("GracePeriod", value); }
        }

        [ToolTip("Check this value if the 1-hour lunch break is flexible")]
        public bool FlexibleAfternoonTimeIn
        {
            get { return GetPropertyValue<bool>("FlexibleAfternoonTimeIn"); }
            set { SetPropertyValue("FlexibleAfternoonTimeIn", value); }
        }

        public int NumberOfWorkDaysPerWeek
        {
            get { return GetPropertyValue<int>("NumberOfWorkDaysPerWeek"); }
            set { SetPropertyValue("NumberOfWorkDaysPerWeek", value); }
        }


        public CompanySettings(Session session) : base(session)
        {

        }

        
    }

    public enum PayrollPeriod
    {
        Monthly = 1,
        SemiMonthly = 2
    }
}