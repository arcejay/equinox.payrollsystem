using System.ComponentModel;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Crm.Module.BusinessObjects;
using Equinox.Infrastructure.Common;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Settings
{
    [NavigationItem(NavigationItemName.Settings)]
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class Company : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set
            {
                SetPropertyValue("Name", value);
                var result = value.ToUpper().Split().Select(x => x[0].ToString());
                ShortName = string.Join("", result);
                OnChanged(ShortName);
            }
        }

        public string ShortName
        {
            get { return GetPropertyValue<string>("ShortName"); }
            set { SetPropertyValue("ShortName", value); }
        }

        public CompanyAddress Address
        {
            get { return GetPropertyValue<CompanyAddress>("Address"); }
            set { SetPropertyValue("Address", value); }
        }

        public string EmailAddress
        {
            get { return GetPropertyValue<string>("EmailAddress"); }
            set { SetPropertyValue("EmailAddress", value); }
        }

        public CompanyPhone Phone
        {
            get { return GetPropertyValue<CompanyPhone>("Phone"); }
            set { SetPropertyValue("Phone", value); }
        }

        [Association(AssociationName.CompanyBranches)]
        public Company ParentCompany
        {
            get { return GetPropertyValue<Company>("ParentCompany"); }
            set { SetPropertyValue("ParentCompany", value); }
        }

        [Association(AssociationName.CompanyBranches)]
        public XPCollection<Company> Branches => GetCollection<Company>("Branches");

        [Association(AssociationName.CompanyDepartments)]
        public XPCollection<Department> Departments => GetCollection<Department>("Departments");

        public Company(Session session) : base(session)
        {

        }
    }

    public class CompanyPhone : Phone
    {
        public CompanyPhone(Session session) : base(session)
        {
        }
    }
}