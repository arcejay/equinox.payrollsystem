using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Settings
{
    public class Department : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        [Association(AssociationName.CompanyDepartments)]
        public Company Company
        {
            get { return GetPropertyValue<Company>("Company"); }
            set { SetPropertyValue("Company", value); }
        }
        
        [Association(AssociationName.DepartmentDivisions)]
        public Department ParentDepartment
        {
            get { return GetPropertyValue<Department>("ParentDepartment"); }
            set { SetPropertyValue("ParentDepartment", value); }
        }

        [Association(AssociationName.DepartmentDivisions)]
        public XPCollection<Department> Divisions => GetCollection<Department>("Divisions");

        [Association(AssociationName.DepartmentEmployees)]
        public XPCollection<Employee> Employees => GetCollection<Employee>("Employees");


        public Department(Session session) : base(session)
        {

        }
    }
}