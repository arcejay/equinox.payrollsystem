using System.ComponentModel;
using DevExpress.Xpo;
using Equinox.Crm.Module.BusinessObjects;

namespace Equinox.PayrollSystem.Module.BusinessObjects.Settings
{
    [DefaultProperty("FullName")]
    public class CompanyAddress : AddressBase
    {
        public CompanyAddress(Session session) : base(session)
        {
        }
    }
}