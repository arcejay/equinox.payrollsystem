﻿using System;
using System.ComponentModel;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;
using Equinox.PayrollSystem.Module.Enums;

namespace Equinox.PayrollSystem.Module.BusinessObjects
{
    [ModelDefault("Caption", "PhilHealth Contrib Schedule")]
    [DefaultProperty("Description")]
    [DefaultClassOptions, NavigationItem(NavigationItemName.Lookup)]
    public class PhilhealthContributionSummarySchedule : XPObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        public string Reference
        {
            get { return GetPropertyValue<string>("Reference"); }
            set { SetPropertyValue("Reference", value); }
        }

        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public Status Status
        {
            get { return GetPropertyValue<Status>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        public PhilhealthContributionSummarySchedule(Session session) : base(session)
        {

        }

        [Association, Aggregated]
        public XPCollection<PhilhealthContributionItem> PhilhealthContributionTable
        {
            get { return GetCollection<PhilhealthContributionItem>("PhilhealthContributionTable"); }
        }
    }
}