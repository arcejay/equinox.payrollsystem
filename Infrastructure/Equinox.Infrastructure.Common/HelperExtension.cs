﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace Equinox.Infrastructure.Common
{
    public static class HelperExtension
    {

        private const string Alphanumeric = "0123456789";
        //private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        public static string GetUniqueKey(this BaseObject obj, int digits = 6)
        {
            return GenerateKey(digits);

        }

        public static string GetUniqueKey(this XPObject obj, int digits = 6)
        {
            return GenerateKey(digits);
        }

        private static string GenerateKey(int digits = 6)
        {
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(Alphanumeric, digits)
                    .Select(s => s[random.Next(s.Length)])
                    .ToArray());


            return $"{DateTime.Now.Year}{DateTime.Now.Month:00}-{result}";
        }
    }
}
