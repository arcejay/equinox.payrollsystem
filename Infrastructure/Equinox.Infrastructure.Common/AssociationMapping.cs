﻿namespace Equinox.Infrastructure.Common
{
    public class AssociationMapping
    {
        public const string ProductInventoryItems = "Product-InventoryItems";
        public const string QuickNoteMemos = "QuickNote-Memos";
        public const string PurchaseOrderMemos = "PurchaseOrder-Memos";
        public const string QuickQuoteQuickQuoteItems = "QuickQuote-QuickQuoteItems";
        public const string PurchaseOrderPurchaseOrderItems = "PurchaseOrder-PurchaseOrderItems";
        public const string QuickQuoteExpenses = "QuickQuote-Espenses";
        public const string PurchaseOrderQuickNotes = "PurchaseOrder-QuickNotes";
        public const string MachineMachinePartInfos = "Machine-MachinePartInfos";
        public const string CustomerMachines = "Customer-Machines";
        public const string JobTicketMachines = "JobTickets-Machines";
        public const string EngineerJobTickets = "Engineer-JobTickets";
        public const string JobTicketJobTicketNotes = "JobTicket-JobTicketNotes";
        public const string JobTicketFileAttachments = "JobTicket-FileAttachments";
        public const string CustomerJobTickets = "Customer-JobTickets";
        public const string EmployeeJobTicketNotes = "Employee-JobTicketNotes";
        public const string CustomerCompanyAddresses = "CustomerCompany-Addresses";
        public const string CustomerAddresses = "Customer-Addresses";
        public const string CustomerCompany = "Customer-Companny";
        public const string CompanyPhoneNumbers = "Company-PhoneNumbers";
        public const string DepartmentDivisions = "Department-Divisions";
        public const string CompanyDepartments = "Company-Departments";
        public const string PersonPhones = "Person-Phones";
        public const string EmployeeAddresses = "Employee-Addresses";
        public const string CompanyBranches = "Company-Branches";
        public const string BillingStatementPayments = "BillingStatement-Payments";
        public const string ProvinceTowns = "ProvinceTowns";
        public const string PurchaseOrderExpenseItems = "PurchaseOrder-ExpenseItems";
        public const string VendorQuickQuotes = "Vendor-QuickQuotes";

    }

    public class AssociationName
    {
        public const string TownCityAddresses = "TownCityAddresses";
        public const string ProvinceTowns = "ProvinceTowns";
        public const string TownCityBarangays = "TownCityBarangays";
        public const string BarangaySubdivisions = "BarangaySubdivisions";
        public const string DeviceTimeLogs = "DeviceTimeLogs";
        public const string EmployeeSubordinates = "EmployeeSubordinates";
        public const string CompanyBranches = "CompanyBranches";
        public const string EmployeeCashAdvances = "EmployeeCashAdvances";
        public const string WorkScheduleEmployees = "WorkScheduleEmployees";
        public const string PlantillaPositions = "PlantillaPositions";
        public const string PositionEmployees = "PositionEmployees";
        public const string DepartmentDivisions = "DepartmentDivisions";
        public const string CompanyDepartments = "CompanyDepartments";
        public const string DepartmentEmployees = "DepartmentEmployees";
        public const string CompanyEmployees = "CompanyEmployees";
        public const string EmployeeLeaveCredits = "EmployeeLeaveCredits";
        public const string PayrollPayrollItems = "PayrollPayrollItems";
        public const string EmployeePayrollItems = "EmployeePayrollItems";
        public const string PayrollItemDeductions = "PayrollItemDeductions";
        public const string PayrollItemDailyTimeRecords = "PayrollItemDailyTimeRecords";
        public const string EmployeeDailyTimeRecords = "EmployeeDailyTimeRecords";
        public const string OfficialBusinessEmployees = "OfficialBusinessEmployees";
        public const string EmployeeBonusAdjustments = "EmployeeBonusAdjustments";
        public const string PayrollItemBonusAdjustments = "PayrollItemBonusAdjustments";
        public const string EmployeeLeaveApplications = "EmployeeLeaveApplications";
        public const string CashAdvanceCashAdvancePayments = "CashAdvanceCashAdvancePayments";
        public const string EmployeeServiceRecords = "EmployeeServiceRecords";
        public const string OfficialBusinessAttachments = "OfficialBusinessAttachments";
        public const string HolidayScheduleHolidays = "HolidayScheduleHolidays";
        public const string EmployeeOfficialBusinesses = "EmployeeOfficialBusinesses";
        public const string PayrollItemOfficialBusinesses = "PayrollItemOfficialBusinesses";
    }
}