﻿namespace Equinox.Infrastructure.Common
{
    public class NavigationItemName
    {
        public const string Customers = "Customers";
        public const string Employees = "Employees";
        public const string Services = "Services";
        public const string Vendors = "Vendors";
        public const string Accounting = "Accounting";
        public const string Sales = "Sales";
        public const string Inventories = "Inventories";

        public const string Lookup = "Lookup";
        public const string HumanResources = "Human Resources";
        public const string Payroll = "Payroll";
        public const string TimeKeeping = "TimeKeeping";
        public const string Settings = "Settings";

    }
}