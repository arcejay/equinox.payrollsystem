﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace ParcelTrackingService.Services
{
    public class DhlTrackingService : IParcelTrackingService
    {
        private readonly string _url = "http://www.dhl.com.ph/shipmentTracking?AWB={0}&countryCode=ph&languageCode=en";

        public string Result { get; set; }

        public async Task<string> Track(string trackingNumber)
        {
            var url = string.Format(_url, trackingNumber);
            var userAgent = @"37.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";
            var referer = string.Format("http://www.dhl.com.ph/en/express/tracking.html?AWB={0}&brand=DHL",
                trackingNumber);
            var cookie =
                string.Format(
                    @"AWBS_crossrefpar1_taskcenter_taskcentertabs_item1229046233349_par_expandablelink_insideparsys_fasttrack={0}; BRAND_crossrefpar1_taskcenter_taskcentertabs_item1229046233349_par_expandablelink_insideparsys_fasttrack=DHL%20Air%20waybill; TS016f3c0b=01914b743d67ceaebcc4872568d5e613e605a95ad150f74380071be11f5add0b45e94a1a4a; WT_FPC=id=920ccef.000-6978-44f9-9a7a-ce781ec0da62:lv=1427824880514:ss=1427824414986",
                    trackingNumber);

            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.Host] = @"www.dhl.com.ph";
                client.Headers[HttpRequestHeader.Accept] = "application/json, text/javascript, */*; q=0.01";
                client.Headers["X-Requested-With"] = "XMLHttpRequest";
                client.Headers[HttpRequestHeader.UserAgent] = userAgent;
                client.Headers[HttpRequestHeader.Referer] = referer;
                client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate, sdch";
                client.Headers[HttpRequestHeader.AcceptLanguage] = "en-US,en;q=0.8";
                client.Headers[HttpRequestHeader.Cookie] = cookie;

                var result = await client.DownloadStringTaskAsync(new Uri(url));

                return result;
            }
        }
    }

}