﻿using System.Threading.Tasks;

namespace ParcelTrackingService
{
    public interface IParcelTrackingService
    {
        string Result { get; set; }

        Task<string> Track(string trackingNumber);
    }
}