namespace Equinox.Forms.Module.BusinessObjects
{
    public enum DocumentStatus
    {
        InProgress,Pending, Approved, Cancelled
    }
}