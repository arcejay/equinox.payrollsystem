﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpo;

namespace Equinox.Forms.Module.BusinessObjects
{
    public class CashVoucherItem : LineItem
    {
        [Association]
        public CashVoucher CashVoucher
        {
            get { return GetPropertyValue<CashVoucher>("CashVoucher"); }
            set { SetPropertyValue("CashVoucher", value); }
        }

        public CashVoucherItem(Session session) : base(session)
        {
        }
    }
}
