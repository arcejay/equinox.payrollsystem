using System;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;
using Veti.SequenceIdGenerator.Module.BusinessObjects;

namespace Equinox.Forms.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Forms")]
    public class FundRequest : VetiPersistentObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        [PersistentAlias("concat('FR-',PadLeft(ToStr(SequentialNumber),6,'0'))")]
        public string FrNumber
        {
            get { return Convert.ToString(EvaluateAlias("FrNumber")); }
        }

        public string QbRefNumber
        {
            get { return GetPropertyValue<string>("QbRefNumber"); }
            set { SetPropertyValue("QbRefNumber", value); }
        }

        public Department Department
        {
            get { return GetPropertyValue<Department>("Department"); }
            set { SetPropertyValue("Department", value); }
        }

        [RuleRequiredField]
        [DataSourceProperty("Department.Employees")]
        public Employee RequestedBy
        {
            get { return GetPropertyValue<Employee>("RequestedBy"); }
            set { SetPropertyValue("RequestedBy", value); }
        }

        [RuleRequiredField]
        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public decimal TotalAmount
        {
            get
            {
                var results = LineItems.Sum(x => x.Total);
                return results;
            }
        }
        
        public DocumentStatus DocumentStatus
        {
            get { return GetPropertyValue<DocumentStatus>("DocumentStatus"); }
            set { SetPropertyValue("DocumentStatus", value); }
        }

        [ImmediatePostData]
        [Association, Aggregated]
        public XPCollection<FundRequestItem> LineItems => GetCollection<FundRequestItem>("LineItems");

        public FundRequest(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Date = DateTime.Now;
        }
    }

    public class FundRequestItem : LineItem 
    {
        [Association]
        public FundRequest FundRequest
        {
            get { return GetPropertyValue<FundRequest>("FundRequest"); }
            set { SetPropertyValue("FundRequest", value); }
        }

        public FundRequestItem(Session session) : base(session)
        {

        }

    }
}