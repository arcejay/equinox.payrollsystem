﻿using DevExpress.Xpo;
using System;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;
using Veti.SequenceIdGenerator.Module.BusinessObjects;

namespace Equinox.Forms.Module.BusinessObjects
{
    [DefaultClassOptions, NavigationItem("Forms")]
    public class Liquidation : VetiPersistentObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        [PersistentAlias("concat('LR-',PadLeft(ToStr(SequentialNumber),6,'0'))")]
        public string LrNumber
        {
            get { return Convert.ToString(EvaluateAlias("LrNumber")); }
        }

        public string QbRefNumber
        {
            get { return GetPropertyValue<string>("QbRefNumber"); }
            set { SetPropertyValue("QbRefNumber", value); }
        }

        public Department Department
        {
            get { return GetPropertyValue<Department>("Department"); }
            set { SetPropertyValue("Department", value); }
        }

        [RuleRequiredField]
        [DataSourceProperty("Department.Employees")]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [RuleRequiredField]
        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public decimal FundSubjectForLiquidation
        {
            get { return GetPropertyValue<decimal>("FundSubjectForLiquidation"); }
            set { SetPropertyValue("FundSubjectForLiquidation", value); }
        }

        [PersistentAlias("Expenses[].Sum(Amount)")]
        public decimal LiquidatedTotalAmount => Convert.ToDecimal(EvaluateAlias("LiquidatedTotalAmount"));

        [PersistentAlias("FundSubjectForLiquidation-LiquidatedTotalAmount")]
        public decimal UnliquidatedAmount => Convert.ToDecimal(EvaluateAlias("UnliquidatedAmount"));

        [Association, Aggregated, ImmediatePostData]
        public XPCollection<ExpenseItem> Expenses => GetCollection<ExpenseItem>("Expenses");

        public Liquidation(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Date = DateTime.Now;
        }
    }
}