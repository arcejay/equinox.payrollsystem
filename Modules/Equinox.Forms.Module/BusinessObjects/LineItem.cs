using System;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace Equinox.Forms.Module.BusinessObjects
{
    public class LineItem : XPObject
    {
        [RuleRequiredField]
        public string ItemDescription
        {
            get { return GetPropertyValue<string>("ItemDescription"); }
            set { SetPropertyValue("ItemDescription", value); }
        }

        public double Quantity
        {
            get { return GetPropertyValue<double>("Quantity"); }
            set { SetPropertyValue("Quantity", value); }
        }

        [RuleRequiredField]
        public float UnitCost
        {
            get { return GetPropertyValue<float>("UnitCost"); }
            set { SetPropertyValue("UnitCost", value); }
        }

        [PersistentAlias("Quantity*UnitCost")]
        public decimal Total
        {
            get
            {
                var result = EvaluateAlias("Total");

                return Convert.ToDecimal(result);
            }
        }

        public LineItem(Session session) : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Quantity = 1.0f;
        }
    }
}