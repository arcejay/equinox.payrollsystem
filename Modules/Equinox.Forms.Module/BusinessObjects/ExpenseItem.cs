using System;
using DevExpress.Xpo;

namespace Equinox.Forms.Module.BusinessObjects
{
    public class ExpenseItem : XPObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public string ReferenceNumber
        {
            get { return GetPropertyValue<string>("ReferenceNumber"); }
            set { SetPropertyValue("ReferenceNumber", value); }
        }

        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set { SetPropertyValue("Amount", value); }
        }

        [Association]
        public Liquidation Liquidation
        {
            get { return GetPropertyValue<Liquidation>("Liquidation"); }
            set { SetPropertyValue("Liquidation", value); }
        }

        public ExpenseItem(Session session) : base(session)
        {
        }
    }
}