using System;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.Settings;

namespace Equinox.Forms.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Forms")]
    public class CashVoucher : XPObject
    {
        public DateTime Date
        {
            get { return GetPropertyValue<DateTime>("Date"); }
            set { SetPropertyValue("Date", value); }
        }

        //[PersistentAlias("concat('CV-',PadLeft(ToStr(SequentialNumber),6,'0'))")]
        //public string CvNumber
        //{
        //    get { return Convert.ToString(EvaluateAlias("CvNumber")); }
        //}
        
        public Department RequestingDepartment
        {
            get { return GetPropertyValue<Department>("RequestingDepartment"); }
            set { SetPropertyValue("RequestingDepartment", value); }
        }

        [DataSourceProperty("RequestingDepartment.Employees")]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }

        [RuleRequiredField]
        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }
        
        public decimal TotalRequestedAmount
        {
            get
            {
                var results = LineItems.Sum(x => x.Total);
                return results;
            }
        }

        public DocumentStatus DocumentStatus
        {
            get { return GetPropertyValue<DocumentStatus>("DocumentStatus"); }
            set { SetPropertyValue("DocumentStatus", value); }
        }

        [ImmediatePostData]
        [Association, Aggregated]
        public XPCollection<CashVoucherItem> LineItems => GetCollection<CashVoucherItem>("LineItems");

        public CashVoucher(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Date = DateTime.Now;
        }

        
    }
}