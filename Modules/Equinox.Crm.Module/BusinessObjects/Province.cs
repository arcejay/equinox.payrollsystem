using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Lookup)]
    public class Province : XPObject
    {
        public Province(Session session) : base(session)
        {
            
        }

        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public string ShortName
        {
            get { return GetPropertyValue<string>("ShortName"); }
            set { SetPropertyValue("ShortName", value); }
        }

        [Association(AssociationMapping.ProvinceTowns), Aggregated]
        public XPCollection<City> CityOrTowns => GetCollection<City>("CityOrTowns");
    }
}