using DevExpress.Xpo;

namespace Equinox.Crm.Module.BusinessObjects
{
    public class VendorBankAccount : BankAccount
    {
        public VendorBankAccount(Session session)
            : base(session)
        {
        }

        [Association]
        public Vendor Vendor
        {
            get { return GetPropertyValue<Vendor>("Vendor"); }
            set { SetPropertyValue("Vendor", value); }
        }
    }
}