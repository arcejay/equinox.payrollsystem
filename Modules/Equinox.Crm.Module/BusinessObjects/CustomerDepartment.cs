using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    public class CustomerDepartment : XPObject
    {
        public CustomerDepartment(Session session)
            : base(session)
        {
        }

        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        [Association(AssociationMapping.CompanyDepartments)]
        public CustomerCompany Company
        {
            get { return GetPropertyValue<CustomerCompany>("Company"); }
            set { SetPropertyValue("Company", value); }
        }

        [Association(AssociationMapping.DepartmentDivisions)]
        public CustomerDepartment ParentDepartment
        {
            get { return GetPropertyValue<CustomerDepartment>("ParentDepartment"); }
            set { SetPropertyValue("ParentDepartment", value); }
        }

        [Association(AssociationMapping.DepartmentDivisions), Aggregated]
        public XPCollection<CustomerDepartment> Divisions => GetCollection<CustomerDepartment>("Divisions");
    }
}