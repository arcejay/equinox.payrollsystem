using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Crm.Module.Enums;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [DefaultClassOptions, ModelDefault("Caption", "Customer")]
    [NavigationItem(NavigationItemName.Customers)]
    //[DefaultProperty(DisplayName)]
    public class Customer : PersonBase
    {
        public Customer(Session session)
            : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Status = PersonStatus.Active;
        }

        public new string FullName
        {
            get
            {
                if (CustomerType == CustomerType.Corporate)
                {
                    return Company.Name;
                }
                
                return base.FullName;
            }
        }

        [ImmediatePostData]
        public CustomerType CustomerType
        {
            get { return GetPropertyValue<CustomerType>("CustomerType"); }
            set { SetPropertyValue("CustomerType", value); }
        }

        [Association(AssociationMapping.CustomerCompany)]
        [Appearance("CompanyVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "CustomerType <>'Corporate'", Context = "DetailView")]
        public CustomerCompany Company
        {
            get { return GetPropertyValue<CustomerCompany>("Company"); }
            set { SetPropertyValue("Company", value); }
        }

        [DataSourceProperty("Company.Departments")]
        [Appearance("DepartmentVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "CustomerType <>'Corporate'", Context = "DetailView")]
        public CustomerDepartment Department
        {
            get { return GetPropertyValue<CustomerDepartment>("Department"); }
            set { SetPropertyValue("Department", value); }
        }

        [Association(AssociationMapping.CustomerAddresses)]
        [Appearance("AddressVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "CustomerType <>'Walkin'", Context = "DetailView")]
        public CustomerAddress Address
        {
            get { return GetPropertyValue<CustomerAddress>("Address"); }
            set { SetPropertyValue("Address", value); }
        }

    }

    public class CustomerAddress : AddressBase
    {
        public CustomerAddress(Session session) : base(session)
        {}


        [Association(AssociationMapping.CustomerAddresses)]
        public XPCollection<Customer> Customers
        {
            get { return GetCollection<Customer>("Customers"); }
        }

    }

    public enum CustomerType
    {
        Walkin,
        Corporate
    }
}
