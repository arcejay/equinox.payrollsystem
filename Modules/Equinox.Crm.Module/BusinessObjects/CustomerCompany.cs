using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Customers)]
    public class CustomerCompany : XPObject
    {

        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set
            {
                if (SetPropertyValue("Name", value) && string.IsNullOrEmpty(ShortName))
                {
                    if (value.Length <=4) return;

                    var result = value.Replace(",", "").Replace(".", "");
                    var array = result.Split(" ".ToCharArray()).Select(x => x.ToUpper()[0]).ToList();

                    ShortName = array.Count == 1 ? array[0].ToString() : string.Join("", array);
                }
            }}

        public string ShortName
        {
            get { return GetPropertyValue<string>("ShortName"); }
            set
            {
                SetPropertyValue("ShortName", value);
            }
        }
        
        public string WebAddress
        {
            get { return GetPropertyValue<string>("WebAddress"); }
            set { SetPropertyValue("WebAddress", value); }

        }

        [Association(AssociationMapping.CompanyBranches)]
        public CustomerCompany ParentCompany
        {
            get { return GetPropertyValue<CustomerCompany>("ParentCompany"); }
            set { SetPropertyValue("ParentCompany", value); }
        }


        [Association(AssociationMapping.CompanyBranches)]
        public XPCollection<CustomerCompany> Branches => GetCollection<CustomerCompany>("Branches");

        [Association(AssociationMapping.CustomerCompanyAddresses), Aggregated]
        public XPCollection<CustomerCompanyAddress> Addresses => GetCollection<CustomerCompanyAddress>("Addresses");

        [Association(AssociationMapping.CustomerCompany), Aggregated]
        public XPCollection<Customer> Employees => GetCollection<Customer>("Employees");

        [Association(AssociationMapping.CompanyDepartments), Aggregated]
        public XPCollection<CustomerDepartment> Departments => GetCollection<CustomerDepartment>("Departments");

        public CustomerCompany(Session session)
            : base(session)
        {

        }
    }
}