using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [ ImageName("BO_Address")]
    public class AddressBase : XPObject
    {
        [PersistentAlias("concat(Iif(IsNull(StreetName),'',StreetName),', ', IIf(IsNull(Subdivision),'',Subdivision),', ', Iif(IsNull(Barangay),'',Barangay),', ', Iif(IsNull(CityOrTown),'',CityOrTown),', ', Iif(IsNull(Province),'',Province) )")]
        public string FullName => Convert.ToString(EvaluateAlias("FullName"));


        public Province Province
        {
            get { return GetPropertyValue<Province>("Province"); }
            set { SetPropertyValue("Province", value); }
        }
        [Association(AssociationName.TownCityAddresses)]
        [DataSourceProperty("Province.Towns")]
        public City CityOrTown
        {
            get { return GetPropertyValue<City>("CityOrTown"); }
            set { SetPropertyValue("CityOrTown", value); }
        }

        [DataSourceProperty("CityOrTown.Barangays")]
        public Barangay Barangay
        {
            get { return GetPropertyValue<Barangay>("Barangay"); }
            set { SetPropertyValue("Barangay", value); }
        }

        public string StreetName
        {
            get { return GetPropertyValue<string>("StreetName"); }
            set { SetPropertyValue("StreetName", value); }
        }

        public string Subdivision
        {
            get { return GetPropertyValue<string>("Subdivision"); }
            set { SetPropertyValue("Subdivision", value); }
        }


        public AddressBase(Session session)
            : base(session)
        {

        }

    }
}