using System.ComponentModel;
using DevExpress.Xpo;
using Equinox.CommonModule;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [DefaultProperty("PhoneNumber")]
    public class Phone : XPObject
    {
        public string PhoneNumber
        {
            get { return GetPropertyValue<string>("PhoneNumber"); }
            set { SetPropertyValue("PhoneNumber", value); }
        }

        public PhoneType PhoneType
        {
            get { return GetPropertyValue<PhoneType>("PhoneType"); }
            set { SetPropertyValue("PhoneType", value); }
        }

        public Phone(Session session) : base(session)
        {

        }
    }
}