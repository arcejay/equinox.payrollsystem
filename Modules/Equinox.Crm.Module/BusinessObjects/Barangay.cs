using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [DefaultProperty("Name")]
    [DefaultClassOptions, NavigationItem(NavigationItemName.Lookup)]
    public class Barangay : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        //[ProvidedAssociation(AssociationName.TownCityBarangays)]
        [Association(AssociationName.TownCityBarangays)]
        public City Town
        {
            get { return GetPropertyValue<City>("Town"); }
            set { SetPropertyValue("Town", value); }
        }

        [Association(AssociationName.BarangaySubdivisions)]
        public XPCollection<Subdivision> Subdivisions => GetCollection<Subdivision>("Subdivisions");


        public Barangay(Session session) : base(session)
        {

        }
    }
}