namespace Equinox.Crm.Module.BusinessObjects
{
    public enum VetiDepartment
    {
        Helpdesk,
        Service,
        Billing,
        Admin
    }
}