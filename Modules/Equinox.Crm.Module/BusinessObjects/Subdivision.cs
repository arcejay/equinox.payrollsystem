using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [DefaultProperty("Name")]
    [DefaultClassOptions]
    public class Subdivision : XPObject
    {
        public string Name{
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        [Association(AssociationName.BarangaySubdivisions)]
        public Barangay Barangay
        {
            get { return GetPropertyValue<Barangay>("Barangay"); }
            set { SetPropertyValue("Barangay", value); }
        }


        public Subdivision(Session session) : base(session)
        {

        }
    }
}