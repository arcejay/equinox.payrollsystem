using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    public class CustomerCompanyAddress : AddressBase
    {
        public CustomerCompanyAddress(Session session)
            : base(session)
        {
        }

        [Association(AssociationMapping.CustomerCompanyAddresses)]
        public CustomerCompany Company
        {
            get { return GetPropertyValue<CustomerCompany>("Company"); }
            set { SetPropertyValue("Company", value); }
        }
    }
}