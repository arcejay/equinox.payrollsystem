using DevExpress.Xpo;

namespace Equinox.Crm.Module.BusinessObjects
{
    public class BankAccount : XPObject
    {
        public BankAccount(Session session) : base(session)
        {
            
        }

        public string AccountName
        {
            get { return GetPropertyValue<string>("AccountName"); }
            set { SetPropertyValue("AccountName", value); }
            
        }

        public string AccountNumber
        {
            get { return GetPropertyValue<string>("AccountNumber"); }
            set { SetPropertyValue("AccountNumber", value); }
            
        }

        public string AccountAddress
        {
            get { return GetPropertyValue<string>("AccountAddress"); }
            set { SetPropertyValue("AccountAddress", value); }
        }

        public string BankName
        {
            get { return GetPropertyValue<string>("BankName"); }
            set { SetPropertyValue("BankName", value); }
        }

        public string Branch
        {
            get { return GetPropertyValue<string>("Branch"); }
            set { SetPropertyValue("Branch", value); }
        }
    }
}