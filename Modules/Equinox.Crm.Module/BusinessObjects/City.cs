using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Infrastructure.Common;

namespace Equinox.Crm.Module.BusinessObjects
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Lookup)]
    public class City : XPObject
    {
        public City(Session session) : base(session)
        {
            
        }

        public string Name { get; set; }

        public string ZipCode { get; set; }

        [Association(AssociationName.TownCityBarangays)]
        public XPCollection<Barangay> Barangays
        {
            get { return GetCollection<Barangay>("Barangays"); }
        }

        [Association(AssociationName.TownCityAddresses), Aggregated]
        public XPCollection<AddressBase> Addresses => GetCollection<AddressBase>("Addresses");

        [Association(AssociationMapping.ProvinceTowns)]
        public Province Province
        {
            get { return GetPropertyValue<Province>("Province"); }
            set { SetPropertyValue("Province", value); }
        }
    }
}