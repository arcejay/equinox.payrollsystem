namespace Equinox.Crm.Module.Enums
{
    public enum VendorStatus
    {
        Active,
        Inactive
    }
}