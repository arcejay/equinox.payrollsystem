namespace Equinox.Crm.Module.Enums
{
    public enum PersonStatus
    {
        Active,
        NotActive
    }
}