using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Xpo;
using Veti.SequenceIdGenerator.Module.Utils;

namespace Veti.SequenceIdGenerator.Module.BusinessObjects
{
    //Dennis: Uncomment this code if you want to have the SequentialNumber column created in each derived class table.
    [NonPersistent]
    public abstract class VetiPersistentObject : XPObject, ISupportSequentialNumber
    {
        private long _sequentialNumber;
        private static SequenceGenerator _sequenceGenerator;
        private static object _syncRoot = new object(); 

        public VetiPersistentObject(Session session)
            : base(session)
        {
        }

        [Browsable(false)]
        //Dennis: Comment out this code if you do not want to have the SequentialNumber column created in each derived class table.
        [Indexed(Unique = false)]
        public long SequentialNumber
        {
            get { return _sequentialNumber; }
            set { SetPropertyValue("SequentialNumber", ref _sequentialNumber, value); }
        }

        private void OnSequenceGenerated(long newId)
        {
            SequentialNumber = newId;
        }

        protected override void OnSaving()
        {
            try
            {
                base.OnSaving();
                if (Session.IsNewObject(this) && !typeof(NestedUnitOfWork).IsInstanceOfType(Session))
                    GenerateSequence();
            }
            catch
            {
                CancelSequence();
                throw;
            }
        }
        public void GenerateSequence()
        {
            lock (_syncRoot)
            {
                Dictionary<string, bool> typeToExistsMap = new Dictionary<string, bool>();

                foreach (object item in Session.GetObjectsToSave())
                    typeToExistsMap[Session.GetClassInfo(item).FullName] = true;

                if (_sequenceGenerator == null)
                    _sequenceGenerator = new SequenceGenerator(typeToExistsMap);

                SubscribeToEvents();

                OnSequenceGenerated(_sequenceGenerator.GetNextSequence(ClassInfo));
            }
        }

        private void AcceptSequence()
        {
            lock (_syncRoot)
            {
                if (_sequenceGenerator != null)
                {
                    try
                    {
                        _sequenceGenerator.Accept();
                    } finally
                    {
                        CancelSequence();
                    }
                }
            }
        }
        private void CancelSequence()
        {
            lock (_syncRoot)
            {
                UnSubscribeFromEvents();
                if (_sequenceGenerator != null)
                {
                    _sequenceGenerator.Close();
                    _sequenceGenerator = null;
                }
            }
        }

        private void Session_AfterCommitTransaction(object sender, SessionManipulationEventArgs e)
        {
            AcceptSequence();
        }

        private void Session_AfterRollBack(object sender, SessionManipulationEventArgs e)
        {
            CancelSequence();
        }

        private void Session_FailedCommitTransaction(object sender, SessionOperationFailEventArgs e)
        {
            CancelSequence();
        }

        private void SubscribeToEvents()
        {
            if (!(Session is NestedUnitOfWork))
            {
                Session.AfterCommitTransaction += Session_AfterCommitTransaction;
                Session.AfterRollbackTransaction += Session_AfterRollBack;
                Session.FailedCommitTransaction += Session_FailedCommitTransaction;
            }
        }

        private void UnSubscribeFromEvents()
        {
            if (!(Session is NestedUnitOfWork))
            {
                Session.AfterCommitTransaction -= Session_AfterCommitTransaction;
                Session.AfterRollbackTransaction -= Session_AfterRollBack;
                Session.FailedCommitTransaction -= Session_FailedCommitTransaction;
            }
        }
    }
}