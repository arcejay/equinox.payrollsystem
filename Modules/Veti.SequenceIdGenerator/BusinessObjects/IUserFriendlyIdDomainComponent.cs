﻿using DevExpress.ExpressApp.DC;
using Veti.SequenceIdGenerator.Module.Utils;

namespace Veti.SequenceIdGenerator.Module.BusinessObjects
{
    [DomainComponent]
    public interface IUserFriendlyIdDomainComponent : ISupportSequentialNumber
    {
        
    }
}