using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;

namespace Veti.SequenceIdGenerator.Module.Utils
{
    public static class SequenceGeneratorInitializer
    {
        private static XafApplication application;
        private static XafApplication Application { get { return application; } }

        public static void Register(XafApplication app)
        {
            application = app;
            if (application != null)
                application.LoggedOn += new EventHandler<LogonEventArgs>(application_LoggedOn);
        }

        private static void application_LoggedOn(object sender, LogonEventArgs e)
        {
            Initialize();
        }

        //Dennis: It is important to set the SequenceGenerator.DefaultDataLayer property to the main application data layer.
        //If you use a custom IObjectSpaceProvider implementation, ensure that it exposes a working IDataLayer.
        public static void Initialize()
        {
            Guard.ArgumentNotNull(Application, "Application");
            XPObjectSpaceProvider provider = Application.ObjectSpaceProvider as XPObjectSpaceProvider;
            Guard.ArgumentNotNull(provider, "provider");

            if (provider.DataLayer == null) {
                //Dennis: This call is necessary to initialize a working data layer.
                provider.CreateObjectSpace();
            }

            if (provider.DataLayer is ThreadSafeDataLayer) {
                //Dennis: We have to use a separate datalayer for the sequence generator because ThreadSafeDataLayer is usually used for ASP.NET applications.
                SequenceGenerator.DefaultDataLayer = XpoDefault.GetDataLayer(
                    Application.Connection == null ? Application.ConnectionString : Application.Connection.ConnectionString,
                    XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary,
                    DevExpress.Xpo.DB.AutoCreateOption.None
                    );
            }
            else
            {
                SequenceGenerator.DefaultDataLayer = provider.DataLayer;
            }
        }
    }
}