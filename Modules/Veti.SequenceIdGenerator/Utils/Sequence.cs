using DevExpress.Xpo;

namespace Veti.SequenceIdGenerator.Module.Utils
{
    public class Sequence : XPBaseObject
    {
        private string typeName;
        private long nextSequence;

        public Sequence(Session session) : base(session)
        {
            
        }

        [Key]
        //Dennis: The size should be enough to store a full type name. However, you cannot use unlimited size for key columns.
        [Size(255)]
        public string TypeName
        {
            get { return typeName; }
            set { SetPropertyValue("TypeName", ref typeName, value); }
        }

        public long NextSequence
        {
            get { return nextSequence; }
            set { SetPropertyValue("NextSequence", ref nextSequence, value); }
        }
    }
}