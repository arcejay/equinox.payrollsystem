namespace Veti.SequenceIdGenerator.Module.Utils
{
    public interface ISupportSequentialNumber
    {
        long SequentialNumber { get; set; }
    }
}