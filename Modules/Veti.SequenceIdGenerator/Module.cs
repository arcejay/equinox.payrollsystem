using DevExpress.ExpressApp;
using Veti.SequenceIdGenerator.Module.Utils;

namespace Veti.SequenceIdGenerator.Module {
    public sealed partial class SequenceIdGeneratorModule : ModuleBase {
        public SequenceIdGeneratorModule() {
            InitializeComponent();
        }
        public override void Setup(XafApplication application) {
            base.Setup(application);
            SequenceGeneratorInitializer.Register(application);
        }
    }
}