namespace Equinox.CommonModule
{
    public enum PhoneType
    {
        Landline, Mobile, Fax
    }
}