﻿using System;
using System.Linq;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module
{
    public static class HelperExtension
    {

        private const string alphanumeric = "0123456789";
        //private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        public static string GetUniqueKey(this BaseObject obj, int digits = 6)
        {
            return GenerateKey(digits);

        }

        public static string GetUniqueKey(this XPObject obj, int digits = 6)
        {
            return GenerateKey(digits);
        }

        private static string GenerateKey(int digits = 6)
        {
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(alphanumeric, digits)
                    .Select(s => s[random.Next(s.Length)])
                    .ToArray());


            return string.Format("{0}{1:00}-{2}", DateTime.Now.Year, DateTime.Now.Month,
                result);
        }


    }
}