using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;

namespace EquinoxSystemManagement.Module.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppUpdatingModuleUpdatertopic
    public class Updater : ModuleUpdater {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            //string name = "MyName";
            //DomainObject1 theObject = ObjectSpace.FindObject<DomainObject1>(CriteriaOperator.Parse("Name=?", name));
            //if(theObject == null) {
            //    theObject = ObjectSpace.CreateObject<DomainObject1>();
            //    theObject.Name = name;
            //}

            SecuritySystemRole adminRole = ObjectSpace.FindObject<SecuritySystemRole>(new BinaryOperator("Name", SecurityStrategy.AdministratorRoleName));

            if (adminRole == null)
            {
                adminRole = ObjectSpace.CreateObject<SecuritySystemRole>();
                adminRole.Name = SecurityStrategy.AdministratorRoleName;
                adminRole.IsAdministrative = true;
            }



            var userRole = ObjectSpace.FindObject<SecuritySystemRole>(new BinaryOperator("Name", "User"));

            if (userRole == null)
            {
                userRole = ObjectSpace.CreateObject<SecuritySystemRole>();
                userRole.Name = "User";

                var userTypePermission = ObjectSpace.CreateObject<SecuritySystemTypePermissionObject>();
                userTypePermission.TargetType = typeof(SecuritySystemUser);

                var currentUserObjectPermission = ObjectSpace.CreateObject<SecuritySystemObjectPermissionsObject>();

                currentUserObjectPermission.Criteria = "[Oid] = CurrentUserId()";
                currentUserObjectPermission.AllowNavigate = true;
                currentUserObjectPermission.AllowRead = true;
                userTypePermission.ObjectPermissions.Add(currentUserObjectPermission);
                userRole.TypePermissions.Add(userTypePermission);


            }

            // If a user named 'Sam' does not exist in the database, create this user. 
            SecuritySystemUser user1 = ObjectSpace.FindObject<SecuritySystemUser>(new BinaryOperator("UserName", "Oliver"));

            if (user1 == null)
            {
                user1 = ObjectSpace.CreateObject<SecuritySystemUser>();
                user1.UserName = "Oliver";
                // Set a password if the standard authentication type is used. 
                user1.SetPassword("");
            }

            // If a user named 'John' does not exist in the database, create this user. 
            SecuritySystemUser user2 = ObjectSpace.FindObject<SecuritySystemUser>(new BinaryOperator("UserName", "test"));

            if (user2 == null)
            {
                user2 = ObjectSpace.CreateObject<SecuritySystemUser>();
                user2.UserName = "test";
                // Set a password if the standard authentication type is used. 
                user2.SetPassword("test");
            }

            var japs = ObjectSpace.FindObject<SecuritySystemUser>(new BinaryOperator("UserName", "japs"));

            if (japs == null)
            {
                japs = ObjectSpace.CreateObject<SecuritySystemUser>();
                japs.UserName = "japs";
                japs.SetPassword("");
            }

            // Add the "Administrators" Role to user1. 
            user1.Roles.Add(adminRole);
            // Add the "Users" Role to user2. 
            user2.Roles.Add(userRole);
            japs.Roles.Add(adminRole);}
        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }
    }
}
