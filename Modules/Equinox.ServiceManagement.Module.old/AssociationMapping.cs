﻿namespace EquinoxSystemManagement.Module
{
    public class AssociationMapping
    {
        public const string ProductInventoryItems = "Product-InventoryItems";
        public const string QuickNoteMemos = "QuickNote-Memos";
        public const string PurchaseOrderMemos = "PurchaseOrder-Memos";
        public const string QuickQuoteQuickQuoteItems = "QuickQuote-QuickQuoteItems";
        public const string PurchaseOrderPurchaseOrderItems = "PurchaseOrder-PurchaseOrderItems";
        public const string QuickQuoteExpenses = "QuickQuote-Espenses";
        public const string PurchaseOrderQuickNotes = "PurchaseOrder-QuickNotes";
        public const string MachineMachinePartInfos = "Machine-MachinePartInfos";
        public const string CustomerMachines = "Customer-Machines";
        public const string JobTicketMachines = "JobTickets-Machines";
        public const string EngineerJobTickets = "Engineer-JobTickets";
        public const string JobTicketJobTicketNotes = "JobTicket-JobTicketNotes";
        public const string JobTicketFileAttachments = "JobTicket-FileAttachments";
        public const string CustomerJobTickets = "Customer-JobTickets";
        public const string EmployeeJobTicketNotes = "Employee-JobTicketNotes";
        public const string CompanyAddresses = "Company-Addresses";
        public const string CustomerAddresses = "Customer-Addresses";
        public const string CustomerCompany = "Customer-Companny";
        public const string CompanyPhoneNumbers = "Company-PhoneNumbers";
        public const string DepartmentDivisions = "Department-Divisions";
        public const string CompanyDepartments = "Company-Departments";
        public const string PersonPhones = "Person-Phones";
        public const string EmployeeAddresses = "Employee-Addresses";
        public const string CompanyBranches = "Company-Branches";
        public const string BillingStatementPayments = "BillingStatement-Payments";
        public const string ProvinceTowns = "ProvinceTowns";
        public const string PurchaseOrderExpenseItems = "PurchaseOrder-ExpenseItems";
    }

    public class NavigationItemName
    {
        public const string Inventories = "Inventories";
        public const string Customers = "Customers";
        public const string Services = "Services";
        public const string Vendors = "Vendors";
        public const string Accounting = "Accounting";
        public const string Sales = "Sales";
    }
}