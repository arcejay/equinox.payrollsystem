﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraEditors;
using EquinoxSystemManagement.Module.BusinessObjects;
using Task = System.Threading.Tasks.Task;

namespace EquinoxSystemManagement.Module.Extensions
{
    public static class GmailExtension
    {
        public async static Task<bool> GmailSender(string to, string subject, string content)
        {
            var userAccount = "noreply@veraequinox.com";
            var pass = "D1w4kw4k@3";
            var serverName = "smtp.gmail.com";
            var port = 587;

            return await Task.Run(async () =>
            {
                using (var client = new SmtpClient(serverName, port))
                {

                    client.Credentials = new NetworkCredential(userAccount, pass);
                    client.EnableSsl = true;

                    try
                    {
                        var message = new MailMessage(userAccount, to);
                        message.IsBodyHtml = true;
                        message.Body = content;
                        message.Subject = subject;

                        await client.SendMailAsync(message);

                        return true;
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine(ex.ToString(), ex.Message);
                    }

                    return false;
                }
            });
        }

        public async static Task<bool> SendAsEmail(this JobTicketNote note)
        {
            if (note == null) throw new ArgumentNullException("note");
            if (note.JobTicket == null) throw new ArgumentNullException("note.JobTicket");
            if (note.JobTicket.Customer == null) throw new ArgumentNullException("note.JobTicket.Customer");
            if (string.IsNullOrEmpty(note.JobTicket.Customer.Email)) throw new ArgumentNullException("email");

            var body = string.Format("<html><body><h1>{0}</h1><p>{1}</body></html>", note.Text, note.Author);
            var subject = string.Format("[{0}] {1}", note.JobTicket.JobTicketNumber, note.JobTicket.Machine);

            return await GmailSender(note.JobTicket.Customer.Email, subject, body);
            
        }
    }
}