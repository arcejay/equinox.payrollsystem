using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Threading.Tasks;
using EquinoxSystemManagement.Module.BusinessObjects;
using GsmComm.Interfaces;

namespace EquinoxSystemManagement.Module.Extensions
{
    public static class SmsExtension
    {
        private const string ServerName = "192.168.10.8";public async static Task<bool> SendAsSms(this JobTicketNote note)
        {
            if (note == null) throw new ArgumentNullException("note");
            if (note.JobTicket == null) throw new ArgumentNullException("note.JobTicket");
            //if (note.JobTicket.Customer == null) throw new ArgumentNullException("note.JobTicket.Customer");
            //if (note.JobTicket.Customer.Phones == null) throw new ArgumentNullException("Phone");

            //get the phone whose type is Mobile
            var mobile = note.JobTicket.Customer.Phones.Where(x => x.PhoneType == PhoneType.Mobile).ToList()[0];

            return await SmsSender(mobile.PhoneNumber, note.Text);}

        private static Task<bool> SmsSender(string phoneNumber, string text)
        {
            return Task.Run(() =>
            {
                // Set up a client channel
                IDictionary props = new Hashtable();
                var clientChannel = new TcpClientChannel(props, null);
                ChannelServices.RegisterChannel(clientChannel, false);

                try
                {
                    //var port = int.Parse(ConfigurationManager.AppSettings["SmsServePort"]);
                    //var server = ConfigurationManager.AppSettings["SmsServer"];
                    //Trace.WriteLine(string.Format("Server: {0}, Port: {1}", server, port));
                    //string url = string.Format("tcp://{0}:{1}/SMSSender", server, port);
                    string url = string.Format("tcp://{0}:{1}/SMSSender", ServerName, 2000);

                    var smsSender = (ISmsSender)Activator.GetObject(typeof(ISmsSender), url);
                    smsSender.SendMessage(text, phoneNumber); 
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(string.Format("{0}\r\n\r\nException type: {1}", ex.Message, ex.GetType().ToString()), "Error");
                    return false;
                }
                finally
                {
                    // Destroy client channel
                    ChannelServices.UnregisterChannel(clientChannel);
                    clientChannel = null;
                }

                return true;
            });
        }
    }
}