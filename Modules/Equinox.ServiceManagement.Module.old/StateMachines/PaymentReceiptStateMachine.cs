using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.StateMachine.NonPersistent;
using DevExpress.ExpressApp.StateMachine.Xpo;
using DevExpress.Xpo;
using EquinoxSystemManagement.Module.BusinessObjects.Billing;

namespace EquinoxSystemManagement.Module.StateMachines
{
    public class PaymentReceiptStateMachine : StateMachine<BillingStatement>, IStateMachineUISettings
    {
        private State _createdState;

        public PaymentReceiptStateMachine(IObjectSpace objectSpace) : base(objectSpace)
        {
            _createdState = new State(this, PaymentStatus.Created);var notedState = new State(this, PaymentStatus.Noted);
            var approvedState = new State(this, PaymentStatus.Approved);

            //Created -> (Noted)
            _createdState.Transitions.Add(new Transition(notedState));
            
            //Noted -> (Approved)
            notedState.Transitions.Add(new Transition(approvedState));
            //notedState.TargetObjectCriteria = "JobOrderNumber is not empty";

            //appearances
            var createdStateCondition = new StateAppearance(_createdState)
            {
                TargetItems = "NotedBy; ApprovedBy; DateNoted; DateApproved",
                Enabled = false
            };

            var notedStateCondition = new StateAppearance(notedState)
            {
                TargetItems = "ApprovedBy; DateApproved",
                Enabled = false
            };
            States.Add(_createdState);
            States.Add(notedState);
            States.Add(approvedState);

        }

        public override string Name
        {
            get { return "Change status to"; }
        }

        public override string StatePropertyName
        {
            get { return "Status"; }
        }

        public override IState StartState
        {
            get { return _createdState; }
        }

        public bool ExpandActionsInDetailView { get { return true; } }
    }
}