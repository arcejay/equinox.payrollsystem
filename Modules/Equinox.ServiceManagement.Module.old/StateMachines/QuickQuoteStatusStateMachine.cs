using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.StateMachine.NonPersistent;
using EquinoxSystemManagement.Module.BusinessObjects;

namespace EquinoxSystemManagement.Module.StateMachines
{
    public class QuickQuoteStatusStateMachine : StateMachine<QuickQuote>, IStateMachineUISettings
    {
        private IState _startState;

        public QuickQuoteStatusStateMachine(IObjectSpace objectSpace) : base(objectSpace)
        {
            //inprogress -> 
            _startState = new State(this, QuickQuoteStatus.NotStarted);

            //different states
            var inProgressState = new State(this, QuickQuoteStatus.InProgress);
            var cancelledState = new State(this, QuickQuoteStatus.Cancelled);
            var completedState = new State(this, QuickQuoteStatus.Completed);

            //NotStarted -> (InProgress, Cancelled)
            _startState.Transitions.Add(new Transition(inProgressState));
            _startState.Transitions.Add(new Transition(cancelledState));

            //InProgress -> (Completed, Cancelled)
            inProgressState.Transitions.Add(new Transition(completedState));
            inProgressState.Transitions.Add(new Transition(cancelledState));

            //Cancelled -> (InProgress)
            cancelledState.Transitions.Add(new Transition(inProgressState));

            //Completed -> (InProgress)
            completedState.Transitions.Add(new Transition(inProgressState));
            States.Add(_startState);
            States.Add(inProgressState);
            States.Add(cancelledState);
            States.Add(completedState);}

        public override string Name
        {
            get { return "Change status to"; }
        }

        public override string StatePropertyName
        {
            get { return "Status"; }
        }

        public override IState StartState
        {
            get { return _startState; }
        }

        public bool ExpandActionsInDetailView { get { return true; }  }
    }

}