using DevExpress.ExpressApp;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.StateMachine.NonPersistent;
using EquinoxSystemManagement.Module.BusinessObjects;

namespace EquinoxSystemManagement.Module.StateMachines
{
    public class JobTicketStateMachine : StateMachine<JobTicket>, IStateMachineUISettings
    {
        private IState _startState;

        public JobTicketStateMachine(IObjectSpace objectSpace) : base(objectSpace)
        {
            _startState = new State(this, JobTicketStatus.InProgress);

            var billedState = new State(this, JobTicketStatus.Billed);
            var pullOutState = new State(this, JobTicketStatus.PullOut);
            var releasedState = new State(this, JobTicketStatus.Released);

            //Billed -> (PullOut, Released)
            billedState.Transitions.Add(new Transition(pullOutState));
            billedState.Transitions.Add(new Transition(releasedState));

            //InProgress -> (PullOut, Billed) 
            _startState.Transitions.Add(new Transition(pullOutState));
            _startState.Transitions.Add(new Transition(billedState));

            //PullOut -> (Released, InProgress)
            pullOutState.Transitions.Add(new Transition(releasedState));
            pullOutState.Transitions.Add(new Transition(_startState));
            
            //Released -> ()

            States.Add(_startState);
            States.Add(billedState);
            States.Add(pullOutState);
            States.Add(releasedState);

        }

        public override string Name
        {
            get { return "(Sales) Change status to"; }
        }

        public override string StatePropertyName
        {
            get { return "Status"; }
        }

        public override IState StartState
        {
            get { return _startState; }
        }

        public bool ExpandActionsInDetailView { get { return true; } }
    }
}