namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum PersonStatus
    {
        Active,
        NotActive
    }
}