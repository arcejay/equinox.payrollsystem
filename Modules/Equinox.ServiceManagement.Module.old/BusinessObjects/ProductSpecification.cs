using System;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class ProductSpecification : XPObject
    {
        private string _name;
        private string _value;
        private Product _product;

        public ProductSpecification()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public ProductSpecification(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string Value
        {
            get { return _value; }
            set { SetPropertyValue("Value",  ref _value, value); }
        }

        [Association]
        public Product Product
        {
            get { return _product; }
            set { SetPropertyValue("Product", ref _product, value); }
        }
    }

}