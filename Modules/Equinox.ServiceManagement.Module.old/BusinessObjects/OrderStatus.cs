namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum OrderStatus
    {
        NotStarted,
        InProgress,
        Ordered,
        Closed,
        Shipped,
        Completed}
}