using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class QuickQuoteItem : OrderItem
    {
        private QuickQuote _quickQuote;

        public QuickQuoteItem(Session session)
            : base(session)
        {
            
        }

        public string PartNumber
        {
            get
            {
                if (InventoryItem != null && InventoryItem.Product != null)
                    return this.InventoryItem.Product.ManufacturerPartNumber;

                return null;
            }
        }

        public string OemPartNo
        {
            get
            {
                if (InventoryItem != null && InventoryItem.Product != null)
                    return InventoryItem.Product.OemPartNumber;

                return null;}

        }
        

        public string Description
        {
            get
            {
                if (InventoryItem != null && InventoryItem.Product != null)
                    return this.InventoryItem.Product.Description;

                return null;
            }
        }

        [Association(AssociationMapping.QuickQuoteQuickQuoteItems)]
        public QuickQuote QuickQuote
        {
            get { return _quickQuote; }
            set { SetPropertyValue("QuickQuote", ref _quickQuote, value); }
        }
    }
}