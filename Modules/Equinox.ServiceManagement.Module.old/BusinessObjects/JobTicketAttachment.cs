using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class JobTicketAttachment : FileAttachmentBase
    {
        public JobTicketAttachment(Session session) : base(session)
        {
        }

        [Association(AssociationMapping.JobTicketFileAttachments)]
        public XPCollection<JobTicket> JobTickets { get { return GetCollection<JobTicket>("JobTickets"); } }
    }
}