using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class Phone : XPObject
    {
        public Phone(Session session)
            : base(session)
        {

        }

        public string PhoneNumber
        {
            get { return GetPropertyValue<string>("PhoneNumber"); }
            set { SetPropertyValue("PhoneNumber", value); }
        }

        public PhoneType PhoneType
        {
            get { return GetPropertyValue<PhoneType>("PhoneType"); }
            set { SetPropertyValue("PhoneType", value); }
        }}

    public enum PhoneType
    {
        Landline, Mobile, Fax
    }
}