using System.Threading.Tasks;
using DevExpress.Xpo;
using Equinox.ServiceManagement.Module.BusinessObjects.Tracking;

namespace EquinoxSystemManagement.Module.BusinessObjects.Tracking
{
    public class CourierBase : XPObject
    {

        private string _name;

        public CourierBase(Session session)
            : base(session)
        {

        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Association]
        public XPCollection<TrackingNumber> TrackingNumbers
        {
            get { return GetCollection<TrackingNumber>("TrackingNumbers"); }
        }public virtual async Task<XPCollection<CheckpointItem>> Track(string trackingNumber)
        {
            return await Task.Run(() => new XPCollection<CheckpointItem>());
        }

    }
}