using System;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class OrderItem : XPObject
    {
        private decimal _price;
        private float _quantity;
        private UnitOfMeasure _unitOfMeasure;
        private InventoryItem _inventoryItem;

        public OrderItem()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public OrderItem(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Quantity = 1;
            UnitOfMeasure = Session.FindObject<UnitOfMeasure>(CriteriaOperator.Parse("Name = 'pcs'"));

        }

        [Index(0), Association]
        public InventoryItem InventoryItem
        {
            get { return _inventoryItem; }
            set { SetPropertyValue("InventoryItem", ref _inventoryItem, value); }
        }[Index(1)]
        public float Quantity
        {
            get { return _quantity; }
            set
            {
                if (SetPropertyValue("Quantity", ref _quantity, value))
                    OnChanged("TotalAmount");
            }
            }

        [Index(2)]
        public UnitOfMeasure UnitOfMeasure
        {
            get { return _unitOfMeasure; }
            set { SetPropertyValue("UnitOfMeasure", ref _unitOfMeasure, value); }
        }

        public decimal Price
        {
            get { return _price; }
            set
            {
                if (SetPropertyValue("Price", ref _price, value))
                    OnChanged("TotalAmount");
            }
        }

        [Index(3)]
        [PersistentAlias("Quantity * Price")]
        public decimal TotalAmount
        {
            get
            {
                var objTemp = EvaluateAlias("TotalAmount");
                return objTemp != null ? Convert.ToDecimal(objTemp) : 0;
            }
        }

    }
}