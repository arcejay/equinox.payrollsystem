using System;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Vendor : XPObject
    {
        private string _companyName;
        private string _website;
        private string _email;
        private PhoneNumber _phone;
        private Address _address;
        private VendorStatus _status;

        public Vendor()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Vendor(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        public string CompanyName
        {
            get { return _companyName; }
            set { SetPropertyValue("CompanyName", ref _companyName, value); }
        }

        public string Website
        {
            get { return _website; }
            set { SetPropertyValue("Website", ref _website, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetPropertyValue("Email", ref _email, value); }
        }

        public PhoneNumber Phone
        {
            get { return _phone; }
            set { SetPropertyValue("Phone", ref _phone, value); }
        }
        public Address Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        public VendorStatus Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Association]
        public XPCollection<VendorContact> ContactPersons
        {
            get
            {
                return GetCollection<VendorContact>("ContactPersons");
            }
        }


        [Association]
        public XPCollection<PurchaseOrder> PurchaseOrders
        {
            get { return GetCollection<PurchaseOrder>("PurchaseOrders"); }
        }

        [Association, Aggregated]
        public XPCollection<QuickQuote> QuickQuotes
        {
            get { return GetCollection<QuickQuote>("QuickQuotes"); }
        }

        [Association]
        public XPCollection<VendorBankAccount> BankAccounts
        {
            get
            {
                return GetCollection<VendorBankAccount>("BankAccounts");
            }
        }
    }

    public class VendorBankAccount : BankAccount
    {
        public VendorBankAccount(Session session)
            : base(session)
        {
        }

        [Association]
        public Vendor Vendor
        {
            get { return GetPropertyValue<Vendor>("Vendor"); }
            set { SetPropertyValue("Vendor", value); }
        }
    }

    public class VendorContact : PersonBase
    {
        private string _products;
        private Vendor _vendor;

        public VendorContact(Session session)
            : base(session)
        {
        }

        public string Products
        {
            get { return _products; }
            set { SetPropertyValue("Products", ref _products, value); }
        }

        public string Position
        {
            get { return GetPropertyValue<string>("Position"); }
            set { SetPropertyValue("Position", value); }
        }

        [Association]
        public Vendor Vendor
        {
            get { return _vendor; }
            set { SetPropertyValue("Vendor", ref _vendor, value); }
        }
    }
}