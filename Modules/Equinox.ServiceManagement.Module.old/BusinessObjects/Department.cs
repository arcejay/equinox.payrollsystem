namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum VetiDepartment
    {
        Helpdesk,
        Service,
        Billing,
        Admin
    }
}