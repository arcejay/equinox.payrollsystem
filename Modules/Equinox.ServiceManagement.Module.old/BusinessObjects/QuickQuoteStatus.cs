namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum QuickQuoteStatus
    {
        NotStarted, InProgress,Cancelled, Completed
    }
}