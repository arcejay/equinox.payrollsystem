namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum JobTicketType
    {
        Warranty,
        Walkin,
        Onsite
    }
}