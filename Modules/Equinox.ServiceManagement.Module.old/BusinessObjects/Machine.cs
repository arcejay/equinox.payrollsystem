using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Services)]
    [DefaultProperty("Model")]
    public class Machine : BaseObject{
        private Customer _customer;
        private string _model;private string _brand;
        private string _serialNumber;
        private string _description;

        public Machine(Session session)
            : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            MachineId = this.GetUniqueKey(6);
        }

        public string MachineId
        {
            get { return GetPropertyValue<string>("MachineId"); }
            set { SetPropertyValue("MachineId", value); }
        }


        public string Model
        {
            get { return _model; }
            set { SetPropertyValue("Model", ref _model, value); }
        }


        public string Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        public string SerialNumber
        {
            get { return _serialNumber; }
            set { SetPropertyValue("SerialNumber", ref _serialNumber, value); }
        }

        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Association(AssociationMapping.CustomerMachines)]
        public Customer Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [Association(AssociationMapping.MachineMachinePartInfos), Aggregated]
        public XPCollection<MachinePartInfo> Specifications{
            get
            {
                return GetCollection<MachinePartInfo>("Specifications");
            }
        }

        [Association(AssociationMapping.JobTicketMachines)]
        public XPCollection<JobTicket> JobTickets { get { return GetCollection<JobTicket>("JobTickets"); } } 

    }
}