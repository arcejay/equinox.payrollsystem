using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects.Sales
{
    [NavigationItem(NavigationItemName.Sales)]
    public class SalesOrder : VetiObject
    {
        public SalesOrder(Session session)
            : base(session)
        {

        }public string SalesOrderDate
        {
            get { return GetPropertyValue<string>("InvoiceDate"); }
            set { SetPropertyValue("InvoiceDate", value); }
        }

        public string SalesOrderNumber
        {
            get { return GetPropertyValue<string>("InvoiceNumber"); }
            set { SetPropertyValue("InvoiceNumber", value); }
        }
    }
}