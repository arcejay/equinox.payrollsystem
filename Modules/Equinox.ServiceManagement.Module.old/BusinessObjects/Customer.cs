using System.ComponentModel;
using System.Linq;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions, ModelDefault("Caption", "Customer")]
    [NavigationItem(NavigationItemName.Customers)]
    //[DefaultProperty(DisplayName)]
    public class Customer : PersonBase
    {
        public Customer(Session session)
            : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Status = PersonStatus.Active;}

        public new string FullName
        {
            get
            {
                if (CustomerType == CustomerType.Corporate)
                {
                    return Company.Name;
                }
                
                return base.FullName;
            }
        }

        [ImmediatePostData]
        public CustomerType CustomerType
        {
            get { return GetPropertyValue<CustomerType>("CustomerType"); }
            set { SetPropertyValue("CustomerType", value); }
        }

        [Association(AssociationMapping.CustomerCompany)]
        [Appearance("CompanyVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "CustomerType <>'Corporate'", Context = "DetailView")]
        public Company Company
        {
            get { return GetPropertyValue<Company>("Company"); }
            set { SetPropertyValue("Company", value); }
        }

        [DataSourceProperty("Company.Departments")]
        [Appearance("DepartmentVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "CustomerType <>'Corporate'", Context = "DetailView")]
        public Department Department
        {
            get { return GetPropertyValue<Department>("Department"); }
            set { SetPropertyValue("Department", value); }
        }

        [Association(AssociationMapping.CustomerMachines)]
        public XPCollection<Machine> Machines
        {
            get { return GetCollection<Machine>("Machines"); }
        }
        
        [Association(AssociationMapping.CustomerJobTickets)]
        public XPCollection<JobTicket> JobTickets   
        {
            get { return GetCollection<JobTicket>("JobTickets"); }
        }

        [Association(AssociationMapping.CustomerAddresses)]
        [Appearance("AddressVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "CustomerType <>'Walkin'", Context = "DetailView")]
        public CustomerAddress Address
        {
            get { return GetPropertyValue<CustomerAddress>("Address"); }
            set { SetPropertyValue("Address", value); }
        }

    }

    public class CustomerAddress : AddressBase
    {
        public CustomerAddress(Session session) : base(session)
        {}


        [Association(AssociationMapping.CustomerAddresses)]public XPCollection<Customer> Customers
        {
            get { return GetCollection<Customer>("Customers"); }
        }

    }

    public enum CustomerType
    {
        Walkin,
        Corporate
    }
}
