using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using AggregatedAttribute = DevExpress.ExpressApp.DC.AggregatedAttribute;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Company : XPObject
    {
        public Company(Session session)
            : base(session)
        {

        }

        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set
            {
                if (SetPropertyValue("Name", value) && string.IsNullOrEmpty(ShortName))
                {
                    if (value.Length <=4) return;

                    var result = value.Replace(",", "").Replace(".", "");
                    var array = result.Split(" ".ToCharArray()).Select(x => x.ToUpper()[0]).ToList();

                    ShortName = array.Count == 1 ? array[0].ToString() : string.Join("", array);
                }
            }}

        public string ShortName
        {
            get { return GetPropertyValue<string>("ShortName"); }
            set
            {
                SetPropertyValue("ShortName", value);
            }
        }
        
        public string WebAddress
        {
            get { return GetPropertyValue<string>("WebAddress"); }
            set { SetPropertyValue("WebAddress", value); }

        }

        [Association(AssociationMapping.CompanyBranches)]
        public Company ParentCompany
        {
            get { return GetPropertyValue<Company>("ParentCompany"); }
            set { SetPropertyValue("ParentCompany", value); }
        }


        [Association(AssociationMapping.CompanyBranches)]
        public XPCollection<Company> Branches
        {
            get { return GetCollection<Company>("Branches"); }
        }

        [Association(AssociationMapping.CompanyAddresses)]
        public XPCollection<CompanyAddress> Addresses
        {
            get
            {
                return GetCollection<CompanyAddress>("Addresses");
            }
        }

        [Association(AssociationMapping.CustomerCompany)]
        public XPCollection<Customer> Employees
        {
            get { return GetCollection<Customer>("Employees"); }
        }

        [Association(AssociationMapping.CompanyDepartments)]
        public XPCollection<Department> Departments
        {
            get { return GetCollection<Department>("Departments"); }
        } 
    }

    public class CompanyAddress : AddressBase
    {
        public CompanyAddress(Session session)
            : base(session)
        {
        }

        [Association(AssociationMapping.CompanyAddresses)]
        public Company Company
        {
            get { return GetPropertyValue<Company>("Company"); }
            set { SetPropertyValue("Company", value); }
        }

    }


    public class Department : XPObject
    {
        public Department(Session session)
            : base(session)
        {
        }


        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        [Association(AssociationMapping.CompanyDepartments)]
        public Company Company{
            get { return GetPropertyValue<Company>("Company"); }
            set { SetPropertyValue("Company", value); }
        }

        [Association(AssociationMapping.DepartmentDivisions)]
        public Department ParentDepartment
        {
            get { return GetPropertyValue<Department>("ParentDepartment"); }
            set { SetPropertyValue("ParentDepartment", value); }
        }

        [Association(AssociationMapping.DepartmentDivisions)]
        public XPCollection<Department> Divisions
        {
            get { return GetCollection<Department>("Divisions"); }
        } 

    }
}