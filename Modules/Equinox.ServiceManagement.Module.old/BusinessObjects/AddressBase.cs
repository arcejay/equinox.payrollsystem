using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultProperty("FullAddress"), ImageName("BO_Address")]
    public class AddressBase : XPObject
    {

        public AddressBase(Session session)
            : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            ZipCode = "3100";
            Province = "Nueva Ecija";
        }

        public string StreetName
        {
            get { return GetPropertyValue<string>("StreetName"); }
            set { SetPropertyValue("StreetName", value); }
        }

        public string Subdivision
        {
            get { return GetPropertyValue<string>("Subdivision"); }
            set { SetPropertyValue("Subdivision", value); }
        }

        public string Barangay
        {
            get { return GetPropertyValue<string>("Barangay"); }
            set
            {
                if (SetPropertyValue("Barangay", value))
                {

                }
            }
        }

        public string FullAddress
        {
            get { return GetFullAddress(); }
        }

        private string GetFullAddress()
        {
            var city = CityOrTown != null ? CityOrTown.Name : string.Empty;

            var contents = new[] { StreetName, Subdivision, Barangay, city, ZipCode, Province };

            var list = new List<string>();
            foreach (var item in contents)
            {
                if (!string.IsNullOrEmpty(item))
                    list.Add(item);
            }

            return string.Join(", ", list);
        }

        [Association]
        public CityOrTown CityOrTown { get; set; }

        public string ZipCode { get; set; }

        public string Province { get; set; }
    }
}