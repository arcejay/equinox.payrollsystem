using System;
using System.ComponentModel;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using EquinoxSystemManagement.Module.Extensions;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultProperty("Text")]
    public class JobTicketNote : Note
    {
        private JobTicket _jobTicket;
        private TimeSpan _updateTime;
        private Employee _author;
        //private DateTime _dateTime;
        private bool _setEmail = false;
        private bool _setSms = false;

        public JobTicketNote(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {base.AfterConstruction();

            DateTime = System.DateTime.Now.Date;
            UpdateTime = System.DateTime.Now.TimeOfDay;

            if (this.JobTicket != null)
            {
                this.JobTicket.LastUpdated = DateTime;
            }

            if (SecuritySystem.CurrentUser != null)
            {
                this.Author = Session.GetObjectByKey<SecuritySystemUser>(SecuritySystem.CurrentUserId).UserName;
            }
            
        }


        [Appearance("SendAsEmailToCustomerVisibility", Enabled = false, Context = "DetailView")]
        public bool SendAsEmailToCustomer
        {
            get { return GetPropertyValue<bool>("SendAsEmailToCustomer"); }
            set
            {
                if (SetPropertyValue("SendAsEmailToCustomer", value))
                {
                    _setEmail = value;
                }
            }
        }

        [Appearance("SendAsSmsToCustomerVisibility", Enabled = false, Context = "DetailView")]
        public bool SendAsSmsToCustomer
        {
            get { return GetPropertyValue<bool>("SendAsSmsToCustomer"); }
            set
            {
                if (SetPropertyValue("SendAsSmsToCustomer", value))
                {
                    _setSms = value;
                }
            }
        }

        [Association(AssociationMapping.JobTicketJobTicketNotes)]
        public JobTicket JobTicket
        {
            get { return _jobTicket; }
            set { SetPropertyValue("JobTicket", ref _jobTicket, value); }
        }

        [ModelDefault("DisplayFormat", "{0:t}")]
        [ModelDefault("EditMask", "t")]
        [ModelDefault("EditMaskType", "DateTime")]
        public System.TimeSpan UpdateTime
        {
            get { return _updateTime; }
            set { _updateTime = value; }
        }


        [Action(PredefinedCategory.RecordEdit, 
            AutoCommit = true, 
            Caption = "Send as SMS", 
            ConfirmationMessage = "Do you wish to send the selected note as SMS to customer?", 
            ToolTip = "Send the selected note as SMS",
            ImageName = "BO_Unknown")]
        public void SendSms()
        {
            SendAsSmsToCustomer = AsyncHelpers.RunSync(this.SendAsSms);

            OnChanged("SendAsSmsToCustomer");
        }


        [Action(PredefinedCategory.RecordEdit,
            AutoCommit = true,
            Caption = "Send as Email",
            ConfirmationMessage = "Do you wish to send the selected note as email to customer?",
            ToolTip = "Send the selected note as email",
            ImageName = "BO_Unknown")]
        public void SendEmail()
        {
            SendAsEmailToCustomer = AsyncHelpers.RunSync(this.SendAsEmail);
            OnChanged("SendAsEmailToCustomer");
        }

    }
}