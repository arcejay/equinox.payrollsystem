using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class CityOrTown : XPObject
    {
        public CityOrTown(Session session) : base(session)
        {
            
        }

        public string Name { get; set; }

        public string ZipCode { get; set; }

        [Association]
        public XPCollection<AddressBase> Addresses
        {
            get { return GetCollection<AddressBase>("Addresses"); }
        }

        [Association(AssociationMapping.ProvinceTowns)]
        public Province Province
        {
            get { return GetPropertyValue<Province>("Province"); }
            set { SetPropertyValue("Province", value); }
        }
    }
}