using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class MachinePartInfo : BaseObject
    {
        private string _partName;
        private string _serialNumber;
        private string _comment;
        private Machine _machine;

        public MachinePartInfo(Session session) : base(session) { }

        public string PartName
        {
            get { return _partName; }
            set { SetPropertyValue("PartName", ref _partName, value); }
        }

        public string SerialNumber
        {
            get { return _serialNumber; }
            set { SetPropertyValue("SerialNumber", ref _serialNumber, value); }
        }

        public string Description
        {
            get { return _comment; }
            set { SetPropertyValue("Description", ref _comment, value); }
        }

        [Association(AssociationMapping.MachineMachinePartInfos)]
        public Machine Machine
        {
            get { return _machine; }
            set { SetPropertyValue("Machine", ref _machine, value); }
        }
    }
}