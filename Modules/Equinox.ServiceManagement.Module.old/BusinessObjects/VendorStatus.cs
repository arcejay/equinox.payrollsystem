namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum VendorStatus
    {
        Active,
        Inactive
    }
}