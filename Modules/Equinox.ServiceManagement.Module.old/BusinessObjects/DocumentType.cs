namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum DocumentType
    {
        Unknown, Screenshot, OfficeDocument, Pdf
    }
}