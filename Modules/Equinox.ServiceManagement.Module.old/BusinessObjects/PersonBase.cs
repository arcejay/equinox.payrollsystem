using System.ComponentModel;
using System.Drawing;
using System.Linq;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultProperty("FullName")]
    [ModelDefault("Caption", "Person")]
    public class PersonBase : XPObject
    {
        public PersonBase(Session session) : base(session)
        {
            }

        public string FirstName
        {
            get { return GetPropertyValue<string>("FirstName"); }
            set
            {
                if (SetPropertyValue("FirstName", value))
                    OnChanged("FullName");
            }
        }

        public string LastName
        {
            get { return GetPropertyValue<string>("LastName"); }
            set
            {
                if (SetPropertyValue("LastName", value))
                    OnChanged("FullName");
            }
        }

        public string MiddleName
        {
            get { return GetPropertyValue<string>("MiddleName"); }
            set
            {
                if (SetPropertyValue("MiddleName", value))
                    OnChanged("FullName");
            }
        }

        public string FullName{
            get{
                if (!string.IsNullOrEmpty(MiddleName))
                {
                    var mi = MiddleName.Split().Select(x => x.Substring(0, 1));
                    
                    return string.Format("{0} {1}. {2}", FirstName, string.Join("", mi), LastName);

                }

                return string.Format("{0} {1}", FirstName, LastName);
            }
        }

        public PersonStatus Status
        {
            get { return GetPropertyValue<PersonStatus>("Status"); }
            set { SetPropertyValue("Status", value); }
        }

        public string Email
        {
            get { return GetPropertyValue<string>("Email"); }
            set { SetPropertyValue("Email", value); }
        }


        [Size(SizeAttribute.Unlimited), Delayed()]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PopupPictureEdit)]
        public byte[] Photo
        {
            get { return GetPropertyValue<byte[]>("Photo"); }
            set { SetPropertyValue("Photo", value); }
        }

        [Association(AssociationMapping.PersonPhones)]
        public XPCollection<PersonContactNumber> Phones
        {
            get { return GetCollection<PersonContactNumber>("Phones"); } 
            
        }
        
    }

    public class PersonContactNumber : Phone
    {
        public PersonContactNumber(Session session) : base(session)
        {
        }


        [Association(AssociationMapping.PersonPhones)]public PersonBase Person
        {
            get { return GetPropertyValue<PersonBase>("Person"); }
            set { SetPropertyValue("Person", value); }
        }
    }

   
}