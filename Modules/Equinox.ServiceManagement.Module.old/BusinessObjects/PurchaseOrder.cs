using System;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using EquinoxSystemManagement.Module.BusinessObjects.Tracking;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Vendors), ImageName("BO_Order")]
    public class PurchaseOrder : XPObject
    {
        private DateTime _orderDate;private Employee _orderBy;
        private PaymentMethod _method;
        private Vendor _vendor;
        private string _purchaseOrderNumber;
        private decimal _discount; 
        private OrderStatus _orderStatus; 
        private DateTime _shipDate;

        private string _paymentReferenceNo;
        private bool _isPaid;

        public PurchaseOrder()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public PurchaseOrder(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.

            PurchaseOrderNumber = "PO-" + this.GetUniqueKey();

            OrderDate = DateTime.Now;
            Method = PaymentMethod.CreditCard;
            OrderStatus = OrderStatus.NotStarted;
        }

        [Index(0)]
        public DateTime OrderDate
        {
            get { return _orderDate; }
            set { SetPropertyValue("OrderDate", ref _orderDate, value); }
        }

        [Index(1)]
        public string PurchaseOrderNumber
        {
            get { return _purchaseOrderNumber; }
            set { SetPropertyValue("PurchaseOrderNumber", ref _purchaseOrderNumber, value); }
        }

        public PaymentMethod Method
        {
            get { return _method; }
            set { SetPropertyValue("Terms", ref _method, value); }
        }

        public Employee OrderedBy
        {
            get { return _orderBy; }
            set { SetPropertyValue("OrderedBy", ref _orderBy, value); }
        }

        public decimal Discount
        {
            get { return _discount; }
            set { SetPropertyValue("Discount", ref _discount, value); }
        }

        public bool IsPaid
        {
            get { return _isPaid; }
            set { SetPropertyValue("IsPaid", ref _isPaid, value); }
        }

        public string PaymentReferenceNo
        {
            get { return _paymentReferenceNo; }
            set { SetPropertyValue("PaymentReferenceNo", ref _paymentReferenceNo, value); }
        }

        public OrderStatus OrderStatus
        {
            get { return _orderStatus; }
            set { SetPropertyValue("OrderStatus", ref _orderStatus, value); }
        }

        public DateTime ShipDate
        {
            get { return _shipDate; }
            set { SetPropertyValue("ShipDate", ref _shipDate, value); }
        }
        //[Association]
        //public Courier Courier
        //{
        //    get { return _courier; }
        //    set { SetPropertyValue("Courier", ref _courier, value); }
        //}


        private TrackingNumber _trackingNumber;

        public TrackingNumber TrackingNumber
        {
            get { return _trackingNumber; }
            set
            {
                if (_trackingNumber == value) return;

                TrackingNumber prev = _trackingNumber;

                _trackingNumber = value;

                if (IsLoading) return;

                if (prev != null && prev.PurchaseOrder == this)
                {
                    prev.PurchaseOrder = null;
                }

                if (_trackingNumber != null)
                {
                    _trackingNumber.PurchaseOrder = this;
                }
                OnChanged("TrackingNumber");
            }
        }
        
        [Association]
        public Vendor Vendor
        {
            get { return _vendor; }
            set { SetPropertyValue("Vendor", ref _vendor, value); }
        }

        [Association(AssociationMapping.PurchaseOrderPurchaseOrderItems)]
        //[RuleRequiredField(DefaultContexts.Save)]
        public XPCollection<PurchaseOrderItem> PurchaseOrderItems
        { get { return GetCollection<PurchaseOrderItem>("PurchaseOrderItems"); } }


        [Association(AssociationMapping.PurchaseOrderExpenseItems)]
        public XPCollection<ExpenseItem> Expenses
        {
            get { return GetCollection<ExpenseItem>("Expenses"); }
        }

        [Association(AssociationMapping.PurchaseOrderQuickNotes)]
        public XPCollection<QuickQuote> Quotations
        {
            get { return GetCollection<QuickQuote>("Quotations"); }
        }

        [Association(AssociationMapping.PurchaseOrderMemos)]
        public XPCollection<PurchaseOrderNote> Notes { get { return GetCollection<PurchaseOrderNote>("Notes"); } }

        [Action(Caption = "Import PO", ConfirmationMessage = "Do you wish to import all items from associated PO?")]
        public void ImportQuotationToPurchaseOrder()
        {

        }
    }

}