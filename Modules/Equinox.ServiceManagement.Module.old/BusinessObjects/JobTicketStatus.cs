namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public enum JobTicketStatus
    {
        InProgress,
        Billed,
        Released,
        PullOut
    }

    public enum RepairStatus
    {
        NotStarted,
        InProgress,
        Pending,
        Completed
    }
}