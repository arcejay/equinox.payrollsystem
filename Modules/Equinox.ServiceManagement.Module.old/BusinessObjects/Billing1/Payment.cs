﻿using System;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects.Billing
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Accounting)]
    public class Payment : VetiObject
    {
        public Payment(Session session) : base(session)
        {
            
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            PaymentNumber = string.Format("{0}-{1}", "PN", this.GetUniqueKey());
            PaymentDate = DateTime.Now;
            ReceivedBy = GetCurrentUser();

        }

        public string PaymentNumber
        {
            get { return GetPropertyValue<string>("PaymentNumber"); }
            set { SetPropertyValue("PaymentNumber", value); }
        }

        public DateTime PaymentDate
        {
            get { return GetPropertyValue<DateTime>("PaymentDate"); }
            set { SetPropertyValue("PaymentDate", value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string ORNumber
        {
            get { return GetPropertyValue<string>("ORNumber"); }
            set { SetPropertyValue("ORNumber", value); }
        }


        public decimal Amount
        {
            get { return GetPropertyValue<decimal>("Amount"); }
            set
            {
               SetPropertyValue("Amount", value);
            }
        }

        public decimal RunningBalance
        {
            get { return GetRunningBalance(); }
        }

        public string ReceivedBy
        {
            get { return GetPropertyValue<string>("ReceivedBy"); }
            set { SetPropertyValue("ReceivedBy", value); }
        }

        [Association(AssociationMapping.BillingStatementPayments)]
        public BillingStatement BillingStatement
        {
            get { return GetPropertyValue<BillingStatement>("BillingStatement"); }
            set
            {
                SetPropertyValue("BillingStatement", value);

                if (!IsLoading && !IsSaving && value != null)
                {
                    Amount = value.AmountDue;}
            }
        }

        private decimal GetRunningBalance()
        {
            if (BillingStatement != null && BillingStatement.PaymentHistory.Count > 0)
            {
                return this.BillingStatement.PaymentHistory.Where(x => x.PaymentDate <= this.PaymentDate).Sum(y=>y.Amount);
            }

            return 0;
        }
    }
}