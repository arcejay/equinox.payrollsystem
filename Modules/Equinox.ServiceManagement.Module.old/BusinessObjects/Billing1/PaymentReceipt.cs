using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects.Billing
{
    public class PaymentReceipt : XPObject
    {
    }

    public enum RequestedProofOfPayment{
        AR,OR
    }

    public enum PaymentStatus
    {
        Created, Noted, Approved
    }

    public enum BillingFor{
        Sales,
        Service,
        SalesAndService
    }

    public enum PaymentType
    {
        Full,Partial
    }
}
