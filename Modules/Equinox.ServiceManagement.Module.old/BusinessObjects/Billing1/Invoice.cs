using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects.Billing
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Accounting)]
    public class Invoice : VetiObject
    {
        public Invoice(Session session)
            : base(session)
        {

        }

        public string InvoiceDate
        {
            get { return GetPropertyValue<string>("InvoiceDate"); }
            set { SetPropertyValue("InvoiceDate", value); }
        }

        public string InvoiceNumber
        {
            get { return GetPropertyValue<string>("InvoiceNumber"); }
            set { SetPropertyValue("InvoiceNumber", value); }
        }
    }
}