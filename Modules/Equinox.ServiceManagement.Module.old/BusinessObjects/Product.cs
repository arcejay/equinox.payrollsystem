using System;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Inventories)]
    public class Product : XPObject
    {
        private ProductCategory _category;
        private string _name;
        private string _description;
        private string _manufacturerPartNumber;
        private InventoryItem _inventoryItem;

        public Product()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Product(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        [RuleRequiredField(DefaultContexts.Save)]

        [RuleUniqueValue]
        public string ManufacturerPartNumber    
        {
            get { return _manufacturerPartNumber; }
            set { SetPropertyValue("ManufacturerPartNumber", ref _manufacturerPartNumber, value); }
        }

        public string OemPartNumber
        {
            get { return GetPropertyValue<string>("OemPartNumber"); }
            set { SetPropertyValue("OemPartNumber", value); }
        }
        

        [RuleUniqueValue]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public string Manufacturer { get; set; }

        [Association]
        public ProductCategory Category
        {
            get { return _category; }
            set { SetPropertyValue("ProductCategory", ref _category, value); }
        }

        [Association]
        public XPCollection<ProductSpecification> Specifications
        {
            get { return GetCollection<ProductSpecification>("Specifications"); }
        }

        public string Compatibility
        {
            get { return GetPropertyValue<string>("Compatibility"); }
            set { SetPropertyValue("Compatibility", value); }
        }

        public InventoryItem InventoryItem 
        {
            get { return _inventoryItem; }
            set
            {
                if (_inventoryItem == value) return;
                
                //store reference to old data
                var prevInventoryItem = _inventoryItem;

                _inventoryItem = value;

                if (IsLoading) return;

                if (prevInventoryItem != null && prevInventoryItem.Product == this)
                    prevInventoryItem.Product = null;

                //set the new value
                if (_inventoryItem != null)
                    _inventoryItem.Product = this;

                OnChanged("Inventory");
            }
        }

        [Action(PredefinedCategory.Edit,
            ImageName = "BO_Product",
            Caption = "Add more product", 
            ConfirmationMessage = "Do you really wish to add new product?")]
        public void SetInventoryItem()
        {
            }
    }

}