using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions, ModelDefault("Caption", "Employee")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    public class Employee : PersonBase
    {
// Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public Employee(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        [Association(AssociationMapping.EngineerJobTickets)]
        public XPCollection<JobTicket> JobTickets
        {
            get { return GetCollection<JobTicket>("JobTickets"); }
        }

        [Association]
        public Position Position { get; set; }


        [Association(AssociationMapping.EmployeeAddresses)]
        public XPCollection<EmployeeAddress> Addresses { get { return GetCollection<EmployeeAddress>("Addresses"); } } 
    }

    public class EmployeeAddress : AddressBase
    {
        public EmployeeAddress(Session session) : base(session)
        {
        }[Association(AssociationMapping.EmployeeAddresses)]
        public Employee Employee
        {
            get { return GetPropertyValue<Employee>("Employee"); }
            set { SetPropertyValue("Employee", value); }
        }
    }
}
