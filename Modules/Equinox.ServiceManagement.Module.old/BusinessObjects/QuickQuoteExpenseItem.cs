using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class QuickQuoteExpenseItem : ExpenseItem
    {
        private QuickQuote _quickQuote;

        public QuickQuoteExpenseItem(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            Quantity = 1;

            UnitOfMeasure = Session.FindObject<UnitOfMeasure>(CriteriaOperator.Parse("Name = 'pcs'"));}

        [Association(AssociationMapping.QuickQuoteExpenses)]
        public QuickQuote QuickQuote
        {
            get { return _quickQuote; }
            set { SetPropertyValue("QuickQuote", ref _quickQuote, value); }
        }
    }
}