using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Province : XPObject
    {
        public Province(Session session) : base(session)
        {
            
        }

        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public string ShortName
        {
            get { return GetPropertyValue<string>("ShortName"); }
            set { SetPropertyValue("ShortName", value); }
        }

        [Association(AssociationMapping.ProvinceTowns)]
        public XPCollection<CityOrTown> CityOrTowns
        {
            get { return GetCollection<CityOrTown>("CityOrTowns"); }
        }
    }
}