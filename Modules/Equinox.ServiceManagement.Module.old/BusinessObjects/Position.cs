using DevExpress.Xpo;

namespace EquinoxSystemManagement.Module.BusinessObjects
{
    public class Position : XPObject
    {
        public Position(Session session) : base(session)
        {
            
        }

        public string PositionName
        {
            get { return GetPropertyValue<string>("PositionName"); }
            set { SetPropertyValue("PositionName", value); }
        }

        public string Department
        {
            get { return GetPropertyValue<string>("Department"); }
            set { SetPropertyValue("Department", value); }
        }
        [Association, Aggregated]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>("Employees"); }
        }
    }
}