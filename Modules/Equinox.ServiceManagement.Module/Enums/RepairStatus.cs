namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    public enum RepairStatus
    {
        NotStarted,
        InProgress,
        Pending,
        Completed
    }
}