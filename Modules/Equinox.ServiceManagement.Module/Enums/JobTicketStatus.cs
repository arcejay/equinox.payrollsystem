namespace Equinox.ServiceManagement.Module.Enums
{
    public enum JobTicketStatus
    {
        NotStarted,
        InProgress,
        Billed,
        Released,
        PullOut
    }
}