namespace Equinox.ServiceManagement.Module.Enums
{
    public enum OrderStatus
    {
        NotStarted,
        InProgress,
        Ordered,
        Closed,
        Shipped,
        Completed}

}