namespace Equinox.ServiceManagement.Module.Enums
{
    public enum QuickQuoteStatus
    {
        NotStarted, InProgress,Cancelled, Completed
    }
}