namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    public enum PaymentMethod
    {
        CashOnDelivery,
        Paypal,
        CreditCard,
        PostDatedCheck,
        BankDeposit
    }

}