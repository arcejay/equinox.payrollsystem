using System;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    public class ExpenseItem : XPObject
    {
        private float _quantity;
        private UnitOfMeasure _unitOfMeasure;
        private decimal _amount;
        private string _code;
        private string _description;

        public ExpenseItem(Session session)
            : base(session)
        {
        }

        [Index(0)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Index(1)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Index(2)]
        public float Quantity
        {
            get { return _quantity; }
            set
            {
                if (SetPropertyValue("Quantity", ref _quantity, value))
                    OnChanged("TotalAmount");
            }
        }

        [Index(3)]
        public UnitOfMeasure UnitOfMeasure
        {
            get { return _unitOfMeasure; }
            set { SetPropertyValue("UnitOfMeasure", ref _unitOfMeasure, value); }
        }

        [Index(4)]
        public decimal Amount
        {
            get { return _amount; }
            set
            {
                if (SetPropertyValue("Amount", ref _amount, value))
                    OnChanged("TotalAmount");
            }
        }

        [Index(5)]
        [PersistentAlias("Quantity * Amount")]
        public decimal TotalAmount
        {
            get
            {
                var objTemp = EvaluateAlias("TotalAmount");
                return objTemp != null ? Convert.ToDecimal(objTemp) : 0;
            }
        }


        [Association(AssociationMapping.PurchaseOrderExpenseItems)]
        public PurchaseOrder PurchaseOrder
        {
            get { return GetPropertyValue<PurchaseOrder>("PurchaseOrder"); }
            set { SetPropertyValue("PurchaseOrder", value); }
        }
    }
}
