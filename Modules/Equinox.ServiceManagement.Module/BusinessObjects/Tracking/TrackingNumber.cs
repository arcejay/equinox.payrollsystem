using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using EquinoxSystemManagement.Module.BusinessObjects.Tracking;

namespace Equinox.ServiceManagement.Module.BusinessObjects.Tracking
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Vendors)]
    [DefaultProperty("TrackingReference")]
    public class TrackingNumber : XPObject
    {
        public TrackingNumber(Session session)
            : base(session)
        {

        }

        public string TrackingReference
        {
            get { return GetPropertyValue<string>("TrackingReference"); }
            set { SetPropertyValue("TrackingReference", value); }
        }

        [Association]
        public CourierBase Courier
        {
            get { return GetPropertyValue<CourierBase>("Courier"); }
            set { SetPropertyValue("Courier", value); }
        }

        
        [Association, NonPersistent]
        public XPCollection<CheckpointItem> Checkpoints
        {
            get
            {
                return GetCollection<CheckpointItem>("Checkpoints");
            }
        }

        private PurchaseOrder _purchaseOrder;

        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set
            {
                if (_purchaseOrder == value) return;

                //save previous value
                PurchaseOrder prev = _purchaseOrder;

                //set new value
                _purchaseOrder = value;

                if (IsLoading) return;

                //remove reference to this
                if (prev != null && prev.TrackingNumber == this)
                {
                    prev.TrackingNumber = null;
                }

                //set reference to new value
                if (_purchaseOrder != null)
                {
                    _purchaseOrder.TrackingNumber = this;
                }
            }
        }
        
        

        [Action(PredefinedCategory.Edit, Caption = "Track", ImageName = "BO_Unknown")]
        public async void TrackParcel()
        {
            var result = await Courier.Track(TrackingReference);

            Session.Delete(Checkpoints);
            Session.Save(Checkpoints);

            foreach (var checkpointItem in result)
            {
                Checkpoints.Add(checkpointItem);}
        }

    }
}