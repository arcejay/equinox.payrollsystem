using System;
using DevExpress.Xpo;
using EquinoxSystemManagement.Module.BusinessObjects.Tracking;

namespace Equinox.ServiceManagement.Module.BusinessObjects.Tracking
{
    public class CheckpointItem : XPObject{
        public CheckpointItem(Session session)
            : base(session)
        {

        }

        public DateTime CheckpointDate
        {
            get { return GetPropertyValue<DateTime>("CheckpointDate"); }
            set { SetPropertyValue("CheckpointDate", value); }
        }

        public TimeSpan CheckpointTime
        {
            get { return GetPropertyValue<TimeSpan>("CheckpointTime"); }
            set { SetPropertyValue("CheckpointTime", value); }
        }

        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public string Location
        {
            get { return GetPropertyValue<string>("Location"); }
            set { SetPropertyValue("Location", value); }
        }

        [Association]
        public TrackingNumber TrackingNumber
        {
            get { return GetPropertyValue<TrackingNumber>("TrackingNumber"); }
            set { SetPropertyValue("TrackingNumber", value); }
        }
        
    }
}