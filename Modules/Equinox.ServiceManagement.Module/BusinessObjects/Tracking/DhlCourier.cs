using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DevExpress.Xpo;
using EquinoxSystemManagement.Module.BusinessObjects.Tracking;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace Equinox.ServiceManagement.Module.BusinessObjects.Tracking
{
    public class DhlCourier : CourierBase
    {
        private readonly string _url = "http://www.dhl.com.ph/shipmentTracking?AWB={0}&countryCode=ph&languageCode=en";

        public DhlCourier(Session session)
            : base(session)
        {

        }

        public override async Task<XPCollection<CheckpointItem>> Track(string trackingNumber)
        {
            var url = string.Format(_url, trackingNumber);
            var userAgent = @"37.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";
            var referer = string.Format("http://www.dhl.com.ph/en/express/tracking.html?AWB={0}&brand=DHL",
                trackingNumber);
            var cookie =
                string.Format(
                    @"AWBS_crossrefpar1_taskcenter_taskcentertabs_item1229046233349_par_expandablelink_insideparsys_fasttrack={0}; BRAND_crossrefpar1_taskcenter_taskcentertabs_item1229046233349_par_expandablelink_insideparsys_fasttrack=DHL%20Air%20waybill; TS016f3c0b=01914b743d67ceaebcc4872568d5e613e605a95ad150f74380071be11f5add0b45e94a1a4a; WT_FPC=id=920ccef.000-6978-44f9-9a7a-ce781ec0da62:lv=1427824880514:ss=1427824414986",
                    trackingNumber);

            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.Host] = @"www.dhl.com.ph";
                client.Headers[HttpRequestHeader.Accept] = "application/json, text/javascript, */*; q=0.01";
                client.Headers["X-Requested-With"] = "XMLHttpRequest";
                client.Headers[HttpRequestHeader.UserAgent] = userAgent;
                client.Headers[HttpRequestHeader.Referer] = referer;
                client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate, sdch";
                client.Headers[HttpRequestHeader.AcceptLanguage] = "en-US,en;q=0.8";
                client.Headers[HttpRequestHeader.Cookie] = cookie;

                var result = await client.DownloadStringTaskAsync(new Uri(url));

                var data = JsonConvert.DeserializeObject<dynamic>(result);

                dynamic c = data.results[0].checkpoints;

                var list = new XPCollection<CheckpointItem>(Session);

                foreach (dynamic t in c)
                {

                    var checkpoint = new CheckpointItem(Session);
                    //checkpoint.co = t.counter;
                    checkpoint.CheckpointDate = t.date;
                    checkpoint.CheckpointTime = t.time;
                    checkpoint.Location = t.location;
                    checkpoint.Description = t.description;

                    list.Add(checkpoint);
                }
                return list;
            }
        }
    }

    public class EmsCourier : CourierBase
    {
        //private readonly string _url = "http://tnt.phlpost.gov.ph/index.asp?i={0}";
        public EmsCourier(Session session)
            : base(session)
        {

        }

        public override async Task<XPCollection<CheckpointItem>> Track(string trackingNumber)
        {
            string emsurl = @"http://tnt.phlpost.gov.ph/index.asp?i={0}";

            var url = string.Format(emsurl, trackingNumber);

            var content = string.Empty;

            using (var client = new WebClient())
            {
                content = await client.DownloadStringTaskAsync(new Uri(url));

            }

            var doc = new HtmlDocument();

            doc.LoadHtml(content);

            var list = new XPCollection<CheckpointItem>(Session);

            foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table"))
            {
                var rows = table.SelectNodes("tr").ToList();
                rows.RemoveAt(0);

                foreach (HtmlNode row in rows)
                {var cells = row.SelectNodes("th|td").ToList();

                    var cp = new CheckpointItem(Session);
                    var cell1 = cells[0].InnerText;
                    cp.CheckpointDate = DateTime.Parse(cell1).Date;
                    cp.CheckpointTime = DateTime.Parse(cell1).TimeOfDay;
                    cp.Location = cells[3].InnerText;
                    cp.Description = cells[1].InnerText;

                    list.Add(cp);
                }
            }

            return list;}
    }}