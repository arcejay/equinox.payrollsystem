using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraPrinting.Native;
using Equinox.Crm.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.ServiceManagement.Module.Enums;
using Equinox.ServiceManagement.Module.StateMachines;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Services)]
    [DefaultProperty("JobTicketNumber")]
    public class JobTicket : BaseObject, IStateMachineProvider{
      //  private DateTime _jobTicketDate;
        private Machine _machine;private string _jobTicketNumber;
        private JobTicketStatus _status;
        private Employee _engineer;
        private DateTime _dateCompleted;
        private RepairLevel _recommendedRepairLevel;
        private JobTicketType _jobTicketType;
        private DateTime _lastUpdated = default(DateTime);
        //private TimeSpan _jobTicketTime;
        private Customer _customer;
        //private string _createdBy;

        public JobTicket(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            JobTicketNumber = "JT-" + this.GetUniqueKey(); 
            JobTicketDate = DateTime.Now;
            JobTicketTime = DateTime.Now.TimeOfDay;
            var note = new JobTicketNote(Session) { Text = JobTicketNumber + " added." };
            JobTicketNotes.Add(note);

            CreatedBy = GetCurrentUser();

        }

        private string GetCurrentUser()
        {
            if ( !IsLoading && SecuritySystem.CurrentUser != null)
            {
                return Session.GetObjectByKey<SecuritySystemUser>(SecuritySystem.CurrentUserId).UserName;
            }

            return null;
        }

        [Appearance("JobTicketDateVisibility", Enabled = false, Context = "DetailView")]
        public DateTime JobTicketDate
        {
            get { return GetPropertyValue<DateTime>("JobTicketDate"); }
            set { SetPropertyValue("JobTicketDate", value); }
        }


        [ModelDefault("DisplayFormat", "{0:t}")]
        [ModelDefault("EditMask", "t")]
        [ModelDefault("EditMaskType", "DateTime")]
        [Appearance("JobTicketTimeVisibility", Enabled = false, Context = "DetailView")]
        public TimeSpan JobTicketTime
        {
            get { return GetPropertyValue<TimeSpan>("JobTicketTime"); }
            set { SetPropertyValue("JobTicketTime", value); }

        }


        [Appearance("JobTicketNumberVisibility", Enabled = false, Context = "DetailView")]
        public string JobTicketNumber
        {
            get { return _jobTicketNumber; }
            set { SetPropertyValue("JobTicketNumber", ref _jobTicketNumber, value); }
        }

        public JobTicketStatus Status
        {
            get { return _status; }
            set
            {
                if (SetPropertyValue("Status", ref _status, value))
                {
                    LogChangeStatus();
                }
            }
        }

        public RepairStatus RepairStatus
        {
            get { return GetPropertyValue<RepairStatus>("RepairStatus"); }
            set { SetPropertyValue("RepairStatus", value); }
        }

        [ProvidedAssociation(AssociationMapping.EngineerJobTickets, RelationType.OneToMany)]
        public Employee AssignedTo
        {
            get { return _engineer; }
            set { SetPropertyValue("AssignedTo", ref _engineer, value); }
        }


        [DataSourceProperty("Customer.Machines")]
        [Association(AssociationMapping.JobTicketMachines)]
        public Machine Machine
        {
            get { return _machine; }
            set { SetPropertyValue("Machine", ref _machine, value); }
        }


        [ProvidedAssociation(AssociationMapping.CustomerJobTickets, RelationType.OneToMany)]
        public Customer Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        public string CreatedBy
        {
            get { return GetPropertyValue<string>("CreatedBy"); }
            set { SetPropertyValue("CreatedBy", value); }
        }

        public RepairLevel RecommendedRepairLevel
        {
            get { return _recommendedRepairLevel; }
            set { SetPropertyValue("RecommendedRepairLevel", ref _recommendedRepairLevel, value); }
        }

        public DateTime DateCompleted
        {
            get { return _dateCompleted; }
            set { SetPropertyValue("DateCompleted", ref _dateCompleted, value); }
        }

        public JobTicketType JobTicketType
        {
            get { return _jobTicketType; }
            set { SetPropertyValue("JobTicketType", ref _jobTicketType, value); }
        }

        [PersistentAlias("LastUpdate")]
        [Appearance("LastUpdatedVisibility", Enabled = false, Context = "DetailView")]
        public DateTime LastUpdated
        {
            get
            {
                if (!IsLoading && !IsSaving && (_lastUpdated == default(DateTime)))
                    SetLastUpdate(false);

                return _lastUpdated;

            }

            set { SetPropertyValue("LastUpdated", ref _lastUpdated, value); }
        }

        public string Memo
        {
            get
            {
                if (JobTicketNotes.Count > 0)
                {
                    var notes = JobTicketNotes.OrderByDescending(o => o.DateTime)
                        .GroupBy(g => g.DateTime).FirstOrDefault();

                    if (notes == null) return null;

                    var sb = new StringBuilder();

                    notes.ForEach(n => sb.AppendFormat("[{0}] {1}", n.Author, n.Text));

                    return sb.ToString();
                }

                return string.Empty;}
        }

        private void SetLastUpdate(bool forceChangeEvents)
        {
            var oldUpdate = _lastUpdated;

            var tempValue = default(DateTime);

            foreach (var note in JobTicketNotes)
            {
                if (note.DateTime > tempValue)
                    tempValue = note.DateTime;
            }

            _lastUpdated = tempValue;

            if (forceChangeEvents)
                OnChanged("LastUpdated", oldUpdate, _lastUpdated);
        }

        
        [Association(AssociationMapping.JobTicketJobTicketNotes)]
        public XPCollection<JobTicketNote> JobTicketNotes => GetCollection<JobTicketNote>("JobTicketNotes");


        [Association(AssociationMapping.JobTicketFileAttachments)]
        public XPCollection<JobTicketAttachment> FileAttachments => GetCollection<JobTicketAttachment>("FileAttachments");

        private void LogChangeStatus()
        {
            var note = new JobTicketNote(Session)
            {
                Text = string.Format("Status changed to {0} ", Status),
                Author = GetCurrentUser() 
            };

            JobTicketNotes.Add(note);
        }

        public IList<IStateMachine> GetStateMachines()
        {
            var result = new List<IStateMachine>
            {
                new JobTicketStateMachine(XPObjectSpace.FindObjectSpaceByObject(this)),
                new RepairStatusStateMachine(XPObjectSpace.FindObjectSpaceByObject(this))
            };


            return result;
        }

        #region "Actions"

        
        #endregion
    }
}
