using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Services)]
    public class Service : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        public decimal Rate
        {
            get { return GetPropertyValue<decimal>("Rate"); }
            set { SetPropertyValue("Rate", value); }
        }

        [Association]
        public ServiceCategory ServiceCategory
        {
            get { return GetPropertyValue<ServiceCategory>("ServiceCategory"); }
            set { SetPropertyValue("ServiceCategory", value); }
        }

        public Service(Session session) : base(session)
        {

        }
    }

    public class ServiceCategory : XPObject
    {
        public string Name
        {
            get { return GetPropertyValue<string>("Name"); }
            set { SetPropertyValue("Name", value); }
        }

        [Association]
        public XPCollection<Service> Services => GetCollection<Service>("Services");

        public ServiceCategory(Session session) : base(session)
        {

        }
    }
}