namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    public enum DocumentType
    {
        Unknown, Screenshot, OfficeDocument, Pdf
    }
}