using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Crm.Module.BusinessObjects;

namespace Equinox.ServiceManagement.Module.BusinessObjects.Sales
{
    [DefaultClassOptions]
    [ImageName("BO_Contact"), NavigationItem(NavigationItemName.Sales)]
    [ModelDefault("Caption", "Sales Lead")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    public class SalesLead : Customer
    { 
        // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public SalesLead(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        public string ReferredBy
        {
            get { return GetPropertyValue<string>("ReferredBy"); }
            set { SetPropertyValue("ReferredBy", value); }
        }

        public string ProjectDescription
        {
            get { return GetPropertyValue<string>("ProjectDescription"); }
            set { SetPropertyValue("ProjectDescription", value); }
        }

        [Action(Caption = "Create Customer", ConfirmationMessage = "Are you sure?", ImageName = "Question", AutoCommit = true)]
        public void CreateCustomer()
        {

            //this.PersistentProperty = "Paid";
        }
    }

    public enum LeadStatus
    {
        Hot,
        InProgress,
        Completed
    }
}
