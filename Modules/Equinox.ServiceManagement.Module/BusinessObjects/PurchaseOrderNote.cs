using System;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    public class PurchaseOrderNote : Note
    {
        private PurchaseOrder _purchaseOrder;
        public PurchaseOrderNote(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DateTime = DateTime.Now;
        }

        [Association(AssociationMapping.PurchaseOrderMemos)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }
    }
}