using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects.Billing
{
    [DefaultClassOptions]
    public class Bill : XPObject
    {
        public Bill(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}
