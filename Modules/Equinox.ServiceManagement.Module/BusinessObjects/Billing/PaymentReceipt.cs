using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects.Billing
{
    public class PaymentReceipt : XPObject
    {
        public PaymentReceipt(Session session) : base(session)
        {

        }
    }

    public enum RequestedProofOfPayment{
        AR,OR
    }

    public enum PaymentStatus
    {
        Created, Noted, Approved
    }

    public enum BillingFor{
        Sales,
        Service,
        SalesAndService
    }

    public enum PaymentType
    {
        Full,Partial
    }
}
