using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    public class QuickNoteMemo : Note
    {
        private QuickQuote _quickQuote;

        public QuickNoteMemo(Session session)
            : base(session)
        {
        }
        
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            if (SecuritySystem.CurrentUser != null)
            {
                this.Author = Session.GetObjectByKey<SecuritySystemUser>(SecuritySystem.CurrentUserId).UserName;
            }
            
            DateTime = DateTime.Now;
        }


        [Association(AssociationMapping.QuickNoteMemos)]
        public QuickQuote QuickQuote
        {
            get { return _quickQuote; }set { SetPropertyValue("QuickQuote", ref _quickQuote, value); }
        }

    }
}