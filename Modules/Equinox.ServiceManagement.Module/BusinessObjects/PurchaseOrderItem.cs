using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [ImageName("BO_Order_It")]
    public class PurchaseOrderItem : OrderItem
    {
        private PurchaseOrder _purchaseOrder;

        public PurchaseOrderItem(Session session) : base(session)
        {
            
        }

        [Association(AssociationMapping.PurchaseOrderPurchaseOrderItems)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }
    }
}