using System;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions, NavigationItem(NavigationItemName.Inventories), DefaultProperty("Product")]
    public class InventoryItem : XPObject
    {
        private double _quantity;
        private decimal _averageCost;
        private double _orderThreshold;
        private Product _product;


        public InventoryItem(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }
        
        [Index(1)]
        public double Quantity
        {
            get { return _quantity; }
            set { SetPropertyValue("Quantity", ref _quantity, value); }
        }

        [Index(2)]
        public decimal AverageCost
        {
            get { return _averageCost; }
            set { SetPropertyValue("AverageCost", ref _averageCost, value); }
        }

        [Index(3)]
        public double OrderThreshold
        {
            get { return _orderThreshold; }
            set { SetPropertyValue("OrderThreshold", ref _orderThreshold, value); }
        }

        public string PartNumber
        {
            get
            {
                if (Product != null && Product.ManufacturerPartNumber != null)
                    return Product.ManufacturerPartNumber;

                return null;
            }
        }

        public Product Product
        {
            get { return _product; }
            set
            {
                //SetPropertyValue("Product", ref _product, value);

                if (_product == value) return;

                var prevProduct = _product;
                _product = value;

                if (IsLoading) return;

                if (prevProduct != null && prevProduct.InventoryItem == this)
                    prevProduct.InventoryItem = null;

                //set to new
                if (_product != null)
                    _product.InventoryItem = this;

                OnChanged("Product");}
        }

        [PersistentAlias("AverageCost*Quantity")]
        public decimal Total
        {
            get
            {
                var tempObj = EvaluateAlias("Total");

                return (tempObj != null) ? Convert.ToDecimal(tempObj) : 0;
            }
        }

        [Association]
        public XPCollection<OrderItem> OrderItems { get { return GetCollection<OrderItem>("OrderItems"); } }

        //public override string ToString()
        //{
        //    return Product.Name;}
    }
}
