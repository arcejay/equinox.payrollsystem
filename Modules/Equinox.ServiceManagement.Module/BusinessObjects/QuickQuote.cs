using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Equinox.Crm.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.ServiceManagement.Module.Enums;
using Equinox.ServiceManagement.Module.StateMachines;
using Xpand.ExpressApp.Attributes;
using Xpand.Persistent.Base.General;

//using DevExpress.ExpressApp.DC;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Sales)]
    [DefaultProperty("QuickQuoteNo"), ImageName("BO_Quote")]public class QuickQuote : XPObject, IStateMachineProvider
    {
        private Vendor _vendor;
        private DateTime _quoteDate;
        private string _joNumber;
        private Customer _customer;
        private QuickQuoteStatus _status;
        private int _duration;
        private PurchaseOrder _purchaseOrder;
        private string _quickQuoteNo;

        public QuickQuote(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            QuickQuoteNo = "QQ-" +this.GetUniqueKey();
            QuoteDate = DateTime.Now;

            LeadTime = 15;

            //create an initial instance Note object
            var note = new QuickNoteMemo(Session);
            note.DateTime = DateTime.Now;
            note.Text = string.Format("Quick quote {0} added", QuickQuoteNo);

            this.Notes.Add(note);
        }

        public DateTime QuoteDate{
            get { return _quoteDate; }
            set
            {
                SetPropertyValue("QuoteDate", ref _quoteDate, value);
            }
        }

        public string QuickQuoteNo
        {
            get { return _quickQuoteNo; }
            set { SetPropertyValue("QuickQuoteNo", ref _quickQuoteNo, value); }
        }

        [Index(1)]
        public Customer Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [VisibleInListView(false)]
        public string JoNumber
        {
            get { return _joNumber; }
            set { SetPropertyValue("JoNumber", ref _joNumber, value); }
        }


        [VisibleInListView(false)]public Employee AssignedTo
        {
            get { return GetPropertyValue<Employee>("AssignedTo"); }
            set { SetPropertyValue("AssignedTo", value); }
        }


        [VisibleInListView(false)]
        [ProvidedAssociation(Equinox.Infrastructure.Common.AssociationMapping.VendorQuickQuotes, RelationType.OneToMany)]
        public Vendor Vendor
        {
            get { return _vendor; }
            set { SetPropertyValue("Vendor", ref _vendor, value); }
        }
        
        public QuickQuoteStatus Status
        {
            get { return _status; }
            set
            {
                SetPropertyValue("Status", ref _status, value);

                if (value == QuickQuoteStatus.Completed)
                {
                    //set the owner
                }

                if (!IsSaving) return;

                //log status change
                var note = new QuickNoteMemo(Session)
                {
                    DateTime = DateTime.Now,
                    Text = "Status changed to " + Status
                };

                Notes.Add(note);

            }
        }


        [VisibleInListView(false)]
        public int LeadTime
        {
            get { return _duration; }
            set { SetPropertyValue("LeadTime", ref _duration, value); }
        }

        public string LastUpdate
        {
            get{
                if (!IsLoading && !IsSaving && Notes != null )
                {
                    QuickNoteMemo temp = null;

                    var results = Notes.GroupBy(x => x.DateTime.Date).OrderByDescending(y => y.Key).First();

                    temp = results.Select(x => x).OrderByDescending(y => y.DateTime.TimeOfDay).First();

                    return string.Format("[{0} - {2}]: {1}", temp.DateTime, temp.Text, temp.Author);
                }

                return string.Empty;}
        }


        [Appearance("TotalAmountVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "TotalAmount = 0", Context = "DetailView")]
        public decimal TotalAmount
        {
            get
            {
                decimal total = 0;

                foreach (var item in QuickQuoteItems)
                {
                    total += item.Price*(decimal)item.Quantity;
                }

                return total;
            }
            
        }

        [Association(AssociationMapping.QuickQuoteQuickQuoteItems)]
        public XPCollection<QuickQuoteItem> QuickQuoteItems
        {
            get
            {
                return GetCollection<QuickQuoteItem>("QuickQuoteItems");
            }
        }

        [Association(AssociationMapping.QuickNoteMemos)]
        public XPCollection<QuickNoteMemo> Notes { get { return GetCollection<QuickNoteMemo>("Notes"); } }


        [Aggregated, Association]
        public XPCollection<FileAttachmentData> Attachments
        {
            get { return GetCollection<FileAttachmentData>("Attachments"); }
        }

        [Association(AssociationMapping.QuickQuoteExpenses)]
        public XPCollection<QuickQuoteExpenseItem> Expenses { get { return GetCollection<QuickQuoteExpenseItem>("Expenses"); } }
        [Action(Caption = "Add VAT", ConfirmationMessage = "Are you sure?", ImageName = "Question", AutoCommit = true)]
        public void CalculateAndAddVat()
        {
            // Trigger a custom business logic for the current record in the UI (http://documentation.devexpress.com/#Xaf/CustomDocument2619).
            //this.PersistentProperty = "Paid";
        }

        [Action(Caption = "Add Expense", ConfirmationMessage = "Add expense to this quote?", ImageName = "Question", AutoCommit = true)]
        public void AddExpense()
        {
            }


        [VisibleInListView(false)][Association(AssociationMapping.PurchaseOrderQuickNotes)]
        [Appearance("PurchaseOrderVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "Status <>'Completed'", Context = "DetailView")]
        public PurchaseOrder PurchaseOrder
        {get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }
        
        public IList<IStateMachine> GetStateMachines()
        {
            var result = new List<IStateMachine>();
            result.Add(new QuickQuoteStatusStateMachine(XPObjectSpace.FindObjectSpaceByObject(this)));
            return result;
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();QuickQuoteItems.CollectionChanged += (o, e) => OnChanged("TotalAmount");
        }

    }
}

