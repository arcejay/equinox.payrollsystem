namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    public enum JobTicketType
    {
        Warranty,
        Walkin,
        Onsite
    }
}