using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [FileTypeFilter("DocumentFiles", 1, "*.txt", "*.doc","*.pdf")]
    [FileTypeFilter("AllFiles", 2, "*.*")]
    public class FileAttachmentData : FileAttachmentBase
    {
        private QuickQuote _quickQuote;
        private DocumentType _documentType;
// Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public FileAttachmentData(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction(); 
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        [Persistent, Association]
        public QuickQuote QuickQuote
        {
            get { return _quickQuote; }
            set { SetPropertyValue("QuickQuote", ref _quickQuote, value); }
        }public DocumentType DocumentType{
            get { return _documentType; }
            set { SetPropertyValue("DocumentType", ref _documentType, value); }
        }

    }


}
