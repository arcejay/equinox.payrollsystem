using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Equinox.ServiceManagement.Module.StateMachines;
using EquinoxSystemManagement.Module;
using EquinoxSystemManagement.Module.BusinessObjects.Billing;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Accounting)]
    [ImageName("BO_Payment")]
    [DefaultProperty("JobOrderNumber")]
    public class BillingStatement : VetiObject, IStateMachineProvider
    { 
        
        public BillingStatement(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            PaymentDate = DateTime.Now;
            PaymentType = PaymentType.Partial;
            Status = PaymentStatus.Created;CreatedBy = GetCurrentUser();

        }

        //private string GetCurrentUser()
        //{
        //    if (SecuritySystem.CurrentUser != null)
        //    {
        //        return Session.GetObjectByKey<SecuritySystemUser>(SecuritySystem.CurrentUserId).UserName;
        //    }

        //    return null;
        //}
        
        [Index(0)]
        public DateTime PaymentDate
        {
            get { return GetPropertyValue<DateTime>("PaymentDate"); }
            set { SetPropertyValue("PaymentDate", value); }

        }

        [RuleRequiredField("", DefaultContexts.Save), Index(1)]
        public string JobOrderNumber
        {
            get { return GetPropertyValue<string>("JobOrderNumber"); }
            set { SetPropertyValue("JobOrderNumber", value); }
        }


        [RuleRequiredField("", DefaultContexts.Save), Index(2)]public string SalesOrderNumber
        {
            get { return GetPropertyValue<string>("SalesOrderNumber"); }
            set { SetPropertyValue("SalesOrderNumber", value); }
        }

        [RuleRequiredField("", DefaultContexts.Save), Index(3)]
        public Customer Customer
        {
            get { return GetPropertyValue<Customer>("Customer"); }
            set { SetPropertyValue("Customer", value); }
        }
        

        [Index(4)]
        public RequestedProofOfPayment IssueThisReceipt
        {
            get { return GetPropertyValue<RequestedProofOfPayment>("IssueThisReceipt"); }
            set { SetPropertyValue("IssueThisReceipt", value); }
        }

        [Index(5)]
        public BillingFor BillingFor
        {
            get { return GetPropertyValue<BillingFor>("BillingFor"); }
            set { SetPropertyValue("BillingFor", value); }
        }

        [Index(6)]
        public PaymentType PaymentType
        {
            get { return GetPropertyValue<PaymentType>("PaymentType"); }
            set { SetPropertyValue("PaymentType", value); }
        }

        [RuleValueComparison(ValueComparisonType.GreaterThan, 0), Index(7), ImmediatePostData]
        public decimal BilledAmount
        {
            get { return GetPropertyValue<decimal>("BilledAmount"); }
            set { SetPropertyValue("BilledAmount", value); }
        }

        [Index(8), ImmediatePostData]
        public decimal Discount
        {
            get { return GetPropertyValue<decimal>("Discount"); }
            set { SetPropertyValue("Discount", value); }
        }

        [Index(9)]
        public decimal AmountDue
        {
            get { return BilledAmount - this.PaymentHistory.Sum(x => x.Amount) - Discount; }
        }
        
        public string CreatedBy
        {
            get { return GetPropertyValue<string>("CreatedBy"); }
            set { SetPropertyValue("CreatedBy", value); }
        }


        [Appearance("NotedByVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "Status <>'Noted'", Context = "DetailView")]
        public Employee NotedBy
        {
            get { return GetPropertyValue<Employee>("NotedBy"); }
            set { SetPropertyValue("NotedBy", value); }
        }


        [Appearance("DateNotedVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "Status <>'Noted'", Context = "DetailView")]
        public DateTime DateNoted
        {
            get { return GetPropertyValue<DateTime>("DateNoted"); }
            set { SetPropertyValue("DateNoted", value); }
        }


        [Appearance("ApprovedByVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "Status <>'Approved'", Context = "DetailView")]
        public Employee ApprovedBy
        {
            get { return GetPropertyValue<Employee>("ApprovedBy"); }
            set { SetPropertyValue("ApprovedBy", value); }
        }


        [Appearance("DateApprovedVisibility", Visibility = ViewItemVisibility.Hide, Criteria = "Status <>'Approved'", Context = "DetailView")]
        public DateTime DateApproved
        {
            get { return GetPropertyValue<DateTime>("DateApproved"); }
            set { SetPropertyValue("DateApproved", value); }
        }

        [Size(256)]
        public string Description
        {
            get { return GetPropertyValue<string>("Description"); }
            set { SetPropertyValue("Description", value); }
        }

        public PaymentStatus Status
        {
            get { return GetPropertyValue<PaymentStatus>("Status"); }
            set
            {
                if (SetPropertyValue("Status", value))
                {
                    switch (value)
                    {
                        case PaymentStatus.Created:
                            break;
                        case PaymentStatus.Noted:
                            DateNoted = DateTime.Now;
                            break;
                        case PaymentStatus.Approved:
                            DateApproved = DateTime.Now;
                            break;
                    }
                }
            }
        }

        [Association(AssociationMapping.BillingStatementPayments)]
        public XPCollection<Payment> PaymentHistory { get { return GetCollection<Payment>("PaymentHistory"); } } 

        public IList<IStateMachine> GetStateMachines()
        {
            var list = new List<IStateMachine>
            {
                new PaymentReceiptStateMachine(XPObjectSpace.FindObjectSpaceByObject(this))
            };

            return list;
        }
    }
}