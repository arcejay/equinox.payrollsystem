using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem(NavigationItemName.Inventories)]
    public class ProductCategory : XPObject
    {
        private string _name;
        private string _description;
        private ProductCategory _parentCategory;
        private string _productCategoryCode;

        public ProductCategory()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public ProductCategory(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
        public string ProductCategoryCode
        {
            get { return _productCategoryCode; }
            set { SetPropertyValue("ProductCategoryCode", ref _productCategoryCode, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Association]
        public XPCollection<Product> Products { get { return GetCollection<Product>("Products"); } }



        [Association("Category-SubCategories")]
        public ProductCategory ParentCategory
        {
            get { return _parentCategory; }
            set { SetPropertyValue("ProductCategory", ref _parentCategory, value); }

        }

        [Association("Category-SubCategories")]
        public XPCollection<ProductCategory> SubCategories
        {
            get { return GetCollection<ProductCategory>("SubCategories"); }
        }
        
    }

}