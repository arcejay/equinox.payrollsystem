﻿namespace Equinox.ServiceManagement.Module
{
    public class NavigationItemName
    {
        public const string Inventories = "Inventories";
        public const string Customers = "Customers";
        public const string Services = "Services";
        public const string Vendors = "Vendors";
        public const string Accounting = "Accounting";
        public const string Sales = "Sales";
    }
}