using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;

namespace Equinox.ServiceManagement.Module
{[NonPersistent]
    public class VetiObject : XPObject
    {
        public VetiObject(Session session) : base(session)
        {
            
        }

        public string GetCurrentUser()
        {
            return SecuritySystem.CurrentUser != null ? 
                Session.GetObjectByKey<SecuritySystemUser>(SecuritySystem.CurrentUserId).UserName : 
                null;
        }
    }
}