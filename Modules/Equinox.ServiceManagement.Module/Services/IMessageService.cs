﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace EquinoxSystemManagement.Module.Services
{
    public interface IMessageService
    {
        Task<bool> SendSmsAsync(string destination, string message);

        bool SendSms(string destination, string message);

        Task<bool> SendEmailAsync(string from, MailMessage message);

        bool SendEmail(string from, MailMessage message);
    }
}