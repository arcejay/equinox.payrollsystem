using DevExpress.ExpressApp;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.StateMachine.NonPersistent;
using Equinox.ServiceManagement.Module.BusinessObjects;

namespace Equinox.ServiceManagement.Module.StateMachines
{
    public class RepairStatusStateMachine : StateMachine<JobTicket>, IStateMachineUISettings
    {
        private IState _startState;

        public RepairStatusStateMachine(IObjectSpace objectSpace) : base(objectSpace)
        {
            _startState = new State(this, RepairStatus.NotStarted);
            var pendingState = new State(this, RepairStatus.Pending);
            var inProgressState = new State(this, RepairStatus.InProgress);
            var completedState = new State(this, RepairStatus.Completed);

            //NotStarted -> (InProgress, Pending)
            _startState.Transitions.Add(new Transition(inProgressState));
            _startState.Transitions.Add(new Transition(pendingState));

            //InProgress -> (Pending, Completed)
            inProgressState.Transitions.Add(new Transition(pendingState));
            inProgressState.Transitions.Add(new Transition(completedState));

            //Pending -> (InProgress)
            pendingState.Transitions.Add(new Transition(inProgressState));

            //Completed

            States.Add(_startState);
            States.Add(pendingState);
            States.Add(inProgressState);
            States.Add(completedState);
        }

        public override string Name
        {
            get { return "(Change service repair status to"; }
        }

        public override string StatePropertyName
        {
            get { return "RepairStatus"; }
        }

        public override IState StartState
        {
            get { return _startState; }
        }

        public bool ExpandActionsInDetailView { get { return true; } }
    }
}