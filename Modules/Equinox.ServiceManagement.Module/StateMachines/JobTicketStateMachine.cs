using DevExpress.ExpressApp;
using DevExpress.ExpressApp.StateMachine;
using DevExpress.ExpressApp.StateMachine.NonPersistent;
using Equinox.ServiceManagement.Module.BusinessObjects;
using Equinox.ServiceManagement.Module.Enums;

namespace Equinox.ServiceManagement.Module.StateMachines
{
    public class JobTicketStateMachine : StateMachine<JobTicket>, IStateMachineUISettings
    {
        private IState _startState;

        public JobTicketStateMachine(IObjectSpace objectSpace) : base(objectSpace)
        {
            _startState = new State(this, JobTicketStatus.NotStarted);

            var inProgressState = new State(this, JobTicketStatus.InProgress);
            var billedState = new State(this, JobTicketStatus.Billed);
            var pullOutState = new State(this, JobTicketStatus.PullOut);
            var releasedState = new State(this, JobTicketStatus.Released);
            
            //NotStarted -> (InProgress)
            _startState.Transitions.Add(new Transition(inProgressState));

            //Billed -> (PullOut, Released)
            billedState.Transitions.Add(new Transition(pullOutState));
            billedState.Transitions.Add(new Transition(releasedState));

            //InProgress -> (PullOut, Billed) 
            inProgressState.Transitions.Add(new Transition(pullOutState));
            inProgressState.Transitions.Add(new Transition(billedState));

            //PullOut -> (Released, InProgress)
            pullOutState.Transitions.Add(new Transition(releasedState));
            pullOutState.Transitions.Add(new Transition(_startState));
            
            //Released -> ()

            States.Add(_startState);
            States.Add(inProgressState);
            States.Add(billedState);
            States.Add(pullOutState);
            States.Add(releasedState);

        }

        public override string Name => "(Sales) Change status to";

        public override string StatePropertyName => "Status";

        public override IState StartState => _startState;

        public bool ExpandActionsInDetailView => true;
    }
}