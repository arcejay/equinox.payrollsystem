﻿using DevExpress.ExpressApp.DC;

namespace Veti.CommonModule.BusinessObjects
{
    [DomainComponent]
    public interface IUserFriendlyIdDomainComponent : ISupportSequentialNumber { }
}