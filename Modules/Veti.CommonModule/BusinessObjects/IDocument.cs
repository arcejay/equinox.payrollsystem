﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace Veti.CommonModule.BusinessObjects
{
    [DefaultClassOptions]
    [DomainComponent]
    [XafDefaultProperty("Title")]
    [ImageName("BO_Note")]
    public interface IDocument : IUserFriendlyIdDomainComponent
    {
        [Calculated("concat('D', ToStr(SequentialNumber))")]
        string DocumentId { get; }
        [RuleRequiredField("IDocument.Title.RuleRequiredField", DefaultContexts.Save)]
        [FieldSize(255)]
        string Title { get; set; }
        [FieldSize(8192)]
        string Text { get; set; }
    }

    //Dennis: Uncomment this code if you want to have the SequentialNumber column created in each derived class table.
}