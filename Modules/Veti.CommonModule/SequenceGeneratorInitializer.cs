﻿using System;
using System.Configuration;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;

namespace Veti.CommonModule
{
    //This persistent class is used to store last sequential number for persistent objects.
    public class Sequence : XPBaseObject
    {
        private string typeName;
        private long nextSequence;
        public Sequence(Session session)
            : base(session)
        {
        }
        [Key]
        //Dennis: The size should be enough to store a full type name. However, you cannot use unlimited size for key columns.
        [Size(1024)]
        public string TypeName
        {
            get { return typeName; }
            set { SetPropertyValue("TypeName", ref typeName, value); }
        }
        public long NextSequence
        {
            get { return nextSequence; }
            set { SetPropertyValue("NextSequence", ref nextSequence, value); }
        }
    }
    public interface ISupportSequentialNumber
    {
        long SequentialNumber { get; set; }
    }
    public static class SequenceGeneratorInitializer
    {
        private static XafApplication application;
        private static XafApplication Application { get { return application; } }
        public static void Register(XafApplication app)
        {
            application = app;
            if (application != null)
                application.LoggedOn += new EventHandler<LogonEventArgs>(application_LoggedOn);
        }
        private static void application_LoggedOn(object sender, LogonEventArgs e)
        {
            Initialize();
        }
        //Dennis: It is important to set the SequenceGenerator.DefaultDataLayer property to the main application data layer.
        //If you use a custom IObjectSpaceProvider implementation, ensure that it exposes a working IDataLayer.
        public static void Initialize()
        {
            Guard.ArgumentNotNull(Application, "Application");
            XPObjectSpaceProvider provider = Application.ObjectSpaceProvider as XPObjectSpaceProvider;
            Guard.ArgumentNotNull(provider, "provider");
            if (provider.DataLayer == null)
            {
                //Dennis: This call is necessary to initialize a working data layer.
                provider.CreateObjectSpace();
            }
            if (provider.DataLayer is ThreadSafeDataLayer)
            {
                string connectionString = string.Empty;

                if (ConfigurationManager.ConnectionStrings["ConnectionString"] != null)
                {
                    connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                }

#if DEBUG
                if (ConfigurationManager.ConnectionStrings["ConnectionStringDebug"] != null)
                {
                    connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDebug"].ConnectionString;
                }
#endif
                //Application.Connection == null ? Application.ConnectionString : Application.Connection.ConnectionString
                //Dennis: We have to use a separate datalayer for the sequence generator because ThreadSafeDataLayer is usually used for ASP.NET applications.

                SequenceGenerator.DefaultDataLayer = XpoDefault.GetDataLayer(
                    connectionString,
                    XpoTypesInfoHelper.GetXpoTypeInfoSource().XPDictionary,
                    DevExpress.Xpo.DB.AutoCreateOption.None
                );
            }
            else
            {
                SequenceGenerator.DefaultDataLayer = provider.DataLayer;
            }
        }
    }
}