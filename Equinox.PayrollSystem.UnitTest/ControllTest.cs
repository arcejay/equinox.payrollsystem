using System;
using System.Data;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.DC.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using Equinox.PayrollSystem.Module.BusinessObjects.Accounting;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.Controllers;
using Equinox.PayrollSystem.Module.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Equinox.PayrollSystem.UnitTest
{
    [TestClass]
    public class ControllerTest
    {
        private IObjectSpace _objectSpace;
        private XafApplication _app;
        private GeneratePayrollViewController _controller;
        //private Session _session;
        [TestInitialize]
        public void Init()
        {
            
            XpoDefault.DataLayer = new SimpleDataLayer(new InMemoryDataStore(AutoCreateOption.None));
            InMemoryDataStoreProvider.Register();
            ModuleBase testModule = new ModuleBase();
            testModule.AdditionalExportedTypes.Add(typeof(LeaveApplication));
            testModule.AdditionalExportedTypes.Add(typeof(Employee));
            testModule.AdditionalExportedTypes.Add(typeof(Payroll));
            testModule.AdditionalExportedTypes.Add(typeof(PayrollItem));

            //_app.Modules.Add(testModule);

            //_app.Modules[0].AdditionalExportedTypes.Add(typeof(LeaveApplication));
            //_app.Modules[0].AdditionalExportedTypes.Add(typeof(Employee));
            //testModule.AdditionalExportedTypes.Add(typeof(Payroll));
            //testModule.AdditionalExportedTypes.Add(typeof(PayrollItem));
            //var cache = new DataSet();
            var dataProvider = new XPObjectSpaceProvider(new MemoryDataStoreProvider());

            _app = new TestApplication(testModule, dataProvider);

            //_app.Setup("Equinox.PayrollSystem", objectSpaceProvider);

            _controller = _app.CreateController<GeneratePayrollViewController>();
            

        }

        [TestMethod]
        public void App_is_not_null()
        {
            Assert.IsNotNull(_app);
        }
       
        [TestMethod]
        public void GeneratePayrollViewController_LeavePayrollAction()
        {
            _objectSpace = _app.CreateObjectSpace();

            var payroll = _objectSpace.CreateObject<Payroll>();

            payroll.StartDate = new DateTime(2015, 12, 16);
            payroll.StartDate = new DateTime(2015, 12, 31);
            payroll.PayrollDate = new DateTime(2016, 1, 5);
            payroll.PayrollStatus = PayrollStatus.InProgress;

            var payrollItem = _objectSpace.CreateObject<PayrollItem>();

            payroll.PayrollItems.Add(payrollItem);
            //payroll.Save();

            var leave = _objectSpace.CreateObject<LeaveApplication>();
            leave.StartDate = DateTime.Today.AddDays(-6);
            leave.EndDate = DateTime.Today.AddDays(-4);
            leave.ApplicationDate = DateTime.Today;

            var employee = _objectSpace.CreateObject<Employee>();
            employee.FirstName = "Oliver";
            employee.LastName = "Agustin";
            leave.Employee = employee;
            leave.LeaveType = LeaveType.VacationLeave;
            //leave.Save();

            payrollItem.Employee = employee;
            _objectSpace.CommitChanges();
            var view = _app.CreateDetailView(_objectSpace, payroll);
            view.CreateControls();
            _controller.SetView(view);
            _controller.LeavePayrollAction.DoExecute();

            Assert.AreEqual(16, payrollItem.LeaveWithPay);
            
        }
    }
}