using System;
using System.Collections.Generic;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Win;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;

namespace Equinox.PayrollSystem.UnitTest
{
    public class TestApplication : XafApplication
    {
        public TestApplication()
        {
            
        }

        public TestApplication(ModuleBase  moduleBase, XPObjectSpaceProvider dataStoreProvider)
        {
            Modules.Add(moduleBase);

            Setup("TestApplication", dataStoreProvider);
        }

        public TestApplication(ModuleBase[] modules, IXpoDataStoreProvider dataStoreProvider)
        {
            foreach (var moduleBase in modules)
            {
                    Modules.Add(moduleBase);
            }

            Setup("Test App", new XPObjectSpaceProvider(dataStoreProvider));
        }

        protected override LayoutManager CreateLayoutManagerCore(bool simple)
        {
            return null;
        }
    }

    public class TestModule : ModuleBase { }
}