﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using Equinox.PayrollSystem.Module;
using Equinox.PayrollSystem.Module.BusinessObjects;
using Equinox.PayrollSystem.Module.BusinessObjects.HR;
using Equinox.PayrollSystem.Module.BusinessObjects.TimeKeeping;
using Equinox.PayrollSystem.Module.Extensions;
using Equinox.PayrollSystem.Module.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Equinox.PayrollSystem.UnitTest
{
    [TestClass]
    public class WorkSheduleWith4TimeCheckDtrServiceUnitTest
    {
        private Session _session = null;
        private IDtrService _dtrService;
        private Employee _employee;
        private DailyTimeRecord _dtr;
        private const double Delta = 0.01;

        [TestInitialize]
        public void Init()
        {
            //initialize in memory db
            XpoDefault.DataLayer = new SimpleDataLayer(new InMemoryDataStore());
            _dtrService = new WorkSheduleWith4TimeCheckDtrService();
        }


        private OfficialBusiness SetOfficialBusiness(DateTime startDate,
                                            DateTime endDate,
                                            TimeSpan startTime,
                                            TimeSpan endTime,
                                            ObTimeDuration obTimeDuration)
        {
            var ob = new OfficialBusiness(_session);
            ob.StartDate = startDate;
            ob.EndDate = endDate;
            ob.StartTime = startTime;
            ob.EndTime = endTime;
            ob.TimeDuration = obTimeDuration;
            //ob.Save();
            return ob;
        }

        private void TestDtr(DateTime am1, DateTime am2, DateTime pm1, DateTime pm2, Holiday holiday = null)
        {
            InitDtr();

            var h = holiday ?? new Holiday(_session);

            _dtr.Time1 = am1;
            _dtr.Time2 = am2;
            _dtr.Time3 = pm1;
            _dtr.Time4 = pm2;

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetUnderTime(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetHoliday(_dtr, h);

        }

        private void InitDtr(int year = 2015, int month = 12, int days = 30)
        {
            _session = new Session(XpoDefault.DataLayer);

            //set default timecheck
            var timeCheck1 = new TimeSpan(9, 0, 0);
            var timeCheck2 = new TimeSpan(12, 0, 0);
            var timeCheck3 = new TimeSpan(13, 0, 0);
            var timeCheck4 = new TimeSpan(18, 0, 0);

            //set work schedule
            var workSchedule = new WorkSchedule(_session);
            workSchedule.TimeCheckItems.Add(new TimeCheckItem(_session) { SequenceId = 0, TimeCheck = timeCheck1 });
            workSchedule.TimeCheckItems.Add(new TimeCheckItem(_session) { SequenceId = 1, TimeCheck = timeCheck2 });
            workSchedule.TimeCheckItems.Add(new TimeCheckItem(_session) { SequenceId = 2, TimeCheck = timeCheck3 });
            workSchedule.TimeCheckItems.Add(new TimeCheckItem(_session) { SequenceId = 3, TimeCheck = timeCheck4 });
            //_session.Save(workSchedule);

            //set Employee
            _employee = new Employee(_session);
            _employee.WorkSchedule = workSchedule;
            _employee.FirstName = "Oliver";
            _employee.LastName = "Agustin";
            _employee.MiddleName = "Cantor";
            //_session.Save(workSchedule);
            //_session.Save(_employee);

            _dtr = new DailyTimeRecord(_session);
            _dtr.Employee = _employee;

            //add dtr
            _dtr.Date = new DateTime(year, month, days);
            _dtr.Time1 = new DateTime(year, month, days, 8, 55, 0);
            _dtr.Time2 = new DateTime(year, month, days, 12, 00, 0);
            _dtr.Time3 = new DateTime(year, month, days, 13, 0, 0);
            _dtr.Time4 = new DateTime(year, month, days, 18, 0, 0);

            _dtr.Save();
        }

        #region "WORKHOUR"
        // UNIT TEST FOR WORKHOUR
        [TestMethod]
        public void WorkHour_equals_8_hours_when_timein_is_earlier_than_9am()
        {
            InitDtr();

            //acceptable work hours
            _dtrService.GetWorkHour(_dtr);

            Assert.AreEqual(8, _dtr.WorkHours);

            // 9:00 - 12:00, 1:00 - 6:00
            _dtr.Time1 = new DateTime(2015, 12, 30, 9, 00, 0);

            _dtrService.GetWorkHour(_dtr);
            
        }

        [TestMethod]
        public void WorkHour_equals_8_hours_when_timein_is_within_graceperiod_of_15min()
        {
            InitDtr();
            // 9:00 - 12:00, 1:00 - 6:00
            _dtr.Time1 = new DateTime(2016, 1, 2, 9, 15, 0);

            _dtrService.GetWorkHour(_dtr);
            Assert.AreEqual(8, _dtr.WorkHours);

            //no tardiness, undertime, awol
            Assert.AreEqual(0, _dtr.Tardiness);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.Lwop);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
            Assert.AreEqual(0, _dtr.Holiday);
        }

        [TestMethod]
        public void WorkHour_is_less_than_8_hours_when_timein_is_greater_than_graceperiod()
        {
            InitDtr();
            // 9:00 - 12:00, 1:00 - 6:00
            _dtr.Time1 = new DateTime(2015, 12, 30, 9, 16, 0);

            _dtrService.GetWorkHour(_dtr);

            double ex = 7.7333;

            Assert.AreEqual(ex, _dtr.WorkHours, Delta, "WH = " + ex);

            //tardiness is calculated
            Assert.AreEqual(16 / 60, _dtr.Tardiness, Delta);

            //no undertime, awol
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.Lwop);
        }

        [TestMethod]
        public void WorkHour_tardiness_undertime()
        {
            //ERNEST DEC 16
            var t1 = new DateTime(2015, 12, 30, 9, 41, 0);
            var t2 = new DateTime(2015, 12, 30, 12, 09, 0);
            var t3 = new DateTime(2015, 12, 30, 12, 29, 0);
            var t4 = new DateTime(2015, 12, 30, 15, 07, 0);

            TestDtr(t1, t2, t3, t4);

            var total = _dtr.WorkHours + _dtr.Tardiness + _dtr.UnderTime;

            Assert.AreEqual(8, total,  "Ernest-dec16: WorkHour=" +
                _dtr.WorkHours + " Tardiness=" +
                _dtr.Tardiness + " UT=" +
                _dtr.UnderTime);

        }

        [TestMethod]
        public void WorkHour_Tardiness_UnderTime_should_not_exceed_8_hours()
        {
            //test 1
            var t1 = new DateTime(2015, 12, 30, 8, 54, 0);
            var t2 = new DateTime(2015, 12, 30, 11, 57, 0);
            var t3 = new DateTime(2015, 12, 30, 13, 12, 0);
            var t4 = new DateTime(2015, 12, 30, 19, 58, 0);

            TestDtr(t1, t2, t3, t4);

            var total = _dtr.WorkHours + _dtr.Tardiness + _dtr.UnderTime + _dtr.Lwop;

            Assert.AreEqual(8, total, Delta, "Test1: WH =" + _dtr.WorkHours + " Tardiness =" + _dtr.Tardiness + " UT = " + _dtr.UnderTime);

            //test 2
            t1 = new DateTime(2015, 12, 30, 6, 37, 0);
            t2 = default(DateTime);
            t3 = new DateTime(2015, 12, 30, 12, 50, 0);
            t4 = new DateTime(2015, 12, 30, 18, 38, 0);

            TestDtr(t1, t2, t3, t4);

            total = _dtr.WorkHours + _dtr.Tardiness + _dtr.UnderTime;

            Assert.AreEqual(8, total, Delta, "Test2: WorkHour=" +
                _dtr.WorkHours + " Tardiness=" +
                _dtr.Tardiness + " UT=" +
                _dtr.UnderTime);

            //Test 3
            t1 = new DateTime(2015, 12, 30, 8, 54, 0);
            t2 = new DateTime(2015, 12, 30, 11, 57, 0);
            t3 = new DateTime(2015, 12, 30, 13, 12, 0);
            t4 = new DateTime(2015, 12, 30, 19, 58, 0);

            TestDtr(t1, t2, t3, t4);

            total = _dtr.WorkHours + _dtr.Tardiness + _dtr.UnderTime;

            Assert.AreEqual(8, total, Delta, "Test3: WorkHour=" +
                _dtr.WorkHours + " Tardiness=" +
                _dtr.Tardiness + " UT=" +
                _dtr.UnderTime);

            
        }

        #endregion

        #region "Generate DTR"

        [TestMethod]
        public void GenerateDtr_correctly_parses_4_pm_timelogs()
        {
            //test 1
            var t1 = new DateTime(2016, 1, 14, 12, 06, 0);
            var t2 = new DateTime(2016, 1, 14, 12, 45, 0);
            var t3 = new DateTime(2016, 1, 14, 18, 4, 0);
            var t4 = new DateTime(2016, 1, 14, 18, 15, 0);

            var list = new List<DateTime> { t1, t2, t3, t4 };

            InitDtr();

            _dtrService.GenerateDtr(_dtr, list);

            Assert.AreEqual(t1, _dtr.Time2);
            Assert.AreEqual(t2, _dtr.Time3);
            Assert.AreEqual(t3, _dtr.Time4);

        }

        [TestMethod]
        public void GenerateDtr_correctly_parses_2_timelogs_between_12_1pm()
        {
            //test 1
            var t1 = new DateTime(2016, 1, 18, 9, 30, 0);
            var t2 = new DateTime(2016, 1, 18, 12, 31, 0);
            var t3 = new DateTime(2016, 1, 18, 12, 43, 0);
            var t4 = default(DateTime);

            var list = new List<DateTime> { t1, t2, t3, t4 };

            InitDtr();

            _dtrService.GenerateDtr(_dtr, list);

            Assert.AreEqual(t1, _dtr.Time1);
            Assert.AreEqual(t2, _dtr.Time2);
            Assert.AreEqual(t3, _dtr.Time3);

        }

        [TestMethod]
        public void GenerateDtr_correctly_parses_1_timelog_in_am_and_2_timelogs_between_12_1pm()
        {
            //test 1
            var t1 = new DateTime(2016, 2, 13, 11, 01, 0);
            var t2 = new DateTime(2016, 2, 13, 12, 11, 0);
            var t3 = new DateTime(2016, 2, 13, 12, 35, 0);
            var t4 = default(DateTime);

            var list = new List<DateTime> { t1, t2, t3, t4 };

            InitDtr();

            _dtrService.GenerateDtr(_dtr, list);

            Assert.AreEqual(t1, _dtr.Time1);
            Assert.AreEqual(t2, _dtr.Time2);
            Assert.AreEqual(t3, _dtr.Time3);

        }


        #endregion

        #region "LWOP"
        // unit test for LWOP

        [TestMethod]
        public void Lwop_is_8_hours_when_employee_is_absent()
        {
            InitDtr();

            // no timelogs
            _dtr.Time1 = default(DateTime);
            _dtr.Time2 = default(DateTime);
            _dtr.Time3 = default(DateTime);
            _dtr.Time4 = default(DateTime);

            _dtrService.GetWorkHour(_dtr);
            Assert.AreEqual(8, _dtr.Lwop);

            //no workhours, undertime, or late
            Assert.AreEqual(0, _dtr.Tardiness);
            Assert.AreEqual(0, _dtr.WorkHours);
            Assert.AreEqual(0, _dtr.UnderTime);
        }
        
        [TestMethod]
        public void Lwop_is_3_hours_when_employee_is_absent_in_the_morning()
        {
            InitDtr();

            // no timelogs
            _dtr.Time1 = default(DateTime);
            _dtr.Time2 = default(DateTime);

            _dtrService.GetWorkHour(_dtr);
            Assert.AreEqual(3, _dtr.Lwop);

            //no workhours, undertime, or late
            Assert.AreEqual(0, _dtr.Tardiness);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
            Assert.AreEqual(0, _dtr.Holiday);
        }

        [TestMethod]
        public void Lwop_is_5_hours_when_employee_is_absent_in_the_afternoon()
        {
            InitDtr();

            // no timelogs
            _dtr.Time3 = default(DateTime);
            _dtr.Time4 = default(DateTime);

            _dtrService.GetWorkHour(_dtr);
            Assert.AreEqual(5, _dtr.Lwop);

            //no workhours, undertime, or late
            Assert.AreEqual(0, _dtr.Tardiness);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
            Assert.AreEqual(0, _dtr.Holiday);

        }

#endregion

        #region "TARDINESS"
        //UNIT TEST FOR TARDINESS
        [TestMethod]
        public void Tardiness_is_2_hours_when_employee_is_late_in_the_morning()
        {
            InitDtr();
            // 11:00 - 12:00
            _dtr.Time1 = new DateTime(2015, 12, 30, 11, 0, 0);

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(2, _dtr.Tardiness);

            //no workhours, undertime, or late
            Assert.AreEqual(6, _dtr.WorkHours);
            Assert.AreEqual(0, _dtr.Lwop);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
        }

        [TestMethod]
        public void Tardiness_is_2_hours_when_employee_is_late_in_the_afternoon()
        {
            InitDtr();
            // 11:00 - 12:00
            _dtr.Time3 = new DateTime(2015, 12, 30, 15, 0, 0);

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(2, _dtr.Tardiness);

            //no workhours, undertime, or late
            Assert.AreEqual(6, _dtr.WorkHours);
            Assert.AreEqual(0, _dtr.Lwop);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
        }

        [TestMethod]
        public void Tardiness_is_3_hours_when_employee_didnt_checkin_in_the_morning()
        {
            InitDtr();
            // 11:00 - 12:00
            _dtr.Time1 = default(DateTime);

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(3, _dtr.Tardiness);

            //no workhours, undertime, or late
            Assert.AreEqual(5, _dtr.WorkHours);
            Assert.AreEqual(0, _dtr.Lwop);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
        }

        [TestMethod]
        public void Tardiness_is_5_hours_when_employee_didnt_checkin_in_the_afternoon()
        {
            InitDtr();
            // 11:00 - 12:00
            _dtr.Time3 = default(DateTime);

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(5, _dtr.Tardiness);

            //no workhours, undertime, or late
            Assert.AreEqual(3, _dtr.WorkHours);
            Assert.AreEqual(0, _dtr.Lwop);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
        }

        [TestMethod]
        public void Tardiness_is_2_hours_when_employee_is_late_1hr_each_in_the_morning_and_afternoon()
        {
            InitDtr();
            // 11:00 - 12:00
            _dtr.Time1 = new DateTime(2015, 12, 30, 10, 0, 0);
            _dtr.Time3 = new DateTime(2015, 12, 30, 14, 0, 0);

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(2, _dtr.Tardiness);

            //no workhours, undertime, or late
            Assert.AreEqual(6, _dtr.WorkHours);
            Assert.AreEqual(0, _dtr.Lwop);
            Assert.AreEqual(0, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.OfficialBusiness);
        }

        [TestMethod]
        public void WorkHour_with_6_minutes_late_PM()
        {
            InitDtr();
            // 9:00 - 12:00, 1:00 - 6:00
            var t1 = new DateTime(2015, 12, 30, 8, 49, 0);
            var t2 = new DateTime(2015, 12, 30, 12, 1, 0);
            var t3 = new DateTime(2015, 12, 30, 13, 7, 0);
            var t4 = new DateTime(2015, 12, 30, 18, 3, 0);

            TestDtr(t1, t2, t3, t4);

            double expected = 7.9;

            Assert.AreEqual(expected, _dtr.WorkHours, Delta, "WH = " + expected);

            //tardiness is calculated
            Assert.AreEqual(0.1, _dtr.Tardiness, Delta, "Tardiness = " + _dtr.Tardiness);

            //no undertime, awol
            Assert.AreEqual(0, _dtr.UnderTime, "UT = " + _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = " + _dtr.Lwop);
        }


        #endregion

        #region "UNDERTIME"
        [TestMethod]
        public void Undertime_is_1_hour_when_employee_checks_out_early_in_the_morning()
        {
            InitDtr();

            // 8:55 - 11:00
            _dtr.Time1 = new DateTime(2015, 12, 30, 9, 0, 0);
            _dtr.Time2 = new DateTime(2015, 12, 30, 11, 0, 0);
            _dtr.Time3 = new DateTime(2015, 12, 30, 13, 0, 0);
            _dtr.Time4 = new DateTime(2015, 12, 30, 18, 0, 0);


            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(1, _dtr.UnderTime);

            //no workhours, undertime, or late
            Assert.AreEqual(7, _dtr.WorkHours, "WorkHour = 7");
            Assert.AreEqual(0, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        [TestMethod]
        public void WorkHour_is_7_hours_when_employee_checks_out_1_hour_early_in_the_morning()
        {
            InitDtr();
            // 9AM-11AM, 1PM-6PM
            var t1 = new DateTime(2015, 12, 30, 9, 0, 0);
            var t2 = new DateTime(2015, 12, 30, 11, 0, 0);
            var t3 = new DateTime(2015, 12, 30, 13, 0, 0);
            var t4 = new DateTime(2015, 12, 30, 18, 0, 0);

            TestDtr(t1, t2, t3, t4);

            Assert.AreEqual(7, _dtr.WorkHours, "WorkHour = 7");

            //no workhours, undertime, or late
            Assert.AreEqual(1, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        [TestMethod]
        public void WorkHour_is_7_hours_when_employee_checks_out_1_hour_early_in_the_afternoon()
        {
            InitDtr();
            // 9AM-11AM, 1PM-6PM
            var t1 = new DateTime(2015, 12, 30, 9, 0, 0);
            var t2 = new DateTime(2015, 12, 30, 12, 0, 0);
            var t3 = new DateTime(2015, 12, 30, 13, 0, 0);
            var t4 = new DateTime(2015, 12, 30, 17, 0, 0);

            TestDtr(t1, t2, t3, t4);

            Assert.AreEqual(7, _dtr.WorkHours, "WorkHour = 7");

            //no workhours, undertime, or late
            Assert.AreEqual(1, _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        [TestMethod]
        public void WorkHour_is_7_hours_when_employee_is_late_1_hour_early_in_the_afternoon()
        {
            InitDtr();
            // 9AM-11AM, 1PM-6PM
            var t1 = new DateTime(2015, 12, 30, 9, 0, 0);
            var t2 = new DateTime(2015, 12, 30, 12, 0, 0);
            var t3 = new DateTime(2015, 12, 30, 14, 0, 0);
            var t4 = new DateTime(2015, 12, 30, 18, 0, 0);

            TestDtr(t1, t2, t3, t4);

            Assert.AreEqual(7, _dtr.WorkHours, "WorkHour = 7");

            //no workhours, undertime, or late
            Assert.AreEqual(0, _dtr.UnderTime, "UT = 0");
            Assert.AreEqual(1, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        [TestMethod]
        public void WorkHour_is_7_hours_when_employee_is_late_1_hour_early_in_the_morning()
        {
            InitDtr();
            // 9AM-11AM, 1PM-6PM
            var t1 = new DateTime(2015, 12, 30, 10, 0, 0);
            var t2 = new DateTime(2015, 12, 30, 12, 0, 0);
            var t3 = new DateTime(2015, 12, 30, 13, 0, 0);
            var t4 = new DateTime(2015, 12, 30, 18, 0, 0);

            TestDtr(t1, t2, t3, t4);

            Assert.AreEqual(7, _dtr.WorkHours, "WorkHour = 7");

            //no workhours, undertime, or late
            Assert.AreEqual(0, _dtr.UnderTime, "UT = 0");
            Assert.AreEqual(1, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        [TestMethod]
        public void Undertime_is_1_hour_when_employee_checks_out_1_hour_early_in_the_afternoon()
        {
            InitDtr();
            // 9AM-11AM, 1PM-6PM

            var t1 = new DateTime(2015, 12, 30, 9, 0, 0);
            var t2 = new DateTime(2015, 12, 30, 11, 0, 0);
            var t3 = new DateTime(2015, 12, 30, 13, 0, 0);
            var t4 = new DateTime(2015, 12, 30, 18, 0, 0);

            TestDtr(t1, t2, t3, t4);

            Assert.AreEqual(1, _dtr.UnderTime);

            //no workhours, undertime, or late
            Assert.AreEqual(7, _dtr.WorkHours, "WorkHour = 7");
            Assert.AreEqual(0, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        [TestMethod]
        public void Undertime_is_3_hours_when_employee_didnt_check_out_in_the_morning()
        {
            InitDtr();
            // 8:55 - 
            _dtr.Time2 = default(DateTime);

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(3, _dtr.UnderTime);

            //no workhours, undertime, or late
            Assert.AreEqual(5, _dtr.WorkHours, "WorkHour = 5");
            Assert.AreEqual(0, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        [TestMethod]
        public void Undertime_is_5_hours_when_employee_didnt_check_out_in_the_afternoon()
        {
            InitDtr();
            // 1:00 - 
            _dtr.Time4 = default(DateTime);

            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetTardiness(_dtr);
            _dtrService.GetUnderTime(_dtr);

            Assert.AreEqual(5, _dtr.UnderTime);

            //no workhours, undertime, or late
            Assert.AreEqual(3, _dtr.WorkHours, "WorkHour = 5");
            Assert.AreEqual(0, _dtr.Tardiness, "Tardiness = 0");
            Assert.AreEqual(0, _dtr.Lwop, "LWOP = 0");
            Assert.AreEqual(0, _dtr.OfficialBusiness, "OB = 0");
        }

        #endregion

        #region "HOLIDAY"

        [TestMethod]
        public void Holiday_is_8_hours_when_day_is_declared_holiday()
        {
            //test 1
            var t1 = new DateTime(2015, 12, 31, 9, 0, 0);
            var t2 = new DateTime(2015, 12, 31, 12, 0, 0);
            var t3 = new DateTime(2015, 12, 31, 13, 0, 0);
            var t4 = new DateTime(2015, 12, 31, 18, 0, 0);

            var date = new DateTime(2015, 12, 31);
            _session = new Session(XpoDefault.DataLayer);
            var holiday = new Holiday(_session)
            {
                StartDate = date,
                EndDate = date,
                HolidayType = HolidayType.RegularHoliday
            };

            TestDtr(t1, t2, t3, t4, holiday);

            Assert.AreEqual(8, _dtr.Holiday, "Holiday = 8");
            Assert.AreEqual(1, _dtr.HolidayMultiplier, "HolidayMultiplier = " +_dtr.HolidayMultiplier);
            Assert.IsTrue(_dtr.IsHoliday, "It's holiday");

        }

        [TestMethod]
        public void Holiday_is_16_hours_and_amount_computed_correctly_for_nonworking_and_legal_holiday()
        {
            //test 1
            var t1 = new DateTime(2015, 12, 31, 9, 0, 0);
            var t2 = new DateTime(2015, 12, 31, 12, 0, 0);
            var t3 = new DateTime(2015, 12, 31, 13, 0, 0);
            var t4 = new DateTime(2015, 12, 31, 18, 0, 0);

            var date = new DateTime(2015, 12, 31);
            _session = new Session(XpoDefault.DataLayer);
            var holiday = new Holiday(_session)
            {
                StartDate = date,
                EndDate = date,
                HolidayType = HolidayType.RegularHoliday
            };

            TestDtr(t1, t2, t3, t4, holiday);

            Assert.AreEqual(8, _dtr.Holiday, "Holiday = 8");
            Assert.AreEqual(1, _dtr.HolidayMultiplier, "HolidayMultiplier = " + _dtr.HolidayMultiplier);
            Assert.IsTrue(_dtr.IsHoliday, "It's holiday");

        }

        [TestMethod]
        public void Holiday_multiplier_is_thirty_percent_for_special_non_working_day_if_present()
        {
            //test 1
            var t1 = new DateTime(2015, 12, 31, 9, 0, 0);
            var t2 = new DateTime(2015, 12, 31, 12, 0, 0);
            var t3 = new DateTime(2015, 12, 31, 13, 0, 0);
            var t4 = new DateTime(2015, 12, 31, 18, 0, 0);

            var date = new DateTime(2015, 12, 31);
            _session = new Session(XpoDefault.DataLayer);
            var holiday = new Holiday(_session)
            {
                StartDate = date,
                EndDate = date,
                HolidayType = HolidayType.SpecialNonWorkingDay
            };

            TestDtr(t1, t2, t3, t4, holiday);
            
            Assert.AreEqual(8, _dtr.Holiday, "Holiday = 8");
            Assert.AreEqual(0.3, _dtr.HolidayMultiplier, "HolidayMultiplier = 0.3");
            Assert.IsTrue(_dtr.IsHoliday, "It's holiday");

        }

        [TestMethod]
        public void Holiday_credit_is_none_if_absent_and_holiday_type_is_special_non_working_day()
        {
            //test 1
            var t1 = default(DateTime);
            var t2 = default(DateTime);
            var t3 = default(DateTime);
            var t4 = default(DateTime);

            var date = new DateTime(2015, 12, 31);
            _session = new Session(XpoDefault.DataLayer);
            var holiday = new Holiday(_session)
            {
                StartDate = date,
                EndDate = date,
                HolidayType = HolidayType.SpecialNonWorkingDay
            };

            TestDtr(t1, t2, t3, t4, holiday);

            Assert.AreEqual(0.3, _dtr.HolidayMultiplier, "HolidayMultiplier = " + _dtr.HolidayMultiplier);
            Assert.IsTrue(_dtr.IsHoliday, "It's holiday");
            Assert.AreEqual(0, _dtr.WorkHours, "WH = " +_dtr.WorkHours);
            Assert.AreEqual(0, _dtr.Holiday, "Holiday = " + _dtr.Holiday);

        }

        #endregion

        #region "OB"

        [TestMethod]
        public void OfficialBusiness_equals_3_hours_if_OB_is_AM_only()
        {
            InitDtr();

            var date = new DateTime(2015, 12, 30);
            var startTime = new TimeSpan(9, 0, 0);
            var endTime = new TimeSpan(12, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.AmOnly);
            _dtrService.GetWorkHour(_dtr);
            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(3, _dtr.OfficialBusiness, "OfficialBusiness = 3");
        }

        [TestMethod]
        public void OfficialBusiness_equals_5_hours_if_OB_is_custom()
        {
            //test 1
            var t1 = new DateTime(2015, 12, 29, 8, 46, 0);
            var t2 = new DateTime(2015, 12, 29, 11, 21, 0);
            var t3 = default(DateTime);
            var t4 = new DateTime(2015, 12, 29, 18, 20, 0);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2015, 12, 29);
            var startTime = new TimeSpan(11, 21, 0);
            var endTime = new TimeSpan(18, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.Custom);

            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(5.65, _dtr.OfficialBusiness,Delta,  "OfficialBusiness = " + _dtr.OfficialBusiness);
            Assert.AreEqual(0, _dtr.Tardiness, Delta, "Tardiness = " + _dtr.Tardiness);
            Assert.AreEqual(0, _dtr.UnderTime, "UT = " + _dtr.UnderTime);
            Assert.AreEqual(2.35, _dtr.WorkHours, Delta, "WH = " + _dtr.WorkHours);
        }

        [TestMethod]
        public void OfficialBusiness_equals_5_hours_if_OB_is_custom_with_complete_login_and_logout()
        {
            //test 1
            var t1 = new DateTime(2015, 12, 29, 9, 19, 0);
            var t2 = new DateTime(2015, 12, 29, 11, 22, 0);
            var t3 = new DateTime(2015, 12, 29, 15, 45, 0);
            var t4 = new DateTime(2015, 12, 29, 18, 12, 0);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2015, 11, 29);
            var startTime = new TimeSpan(11, 22, 0);
            var endTime = new TimeSpan(15, 45, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.Custom);

            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(3.39, _dtr.OfficialBusiness, Delta, "OfficialBusiness = " + _dtr.OfficialBusiness);
            Assert.AreEqual(0.32, _dtr.Tardiness, Delta, "Tardiness = " + _dtr.Tardiness);
            Assert.AreEqual(0, _dtr.UnderTime, "UT = " + _dtr.UnderTime);
            Assert.AreEqual(4.3, _dtr.WorkHours, Delta, "WH = " + _dtr.WorkHours);
        }

        [TestMethod]
        public void OfficialBusiness_equals_3_hours_if_OB_AM_Only()
        {
            //test 1
            var t1 = new DateTime(2015, 12, 21, 9, 27, 0);
            var t2 = default(DateTime);
            var t3 = new DateTime(2015, 12, 21, 13, 2, 0);
            var t4 = new DateTime(2015, 12, 21, 18, 20, 0);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2015, 12, 21);
            var startTime = new TimeSpan(9, 0, 0);
            var endTime = new TimeSpan(12, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.AmOnly);

            _dtrService.GetOfficialBusiness(_dtr, ob);
            
            Assert.AreEqual(3, _dtr.OfficialBusiness, "OfficialBusiness = " +_dtr.OfficialBusiness);
            Assert.AreEqual(0.03, _dtr.Tardiness,Delta,  "Tardiness = " +_dtr.Tardiness);
            Assert.AreEqual(0, _dtr.UnderTime, "UT = " +_dtr.UnderTime);
            Assert.AreEqual(4.97, _dtr.WorkHours, Delta, "WH = " + _dtr.WorkHours);
        }

        [TestMethod]
        public void OfficialBusiness_equals_8_hours_if_OB_Full_day_with_pm_logout()
        {
            //test 1
            var t1 = default(DateTime);
            var t2 = default(DateTime);
            var t3 = default(DateTime);
            var t4 = new DateTime(2015, 12, 22, 10, 55, 0);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2015, 12, 22);
            var startTime = new TimeSpan(9, 0, 0);
            var endTime = new TimeSpan(18, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.WholeDay);

            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(8, _dtr.OfficialBusiness, "OfficialBusiness = " + _dtr.OfficialBusiness);
            Assert.AreEqual(0, _dtr.Tardiness, "Tardiness = " + _dtr.Tardiness);
            Assert.AreEqual(0, _dtr.UnderTime, "UT = " + _dtr.UnderTime);
            Assert.AreEqual(0, _dtr.WorkHours, "WH = " + _dtr.WorkHours);
        }

        [TestMethod]
        public void OfficialBusiness_equals_3_hour_if_OB_IS_2PM_5PM()
        {
            var t1 = new DateTime(2015, 12, 21, 9, 27, 0);
            var t2 = default(DateTime);
            var t3 = new DateTime(2015, 12, 21, 13, 2, 0);
            var t4 = new DateTime(2015, 12, 21, 18, 20, 0);

            TestDtr(t1, t2, t3, t4);
            
            var date = new DateTime(2015, 12, 31);
            var startTime = new TimeSpan(9, 0, 0);
            var endTime = new TimeSpan(12, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.Custom);
            
            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(3, _dtr.OfficialBusiness, "OfficialBusiness = 3");

        }

        [TestMethod]
        public void OB_11AM_to_6PM_JAN15_2016()
        {
            var t1 = default(DateTime);
            var t2 = default(DateTime);
            var t3 = default(DateTime);
            var t4 = default(DateTime);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2016, 1, 15);
            var startTime = new TimeSpan(11, 0, 0);
            var endTime = new TimeSpan(18, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.Custom);

            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(6, _dtr.OfficialBusiness, "OfficialBusiness = " + _dtr.OfficialBusiness);
            //Assert.AreEqual(5, _dtr.WorkHours, "WH = 5");
        }

        [TestMethod]
        public void OfficialBusiness_equals_3_hours_WorkHours_is_5_hours_()
        {
            var t1 = new DateTime(2016, 1, 5, 8, 52, 0);
            var t2 = default(DateTime);
            var t3 = new DateTime(2016, 1, 5, 13, 37, 0);
            var t4 = default(DateTime);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2016, 1, 5);
            var startTime = new TimeSpan(9, 0, 0);
            var endTime = new TimeSpan(13,37, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.Custom);

            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(3.62, _dtr.OfficialBusiness, Delta, "OfficialBusiness = 3.62");
            Assert.AreEqual(4.383, _dtr.WorkHours, Delta, "WH = 4.38");
        }

        [TestMethod]
        public void OfficialBusiness_equals_3_hours_LWOP_is_5_hours()
        {
            var t1 = default(DateTime);
            var t2 = default(DateTime);
            var t3 = default(DateTime);
            var t4 = default(DateTime);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2016, 1, 22);
            var startTime = new TimeSpan(9, 0, 0);
            var endTime = new TimeSpan(12, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.AmOnly);

            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(3, _dtr.OfficialBusiness, "OfficialBusiness = 3");
            Assert.AreEqual(5, _dtr.Lwop, "LWOP = 5");
            Assert.AreEqual(0, _dtr.Tardiness, "Tardy = 0");
            Assert.AreEqual(0, _dtr.UnderTime, "UT = 0");
            Assert.AreEqual(0, _dtr.WorkHours, "WH = 0");
        }

        [TestMethod]
        public void OfficialBusiness_equals_5_hours_WH_is_3_hours()
        {
            var t1 = new DateTime(2016, 2, 10, 9, 05, 0);
            var t2 = new DateTime(2016, 2, 10, 12, 06, 0);
            var t3 = default(DateTime);
            var t4 = default(DateTime);

            TestDtr(t1, t2, t3, t4);

            var date = new DateTime(2016, 2, 10);
            var startTime = new TimeSpan(13, 0, 0);
            var endTime = new TimeSpan(18, 0, 0);

            var ob = SetOfficialBusiness(date, date, startTime, endTime, ObTimeDuration.PmOnly);

            _dtrService.GetOfficialBusiness(_dtr, ob);

            Assert.AreEqual(5, _dtr.OfficialBusiness, "OB = 5");
            Assert.AreEqual(3, _dtr.WorkHours, "WH = 3");
        }

        #endregion
    }
}
